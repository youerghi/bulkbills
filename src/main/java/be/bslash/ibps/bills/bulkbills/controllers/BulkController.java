package be.bslash.ibps.bills.bulkbills.controllers;

import be.bslash.ibps.bills.bulkbills.configurations.MessageEnvelope;
import be.bslash.ibps.bills.bulkbills.dto.request.BulkBillRqDto;
import be.bslash.ibps.bills.bulkbills.dto.request.SearchBulkBillRqDto;
import be.bslash.ibps.bills.bulkbills.dto.response.BulkBillResultTimelineDtoRs;
import be.bslash.ibps.bills.bulkbills.dto.response.PageDtoRs;
import be.bslash.ibps.bills.bulkbills.entities.EntityUser;
import be.bslash.ibps.bills.bulkbills.enums.BulkType;
import be.bslash.ibps.bills.bulkbills.enums.Status;
import be.bslash.ibps.bills.bulkbills.exceptions.HttpServiceException;
import be.bslash.ibps.bills.bulkbills.security.JwtTokenProvider;
import be.bslash.ibps.bills.bulkbills.services.BillNotificationService;
import be.bslash.ibps.bills.bulkbills.services.BillService;
import be.bslash.ibps.bills.bulkbills.services.BulkBillService;
import be.bslash.ibps.bills.bulkbills.services.BulkBillUploadService;
import be.bslash.ibps.bills.bulkbills.services.EntityUserService;
import be.bslash.ibps.bills.bulkbills.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

@CrossOrigin
@Controller
@RequestMapping("/bulk")
public class BulkController {

    @Autowired
    private BulkBillService bulkBillService;

    @Autowired
    private EntityUserService entityUserService;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Autowired
    private BillService billService;

    @Autowired
    private BulkBillUploadService bulkBillUploadService;

    @Autowired
    private BillNotificationService billNotificationService;

    @RequestMapping(value = "/master/generate", method = RequestMethod.POST)
    public ResponseEntity<MessageEnvelope> generateMasterBulk(HttpServletRequest httpServletRequest,
                                                              @RequestParam(value = "bulkType", required = false) String bulkType) {

        Locale locale = httpServletRequest.getLocale();

        String token = httpServletRequest.getHeader("Authorization").split(" ")[1];

        String username = jwtTokenProvider.getUsername(token);

        EntityUser entityUser = entityUserService.findEntityUserByUsername(username, locale);

        if (entityUser.getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_inactive", locale);
        }

        if (entityUser.getStatus().equals(Status.DELETED)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_deleted", locale);
        }

        if (entityUser.getEntity().getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "entity_inactive", locale);
        }

        if (entityUser.getIsPasswordReset() == 1) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "password_reset_issue", locale);
        }

        MessageEnvelope messageEnvelope = new MessageEnvelope(HttpStatus.OK, "success", bulkBillService.generateBulkBill(entityUser, BulkType.getValue(bulkType), locale), locale);

        return new ResponseEntity<>(messageEnvelope, HttpStatus.OK);
    }

    @RequestMapping(value = "/master/upload", method = RequestMethod.POST)
    public ResponseEntity<MessageEnvelope> validateMasterBulk(HttpServletRequest httpServletRequest,
                                                              @RequestParam("file") MultipartFile file) {

        Locale locale = httpServletRequest.getLocale();

        String token = httpServletRequest.getHeader("Authorization").split(" ")[1];

        String username = jwtTokenProvider.getUsername(token);

        EntityUser entityUser = entityUserService.findEntityUserByUsername(username, locale);

        if (entityUser.getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_inactive", locale);
        }

        if (entityUser.getStatus().equals(Status.DELETED)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_deleted", locale);
        }

        if (entityUser.getEntity().getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "entity_inactive", locale);
        }

        if (entityUser.getIsPasswordReset() == 1) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "password_reset_issue", locale);
        }

        long start = System.nanoTime();

        BulkBillRqDto bulkBillRqDto = bulkBillUploadService.validateMasterBulkBill(file, entityUser, locale);

        System.out.println("validation took " + ((System.nanoTime() - start) / 1_000_000_000) + " seconds");

        bulkBillUploadService.uploadMasterBulkBillRefactored(bulkBillRqDto, entityUser, locale);

        MessageEnvelope messageEnvelope = new MessageEnvelope(HttpStatus.OK, "success", null, locale);

        long end = System.nanoTime();
        long elapsedTime = end - start;
        double elapsedTimeInSecond = (double) elapsedTime / 1_000_000_000;
        System.out.println("Total " + elapsedTimeInSecond + " seconds");

        return new ResponseEntity<>(messageEnvelope, HttpStatus.OK);
    }


    @RequestMapping(value = "/check", method = RequestMethod.POST)
    public ResponseEntity<MessageEnvelope> checkBulk(HttpServletRequest httpServletRequest,
                                                     @RequestParam(value = "fileId", required = false) Long fileId) {

        Locale locale = httpServletRequest.getLocale();

        String token = httpServletRequest.getHeader("Authorization").split(" ")[1];

        String username = jwtTokenProvider.getUsername(token);

        EntityUser entityUser = entityUserService.findEntityUserByUsername(username, locale);

        if (entityUser.getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_inactive", locale);
        }

        if (entityUser.getStatus().equals(Status.DELETED)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_deleted", locale);
        }

        if (entityUser.getEntity().getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "entity_inactive", locale);
        }

        if (Utils.isNullOrEmpty(fileId)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "missing_file_id", locale);
        }

        if (entityUser.getIsPasswordReset() == 1) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "password_reset_issue", locale);
        }

        bulkBillService.checkBulkBillRefactored(fileId, entityUser, locale, billService.findAllBillByBulkId(fileId, locale));

        MessageEnvelope messageEnvelope = new MessageEnvelope(HttpStatus.OK, "success", null, locale);

        return new ResponseEntity<>(messageEnvelope, HttpStatus.OK);
    }

    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public ResponseEntity<MessageEnvelope> deleteBulk(HttpServletRequest httpServletRequest,
                                                      @RequestParam(value = "fileId", required = false) Long fileId) {
        Locale locale = httpServletRequest.getLocale();

        String token = httpServletRequest.getHeader("Authorization").split(" ")[1];

        String username = jwtTokenProvider.getUsername(token);

        EntityUser entityUser = entityUserService.findEntityUserByUsername(username, locale);

        if (entityUser.getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_inactive", locale);
        }

        if (entityUser.getStatus().equals(Status.DELETED)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_deleted", locale);
        }

        if (entityUser.getEntity().getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "entity_inactive", locale);
        }

        if (Utils.isNullOrEmpty(fileId)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "missing_bill_id", locale);
        }

        if (entityUser.getIsPasswordReset() == 1) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "password_reset_issue", locale);
        }

        bulkBillService.deleteBill(fileId, entityUser, locale);

        MessageEnvelope messageEnvelope = new MessageEnvelope(HttpStatus.OK, "success", null, locale);

        return new ResponseEntity<>(messageEnvelope, HttpStatus.OK);
    }


    @RequestMapping(value = "/timeline", method = RequestMethod.GET)
    public ResponseEntity<MessageEnvelope> findBulkTimeline(HttpServletRequest httpServletRequest,
                                                            @RequestParam(value = "fileId", required = false) Long fileId) {
        Locale locale = httpServletRequest.getLocale();

        if (Utils.isNullOrEmpty(fileId)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "missing_bill_id", locale);
        }

        BulkBillResultTimelineDtoRs bulkBillResultTimelineDtoRs = bulkBillService.findBulkBillTimeline(fileId, locale);

        MessageEnvelope messageEnvelope = new MessageEnvelope(HttpStatus.OK, "success", bulkBillResultTimelineDtoRs, locale);

        return new ResponseEntity<>(messageEnvelope, HttpStatus.OK);
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public ResponseEntity<MessageEnvelope> searchBill(HttpServletRequest httpServletRequest,
                                                      @RequestParam(value = "reference", required = false) String reference,
                                                      @RequestParam(value = "outerName", required = false) String outerName,
                                                      @RequestParam(value = "innerName", required = false) String innerName,
                                                      @RequestParam(value = "issueDateFrom", required = false) String issueDateFrom,
                                                      @RequestParam(value = "issueDateTo", required = false) String issueDateTo,
                                                      @RequestParam(value = "bulkStatus", required = false) List<String> bulkStatusList,
                                                      @RequestParam(value = "page", required = false) Integer page,
                                                      @RequestParam(value = "size", required = false) Integer size) {

        Locale locale = httpServletRequest.getLocale();

        Map<String, String[]> searchParameterMap = httpServletRequest.getParameterMap();

        Set<String> keys = searchParameterMap.keySet();

        for (String key : keys) {
            if (!key.equals("reference")
                    && !key.equals("outerName")
                    && !key.equals("innerName")
                    && !key.equals("issueDateFrom")
                    && !key.equals("issueDateTo")
                    && !key.equals("bulkStatus")
                    && !key.equals("page")
                    && !key.equals("size")) {
                throw new HttpServiceException(HttpStatus.BAD_REQUEST, "invalid_request_search_parameter", locale);
            }
        }

        String token = httpServletRequest.getHeader("Authorization").split(" ")[1];

        String username = jwtTokenProvider.getUsername(token);

        EntityUser entityUser = entityUserService.findEntityUserByUsername(username, locale);

        if (entityUser.getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_inactive", locale);
        }

        if (entityUser.getStatus().equals(Status.DELETED)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_deleted", locale);
        }

        if (entityUser.getEntity().getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "entity_inactive", locale);
        }

        if (entityUser.getIsPasswordReset() == 1) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "password_reset_issue", locale);
        }

        SearchBulkBillRqDto searchBulkBillRqDto = new SearchBulkBillRqDto();
        searchBulkBillRqDto.setReference(reference);
        searchBulkBillRqDto.setOuterName(outerName);
        searchBulkBillRqDto.setInnerName(innerName);
        searchBulkBillRqDto.setIssueDateFrom(issueDateFrom);
        searchBulkBillRqDto.setIssueDateTo(issueDateTo);
        searchBulkBillRqDto.setBulkStatus(bulkStatusList);
        searchBulkBillRqDto.setPage(page);
        searchBulkBillRqDto.setSize(size);

        PageDtoRs pageDtoRs = bulkBillService.searchBulkBill(searchBulkBillRqDto, entityUser, locale);

        MessageEnvelope messageEnvelope = new MessageEnvelope(HttpStatus.OK, "success", pageDtoRs, locale);

        return new ResponseEntity<>(messageEnvelope, HttpStatus.OK);
    }

    @RequestMapping(value = "/exportPdf", method = RequestMethod.GET)
    public ResponseEntity<MessageEnvelope> exportPdf(HttpServletRequest httpServletRequest,
                                                     @RequestParam(value = "reference", required = false) String reference,
                                                     @RequestParam(value = "outerName", required = false) String outerName,
                                                     @RequestParam(value = "innerName", required = false) String innerName,
                                                     @RequestParam(value = "issueDateFrom", required = false) String issueDateFrom,
                                                     @RequestParam(value = "issueDateTo", required = false) String issueDateTo,
                                                     @RequestParam(value = "bulkStatus", required = false) List<String> bulkStatusList,
                                                     @RequestParam(value = "page", required = false) Integer page,
                                                     @RequestParam(value = "size", required = false) Integer size) {

        Locale locale = httpServletRequest.getLocale();

        Map<String, String[]> searchParameterMap = httpServletRequest.getParameterMap();

        Set<String> keys = searchParameterMap.keySet();

        for (String key : keys) {
            if (!key.equals("reference")
                    && !key.equals("outerName")
                    && !key.equals("innerName")
                    && !key.equals("issueDateFrom")
                    && !key.equals("issueDateTo")
                    && !key.equals("bulkStatus")) {
                throw new HttpServiceException(HttpStatus.BAD_REQUEST, "invalid_request_search_parameter", locale);
            }
        }

        String token = httpServletRequest.getHeader("Authorization").split(" ")[1];

        String username = jwtTokenProvider.getUsername(token);

        EntityUser entityUser = entityUserService.findEntityUserByUsername(username, locale);

        if (entityUser.getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_inactive", locale);
        }

        if (entityUser.getStatus().equals(Status.DELETED)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_deleted", locale);
        }

        if (entityUser.getEntity().getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "entity_inactive", locale);
        }

        if (entityUser.getIsPasswordReset() == 1) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "password_reset_issue", locale);
        }

        SearchBulkBillRqDto searchBulkBillRqDto = new SearchBulkBillRqDto();
        searchBulkBillRqDto.setReference(reference);
        searchBulkBillRqDto.setOuterName(outerName);
        searchBulkBillRqDto.setInnerName(innerName);
        searchBulkBillRqDto.setIssueDateFrom(issueDateFrom);
        searchBulkBillRqDto.setIssueDateTo(issueDateTo);
        searchBulkBillRqDto.setBulkStatus(bulkStatusList);
        searchBulkBillRqDto.setPage(page);
        searchBulkBillRqDto.setSize(size);

        String pdfUrl = bulkBillService.exportPdf(searchBulkBillRqDto, entityUser, locale);

        MessageEnvelope messageEnvelope = new MessageEnvelope(HttpStatus.OK, "success", pdfUrl, locale);

        return new ResponseEntity<>(messageEnvelope, HttpStatus.OK);
    }


    @RequestMapping(value = "/exportExcel", method = RequestMethod.GET)
    public ResponseEntity<MessageEnvelope> exportExcel(HttpServletRequest httpServletRequest,
                                                       @RequestParam(value = "reference", required = false) String reference,
                                                       @RequestParam(value = "outerName", required = false) String outerName,
                                                       @RequestParam(value = "innerName", required = false) String innerName,
                                                       @RequestParam(value = "issueDateFrom", required = false) String issueDateFrom,
                                                       @RequestParam(value = "issueDateTo", required = false) String issueDateTo,
                                                       @RequestParam(value = "bulkStatus", required = false) List<String> bulkStatusList,
                                                       @RequestParam(value = "page", required = false) Integer page,
                                                       @RequestParam(value = "size", required = false) Integer size) {

        Locale locale = httpServletRequest.getLocale();

        Map<String, String[]> searchParameterMap = httpServletRequest.getParameterMap();

        Set<String> keys = searchParameterMap.keySet();

        for (String key : keys) {
            if (!key.equals("reference")
                    && !key.equals("outerName")
                    && !key.equals("innerName")
                    && !key.equals("issueDateFrom")
                    && !key.equals("issueDateTo")
                    && !key.equals("bulkStatus")
                    && !key.equals("page")
                    && !key.equals("size")) {
                throw new HttpServiceException(HttpStatus.BAD_REQUEST, "invalid_request_search_parameter", locale);
            }
        }

        String token = httpServletRequest.getHeader("Authorization").split(" ")[1];

        String username = "Shoyeb_admin";

        EntityUser entityUser = entityUserService.findEntityUserByUsername(username, locale);

        if (entityUser.getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_inactive", locale);
        }

        if (entityUser.getStatus().equals(Status.DELETED)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_deleted", locale);
        }

        if (entityUser.getEntity().getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "entity_inactive", locale);
        }

        if (entityUser.getIsPasswordReset() == 1) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "password_reset_issue", locale);
        }

        SearchBulkBillRqDto searchBulkBillRqDto = new SearchBulkBillRqDto();
        searchBulkBillRqDto.setReference(reference);
        searchBulkBillRqDto.setOuterName(outerName);
        searchBulkBillRqDto.setInnerName(innerName);
        searchBulkBillRqDto.setIssueDateFrom(issueDateFrom);
        searchBulkBillRqDto.setIssueDateTo(issueDateTo);
        searchBulkBillRqDto.setBulkStatus(bulkStatusList);
        searchBulkBillRqDto.setPage(page);
        searchBulkBillRqDto.setSize(size);

        String excelUrl = bulkBillService.exportExcel(searchBulkBillRqDto, entityUser, locale);

        MessageEnvelope messageEnvelope = new MessageEnvelope(HttpStatus.OK, "success", excelUrl, locale);

        return new ResponseEntity<>(messageEnvelope, HttpStatus.OK);
    }


    @RequestMapping(value = "/bills", method = RequestMethod.GET)
    public ResponseEntity<MessageEnvelope> findAllBillsForBulk(HttpServletRequest httpServletRequest,
                                                               @RequestParam(value = "fileId", required = false) Long fileId,
                                                               @RequestParam(value = "type", required = false) String type,
                                                               @RequestParam(value = "page", required = false) Integer page,
                                                               @RequestParam(value = "size", required = false) Integer size) {

        Locale locale = httpServletRequest.getLocale();

        String token = httpServletRequest.getHeader("Authorization").split(" ")[1];

        String username = jwtTokenProvider.getUsername(token);

        EntityUser entityUser = entityUserService.findEntityUserByUsername(username, locale);

        if (entityUser.getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_inactive", locale);
        }

        if (entityUser.getStatus().equals(Status.DELETED)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_deleted", locale);
        }

        if (entityUser.getEntity().getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "entity_inactive", locale);
        }

        if (entityUser.getIsPasswordReset() == 1) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "password_reset_issue", locale);
        }

        MessageEnvelope messageEnvelope = new MessageEnvelope(HttpStatus.OK, "success", billService.findBillsByFileId(fileId, type, page, size, locale), locale);

        return new ResponseEntity<>(messageEnvelope, HttpStatus.OK);
    }


    @RequestMapping(value = "/remind", method = RequestMethod.POST)
    public ResponseEntity<MessageEnvelope> remindBulkOfBills(HttpServletRequest httpServletRequest,
                                                             @RequestParam(value = "fileId", required = false) Long fileId) {

        Locale locale = httpServletRequest.getLocale();

        String token = httpServletRequest.getHeader("Authorization").split(" ")[1];

        String username = jwtTokenProvider.getUsername(token);

        EntityUser entityUser = entityUserService.findEntityUserByUsername(username, locale);

        if (entityUser.getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_inactive", locale);
        }

        if (entityUser.getStatus().equals(Status.DELETED)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_deleted", locale);
        }

        if (entityUser.getEntity().getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "entity_inactive", locale);
        }

        if (entityUser.getIsPasswordReset() == 1) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "password_reset_issue", locale);
        }

        if (Utils.isNullOrEmpty(fileId)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "missing_file_id", locale);
        }

        new Thread() {
            public void run() {
                billNotificationService.sendBillNotificationReminderBulk(fileId, entityUser, locale);
            }
        }.start();

        MessageEnvelope messageEnvelope = new MessageEnvelope(HttpStatus.OK, "success", null, locale);

        return new ResponseEntity<>(messageEnvelope, HttpStatus.OK);
    }


}
