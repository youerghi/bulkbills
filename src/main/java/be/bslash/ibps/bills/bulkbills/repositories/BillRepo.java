package be.bslash.ibps.bills.bulkbills.repositories;

import be.bslash.ibps.bills.bulkbills.entities.Account;
import be.bslash.ibps.bills.bulkbills.entities.Bill;
import be.bslash.ibps.bills.bulkbills.entities.BulkBill;
import be.bslash.ibps.bills.bulkbills.entities.Entity;
import be.bslash.ibps.bills.bulkbills.enums.BillStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

public interface BillRepo extends JpaRepository<Bill, Long>, JpaSpecificationExecutor<Bill> {
    List<Bill> findAll();

    @Query("select sum(b.remainAmount) from Bill b " +
            "where (b.billStatus = 'APPROVED_BY_SADAD' or b.billStatus ='VIEWED_BY_CUSTOMER' or b.billStatus ='PARTIALLY_PAID_BY_SADAD' or b.billStatus ='PARTIALLY_PAID_BY_COMPANY') " +
            "and (:dueDate is null or b.dueDate <= :dueDate) " +
            "and (:account is null or b.account = :account) " +
            "and b.billStatus <>'DELETED'")
    BigDecimal sumOfRemainingDueBillsTest(
            @Param("dueDate") LocalDateTime dueDate,
            @Param("account") Account account);

    @Query("SELECT " +
            "CASE " +
            "WHEN COUNT(bill)> 0 THEN TRUE " +
            "ELSE FALSE " +
            "END " +
            "FROM Bill bill " +
            "WHERE bill.billNumber=:billNumber " +
            "AND bill.entity=:entity " +
            "AND bill.billStatus <> 'DELETED' " +
            "AND bill.billStatus <> 'CANCELED'")
    boolean checkBillExists(@Param("billNumber") String billNumber,
                            @Param("entity") Entity entity);

    @Query("select bill from Bill bill " +
            "where bill.entity=:entity " +
            "and bill.billNumber is not null " +
            "and bill.billStatus <> 'DELETED' " +
            "and bill.billStatus <> 'CANCELED' ")
    List<Bill> findBillByEntity(@Param("entity") Entity entity);

    List<Bill> findAllByBulkBill(BulkBill bulkBill);

    List<Bill> findAllByBulkBillAndBillStatus(BulkBill bulkBill, BillStatus billStatus);

    Page<Bill> findAllByBulkBill(BulkBill bulkBill, Pageable pageable);


    @Modifying
    @Query("update Bill bill set bill.smsIssueFlag='YES' " +
            "where bill.id=:id")
    void updateSmsFlag(
            @Param("id") Long id);


    @Modifying
    @Query("update Bill bill " +
            "set bill.billStatus=:billStatus , bill.approveDateBySadad=:approvedDate " +
            "where bill.bulkBill.id=:bulkId")
    void updateBillApproved(
            @Param("bulkId") Long bulkId,
            @Param("billStatus") BillStatus billStatus,
            @Param("approvedDate") LocalDateTime approveDateBySadad);

    @Query("select count(distinct b.id) from Bill b " +
            "where (b.billStatus = 'PAID_BY_SADAD' or b.billStatus ='PARTIALLY_PAID_BY_SADAD' or b.billStatus='PAID_BY_COMPANY' or b.billStatus='PARTIALLY_PAID_BY_COMPANY' or b.billStatus = 'SETTLED_BY_SADAD' or b.billStatus = 'SETTLED_BY_COMPANY')" +
            "and b.bulkBill=:bulkBill " +
            "group by b.bulkBill")
    Long countAllPaidBillByBulk(@Param("bulkBill") BulkBill bulkBill);

    @Query("select sum(b.totalAmount) from Bill b " +
            "where (:fromDate is null or b.approveDateBySadad >= :fromDate) " +
            "and (:toDate is null or b.approveDateBySadad <= :toDate) " +
            "and (:entity is null or b.entity = :entity) " +
            "and (:entityActivityId is null or b.entityActivity.id = :entityActivityId) " +
            "and b.billStatus <>'CANCELED'" +
            "group by b.entity")
    BigDecimal volumeByApprovedDate(@Param("fromDate") LocalDateTime fromDate,
                                    @Param("toDate") LocalDateTime toDate,
                                    @Param("entityActivityId") Long entityActivityId,
                                    @Param("entity") Entity entity);

}
