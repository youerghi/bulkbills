package be.bslash.ibps.bills.bulkbills.repositories;

import be.bslash.ibps.bills.bulkbills.entities.BulkBill;
import be.bslash.ibps.bills.bulkbills.entities.BulkBillTimeline;
import be.bslash.ibps.bills.bulkbills.enums.BulkBillStatus;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BulkBillTimelineRepo extends CrudRepository<BulkBillTimeline, Long> {
    List<BulkBillTimeline> findAll();

    BulkBillTimeline findBulkBillTimelineByBulkBillAndBulkBillStatus(BulkBill bulkBill, BulkBillStatus bulkBillStatus);

    List<BulkBillTimeline> findBulkBillTimelineByBulkBill(BulkBill bulkBill);
}
