package be.bslash.ibps.bills.bulkbills.enums;

public enum AccountType {
    IndividualAccount(1), CorporateAccount(2);

    final int type;

    AccountType(int type) {
        this.type = type;
    }

    public static AccountType getValue(int value) {
        if (value == IndividualAccount.type) {
            return IndividualAccount;
        } else if (value == CorporateAccount.type) {
            return CorporateAccount;
        }
        return null;
    }
}
