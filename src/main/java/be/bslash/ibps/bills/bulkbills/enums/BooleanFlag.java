package be.bslash.ibps.bills.bulkbills.enums;

public enum BooleanFlag {
    YES,
    NO;

    public static BooleanFlag getValue(String value) {
        try {
            return BooleanFlag.valueOf(value);
        } catch (Exception ex) {
            return null;
        }
    }

    public static BooleanFlag getValueOrDefault(String value) {
        try {
            return BooleanFlag.valueOf(value);
        } catch (Exception ex) {
            return BooleanFlag.NO;
        }
    }
}

