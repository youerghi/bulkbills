package be.bslash.ibps.bills.bulkbills.services;

import be.bslash.ibps.bills.bulkbills.entities.Entity;
import be.bslash.ibps.bills.bulkbills.entities.EntityField;
import be.bslash.ibps.bills.bulkbills.repositories.EntityFieldRepo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class EntityFieldService {
    private final EntityFieldRepo entityFieldRepo;

    public EntityFieldService(EntityFieldRepo entityFieldRepo) {
        this.entityFieldRepo = entityFieldRepo;
    }

    public EntityField findEntityFieldByIdAndEntity(Long id, Entity entity) {
        return entityFieldRepo.findEntityFieldByIdAndEntity(id,entity);
    }
}
