package be.bslash.ibps.bills.bulkbills.repositories;

import be.bslash.ibps.bills.bulkbills.entities.Bill;
import be.bslash.ibps.bills.bulkbills.entities.BillCustomField;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BillCustomFieldRepo extends CrudRepository<BillCustomField, Long> {

    List<BillCustomField> findAll();

    List<BillCustomField> findAllByBill(Bill bill);

}
