package be.bslash.ibps.bills.bulkbills.repositories;

import be.bslash.ibps.bills.bulkbills.entities.BulkBill;
import be.bslash.ibps.bills.bulkbills.entities.Entity;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface BulkBillRepo extends CrudRepository<BulkBill, Long>, JpaSpecificationExecutor<BulkBill> {

    List<BulkBill> findAll();

    BulkBill findBulkBillById(Long id);

    @Query("select bb From BulkBill bb " +
            "where bb.reference=:reference " +
            "and bb.outerName=:outerName " +
            "and bb.entity=:entity")
    BulkBill findBulkBill(@Param("reference") String reference,
                          @Param("outerName") String outerName,
                          @Param("entity") Entity entity);

}
