package be.bslash.ibps.bills.bulkbills.dto.response;

import java.util.List;

public class BulkBillResultTimelineDtoRs {

    private String reference;
    private String innerName;
    private String outerName;
    private Long numberOfBills;
    private String asyncRqUID;
    private Integer successCount;
    private Integer rejectCount;
    private Integer pendingCount;
    private Long numberOfPaid;
    private Long numberOfUnPaid;
    private List<BulkBillTimelineDtoRs> bulkBillTimeline;

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getInnerName() {
        return innerName;
    }

    public void setInnerName(String innerName) {
        this.innerName = innerName;
    }

    public String getOuterName() {
        return outerName;
    }

    public void setOuterName(String outerName) {
        this.outerName = outerName;
    }

    public Long getNumberOfBills() {
        return numberOfBills;
    }

    public void setNumberOfBills(Long numberOfBills) {
        this.numberOfBills = numberOfBills;
    }

    public String getAsyncRqUID() {
        return asyncRqUID;
    }

    public void setAsyncRqUID(String asyncRqUID) {
        this.asyncRqUID = asyncRqUID;
    }

    public Integer getSuccessCount() {
        return successCount;
    }

    public void setSuccessCount(Integer successCount) {
        this.successCount = successCount;
    }

    public Integer getRejectCount() {
        return rejectCount;
    }

    public void setRejectCount(Integer rejectCount) {
        this.rejectCount = rejectCount;
    }

    public Integer getPendingCount() {
        return pendingCount;
    }

    public void setPendingCount(Integer pendingCount) {
        this.pendingCount = pendingCount;
    }

    public List<BulkBillTimelineDtoRs> getBulkBillTimeline() {
        return bulkBillTimeline;
    }

    public void setBulkBillTimeline(List<BulkBillTimelineDtoRs> bulkBillTimeline) {
        this.bulkBillTimeline = bulkBillTimeline;
    }

    public Long getNumberOfPaid() {
        return numberOfPaid;
    }

    public void setNumberOfPaid(Long numberOfPaid) {
        this.numberOfPaid = numberOfPaid;
    }

    public Long getNumberOfUnPaid() {
        return numberOfUnPaid;
    }

    public void setNumberOfUnPaid(Long numberOfUnPaid) {
        this.numberOfUnPaid = numberOfUnPaid;
    }
}
