package be.bslash.ibps.bills.bulkbills.repositories;

import be.bslash.ibps.bills.bulkbills.entities.District;
import org.springframework.data.repository.CrudRepository;

public interface DistrictRepo extends CrudRepository<District, Long> {
    District findDistrictByCode(String code);
}
