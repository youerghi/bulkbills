package be.bslash.ibps.bills.bulkbills;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BulkBillsApplication {

    public static void main(String[] args) {
        SpringApplication.run(BulkBillsApplication.class, args);
    }

}
