package be.bslash.ibps.bills.bulkbills.utils;

import be.bslash.ibps.bills.bulkbills.configurations.Utf8ResourceBundle;
import be.bslash.ibps.bills.bulkbills.dto.response.BulkBillReportEntityDtoRs;
import be.bslash.ibps.bills.bulkbills.entities.AdCampaign;
import be.bslash.ibps.bills.bulkbills.entities.Bill;
import be.bslash.ibps.bills.bulkbills.entities.BulkBill;
import be.bslash.ibps.bills.bulkbills.entities.Entity;
import be.bslash.ibps.bills.bulkbills.entities.EntityActivity;
import be.bslash.ibps.bills.bulkbills.entities.EntityField;
import be.bslash.ibps.bills.bulkbills.entities.EntityItem;
import be.bslash.ibps.bills.bulkbills.entities.EntityUser;
import be.bslash.ibps.bills.bulkbills.entities.EntityVat;
import be.bslash.ibps.bills.bulkbills.enums.AccountCategory;
import be.bslash.ibps.bills.bulkbills.enums.BillStatus;
import be.bslash.ibps.bills.bulkbills.enums.BillType;
import be.bslash.ibps.bills.bulkbills.enums.BooleanFlag;
import be.bslash.ibps.bills.bulkbills.enums.BulkBillStatus;
import be.bslash.ibps.bills.bulkbills.enums.BulkType;
import be.bslash.ibps.bills.bulkbills.enums.EntityType;
import be.bslash.ibps.bills.bulkbills.enums.EntityUserType;
import be.bslash.ibps.bills.bulkbills.enums.IdIssueAuthority;
import be.bslash.ibps.bills.bulkbills.enums.IdType;
import be.bslash.ibps.bills.bulkbills.enums.PaymentMethod;
import be.bslash.ibps.bills.bulkbills.enums.TypeIdDocument;
import be.bslash.ibps.bills.bulkbills.enums.UserRole;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.StringReader;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class Utils {

    private static final Logger LOGGER = LogManager.getLogger("BACKEND_LOGS");

    public static boolean isValidCustomField(EntityField entityField, String value) {
        if (entityField.getIsRequired().equals(BooleanFlag.YES) && isNullOrEmpty(value)) {
            return false;
        } else {
            return true;
        }
    }

    public static boolean isStringEqual(final String value1, final String value2) {
        return value1.trim().equals(value2.trim());
    }

    public static boolean isNumber(String value) {//[0-9]+
        if (value == null) {
            return true;
        }
        String regex = "^[0-9 ]+$";

        Pattern p = Pattern.compile(regex);

        Matcher m = p.matcher(value);

        return m.matches();
    }

    public static boolean isValidTaxNumber(String taxNumber) {
        if (taxNumber == null || taxNumber.isEmpty()) {
            return true;
        }

        taxNumber = taxNumber.trim();

        if (!isNumber(taxNumber)) {
            return false;
        }

        return taxNumber.length() == 15;
    }

    public static boolean isIdNumberValid(String id, IdType idType) {

        if (id == null || id.isEmpty()) {
            return true;
        }

        if (idType == null || idType.equals(IdType.PAS) || idType.equals(IdType.CRR)) {
            return true;
        }

        id = id.trim();

        if (!id.matches("[0-9]+")) {
            return false;//
        }
        if (id.length() != 10) {
            return false;//
        }
        int type = Integer.parseInt(id.substring(0, 1));
        if (type != 2 && type != 1) {
            return false;//
        }
        int sum = 0;
        for (int i = 0; i < 10; i++) {
            if (i % 2 == 0) {
                String ZFOdd = String.format("%02d", Integer.parseInt(id.substring(i, i + 1)) * 2);
                sum += Integer.parseInt(ZFOdd.substring(0, 1)) + Integer.parseInt(ZFOdd.substring(1, 2));
            } else {
                sum += Integer.parseInt(id.substring(i, i + 1));
            }
        }

        int result = (sum % 10 != 0) ? -1 : type;

        return result != -1;
    }

    public static Long parseLong(String number) {
        try {
            return Long.parseLong(number);
        } catch (Exception ex) {
            return null;
        }
    }

    public static boolean isNullOrEmpty(List value) {
        if (value == null || value.isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isNullOrEmpty(String value) {
        if (value == null || value.trim().isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isNullOrEmpty(Long value) {
        if (value == null || value.equals(0L)) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isEnumValueExist(String value, Class enumClass) {

        if (isNullOrEmpty(value)) {
            return false;
        }

        try {
            switch (enumClass.getSimpleName()) {
                case "IdIssueAuthority":
                    return IdIssueAuthority.valueOf(value) != null;
                case "EntityType":
                    return EntityType.valueOf(value) != null;
                case "TypeIdDocument":
                    return TypeIdDocument.valueOf(value) != null;
                case "IdType":
                    return IdType.valueOf(value) != null;
                case "BillStatus":
                    return BillStatus.valueOf(value) != null;
                case "BillType":
                    return BillType.valueOf(value) != null;
                case "EntityUserType":
                    return EntityUserType.valueOf(value) != null;
                case "BulkBillStatus":
                    return BulkBillStatus.valueOf(value) != null;
                case "UserRole":
                    return UserRole.valueOf(value) != null;
                case "PaymentMethod":
                    return PaymentMethod.valueOf(value) != null;
                case "AccountCategory":
                    return AccountCategory.valueOf(value) != null;
                default:
                    return false;
            }
        } catch (Exception ex) {
            return false;
        }
    }

    public static BigDecimal convertToBigDecimal(String number) {
        try {
            return new BigDecimal(number.trim());
        } catch (Exception ex) {
            return null;
        }
    }

    public static String generateOtp(int len) {
        String numbers = "0123456789";
        Random rndm_method = new Random();

        char[] otpArray = new char[len];

        for (int i = 0; i < len; i++) {
            otpArray[i] = numbers.charAt(rndm_method.nextInt(numbers.length()));
        }
        return String.valueOf(otpArray);
    }

    public static boolean isValidMobileNumber(String phoneStr) {
        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
        Phonenumber.PhoneNumber thePhoneNumber = null;
        try {
            thePhoneNumber = phoneUtil.parse(phoneStr.trim(), "SA");
        } catch (NumberParseException e) {
            return false;
        }
        return phoneUtil.isValidNumber(thePhoneNumber);
    }

    public static String formatMobileNumber(String phoneStr) {
        try {
            PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
            Phonenumber.PhoneNumber thePhoneNumber = null;

            thePhoneNumber = phoneUtil.parse(phoneStr.trim(), "SA");

            return phoneUtil.format(thePhoneNumber, PhoneNumberUtil.PhoneNumberFormat.E164);
        } catch (Exception e) {
            return null;
        }

    }

    public static boolean isValidEmail(String emailStr) {
        if (emailStr == null || emailStr.isEmpty()) {
            return true;
        }
        final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr.trim());
        return matcher.find();
    }


    public static BigDecimal calculateTotalAmountFromVatAndPreviousBalance(BigDecimal subAmount, String vat, BigDecimal previousBalance) {

        if (previousBalance == null) {
            previousBalance = new BigDecimal("0");
        }

        BigDecimal vatValue = null;
        if (vat == null || vat.equals("EXE") || vat.equals("NA")) {
            vatValue = new BigDecimal("0");
        } else {
            vatValue = new BigDecimal(vat);
        }

        BigDecimal vatAmount = subAmount.multiply(vatValue);
        return subAmount.add(vatAmount).add(previousBalance);
    }

    public static BigDecimal calculateSubAmountDiscount(BigDecimal subAmount, BigDecimal discount, String discountType) {
//
//        if (discountType == null || discountType.isEmpty()) {
//            discountType = "PERC";
//        }
//
//        BigDecimal discountAmount = null;
//        switch (discountType) {
//            case "PERC":
//                discountAmount = subAmount.multiply(discount);
//                break;
//            case "FIXED":
//                discountAmount = new BigDecimal(discount.toString());
//                break;
//            default:
//                discountAmount = subAmount.multiply(discount);
//        }
//
//        return subAmount.subtract(discountAmount).abs();
        return subAmount;
    }


    public static String parseDateTime(LocalDateTime localDateTime, String pattern) {
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
            String formattedDateTime = localDateTime.format(formatter);
            return formattedDateTime;
        } catch (Exception ex) {
            return null;
        }

    }

    public static String getSmsBody(String layout,
                                    String entityNameAr,
                                    String entityNameEn,
                                    String totalAmount,
                                    String sadadNumber,
                                    String billNumberEncoded,
                                    String dateTime) {

        String body = layout.replaceAll("ENTITY_NAME_AR", entityNameAr)
                .replaceAll("ENTITY_NAME_EN", entityNameEn)
                .replaceAll("BILL_AMOUNT", totalAmount)
                .replaceAll("SADAD_NUMBER", sadadNumber)
                .replaceAll("BILL_NUMBER_ENCODED", billNumberEncoded)
                .replaceAll("DATE_TIME", dateTime);
        return body;
    }


    public static String roundDecimal(BigDecimal value) {
        if (value == null) {
            return "00.00";
        }
        DecimalFormat df = new DecimalFormat("0.00");
        return df.format(value.doubleValue());
    }

    public static String roundDecimal(BigDecimal value, String pattern) {
        if (value == null) {
            return "0";
        }
        DecimalFormat df = new DecimalFormat(pattern);
        return df.format(value.doubleValue());
    }


    public static File getExcelTemplateFile(Entity entity, BulkType bulkType, Locale locale) throws FileNotFoundException {
        if (locale.getISO3Language().equalsIgnoreCase("eng")) {
            return ResourceUtils.getFile("classpath:billTemplateEn.xlsx");
        } else {
            return ResourceUtils.getFile("classpath:billTemplateEn.xlsx");
        }
    }


    public static LocalDateTime parseDateFromString(String date, String pattern) {
        if (date == null || date.isEmpty()) {
            return null;
        }
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
            LocalDate localDate = LocalDate.parse(date.trim(), formatter);
            return localDate.atStartOfDay();
        } catch (Exception ex) {
            return null;
        }
    }

    public static String[] fillVatArray(List<EntityVat> entityVatList, Locale locale) {

        String vatArray[] = new String[entityVatList.size()];
        for (int index = 0; index < entityVatList.size(); index++) {
            vatArray[index] = entityVatList.get(index).getVat().toString();
        }

        return vatArray;
    }

    public static String[] fillCampaignArray(List<AdCampaign> campaignList, Locale locale) {

        if (campaignList != null && campaignList.size() > 0) {
            String campaignArray[] = new String[campaignList.size()];
            for (int index = 0; index < campaignList.size(); index++) {
                campaignArray[index] = campaignList.get(index).getId() + "-" + campaignList.get(index).getAdName().toString();
            }
            return campaignArray;
        } else {
            return null;
        }
    }

    public static String[] fillItemArray(List<EntityItem> entityItemList, Locale locale) {

        if (entityItemList != null && entityItemList.size() > 0) {
            String itemArray[] = new String[entityItemList.size()];
            for (int index = 0; index < entityItemList.size(); index++) {
                itemArray[index] = entityItemList.get(index).getName();
            }
            return itemArray;
        } else {
            return null;
        }
    }


    public static String[] fillActivityArray(List<EntityActivity> activityList, Locale locale) {

        String vatArray[] = new String[activityList.size()];
        for (int index = 0; index < activityList.size(); index++) {

            String activityName = activityList.get(index).getName(locale);
            if (activityName != null && activityName.length() > 15) {
                activityName = activityName.substring(0, 15) + "...";
            }
            vatArray[index] = activityList.get(index).getCode() + "-" + activityName;
        }

        return vatArray;
    }


    public static JasperReport findReportResourceFile(String reportName, Locale locale) {
        try {
            if (locale.getISO3Language().equalsIgnoreCase("eng")) {
                return JasperCompileManager.compileReport(ResourceUtils.getFile("classpath:" + reportName + "En.jrxml").getPath());
            } else {
                return JasperCompileManager.compileReport(ResourceUtils.getFile("classpath:" + reportName + "En.jrxml").getPath());
            }
        } catch (FileNotFoundException e) {
            return null;
        } catch (JRException e) {
            return null;
        }
    }

    public static JasperPrint fillReport(JasperReport jasperReport, Map parameters, JRBeanCollectionDataSource jrBeanCollectionDataSource) {
        try {
            return JasperFillManager.fillReport(jasperReport, parameters,
                    jrBeanCollectionDataSource);
        } catch (JRException e) {
            return null;
        }
    }


    public static List<BulkBillReportEntityDtoRs> fetchBulkReportEntityList(List<BulkBill> bulkBillList, Locale locale) {
        List<BulkBillReportEntityDtoRs> bulkBillReportEntityDtoRsList = new ArrayList<>();

        bulkBillList.forEach(bulkBill -> {

            BulkBillReportEntityDtoRs bulkBillReportEntityDtoRs = new BulkBillReportEntityDtoRs();
            bulkBillReportEntityDtoRs.setReference(bulkBill.getReference());
            bulkBillReportEntityDtoRs.setInnerName(bulkBill.getInnerName());
            bulkBillReportEntityDtoRs.setOuterName(bulkBill.getOuterName());
            bulkBillReportEntityDtoRs.setCreateDate(bulkBill.getCreateDate());
            bulkBillReportEntityDtoRs.setNumberOfBills(bulkBill.getNumberOfBills());
            bulkBillReportEntityDtoRs.setSuccessCount(bulkBill.getSuccessCount());
            bulkBillReportEntityDtoRs.setRejectCount(bulkBill.getRejectCount());
            bulkBillReportEntityDtoRs.setBulkBillStatus(Utf8ResourceBundle.getString(bulkBill.getBulkBillStatus().name(), locale));

            bulkBillReportEntityDtoRsList.add(bulkBillReportEntityDtoRs);
        });

        return bulkBillReportEntityDtoRsList;
    }


    public static String generateFileReference(EntityUser entityUser) {

        String entityCode = entityUser.getEntity().getCode();

        String firstTwoDigit = entityCode.substring(entityCode.length() - 2);

        LocalDateTime localDateTime = LocalDateTime.now();

        Integer second = localDateTime.getMinute();

        String secondStr = second < 10 ? "0" + second : second.toString();

        Integer minute = localDateTime.getMinute();

        String minuteStr = minute < 10 ? "0" + minute : minute.toString();

        Integer hour = localDateTime.getHour();

        String hourStr = hour < 10 ? "0" + hour : hour.toString();

        Integer day = localDateTime.getDayOfMonth();

        String dayStr = day < 10 ? "0" + day : day.toString();

        Integer month = localDateTime.getMonthValue();

        String monthStr = month < 10 ? "0" + month : month.toString();

        Integer year = localDateTime.getYear();

        String yearStr = year.toString().substring(year.toString().length() - 2);

        return firstTwoDigit + dayStr + yearStr + monthStr + hourStr + minuteStr + secondStr + generateOtp(6);
    }

    public static Document convertStringToXmlDocument(String xml) {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            InputSource is = new InputSource(new StringReader(xml));
            return builder.parse(is);
        } catch (Exception ex) {
            return null;
        }
    }

    public static String getElementValueByTagName(Document document, String tagName) {
        try {

            NodeList nodeList = document.getElementsByTagName(tagName);

            if (nodeList == null) {
                return "";
            }

            for (int index = 0; index < nodeList.getLength(); index++) {

                Node nodeItem = nodeList.item(index);

                if (nodeItem.getNodeType() == Node.ELEMENT_NODE) {

                    Element element = (Element) nodeItem;

                    return element.getTextContent();
                }
            }

            return "";
        } catch (Exception ex) {
            return "";
        }
    }

    public static boolean isDateCorrect(String date, String pattern) {
        if (date == null) {
            return true;
        }
        DateFormat sdf = new SimpleDateFormat(pattern);
        sdf.setLenient(false);
        try {
            sdf.parse(date.trim());
        } catch (ParseException e) {
            return false;
        }
        return true;
    }


    public static LocalDateTime getStartDayOfCurrentMonth() {
        LocalDate localDate = LocalDate.now();

        LocalDateTime dateTime = localDate.withDayOfMonth(1).atStartOfDay();

        return dateTime;
    }

    public static LocalDateTime getCurrentDateTime() {
        return LocalDateTime.now();
    }

    public static String getSmsResponseCode(String responseBody) {
        try {
            JSONObject jsonObject = new JSONObject(responseBody);
            String responseData = jsonObject.getString("data");
            return responseData;
        } catch (Exception ex) {
            return null;
        }
    }

    public static Long getCostSms(String responseBody) {
        try {
            JSONObject jsonObject = new JSONObject(responseBody);
            String responseData = jsonObject.getString("data");
            String[] pointArray = responseData.split(",");
            return parseLong(pointArray[1]);
        } catch (Exception ex) {
            return null;
        }
    }

    public static String generateBillNumber(Entity entity) {

        String commercialRegisterName = entity.getCommercialNameEn();

        String commercialNameArray[] = commercialRegisterName.split("\\s+");

        String billNumber = "";

        if (commercialNameArray.length > 1) {
            billNumber = commercialNameArray[0].charAt(0) + "" + commercialNameArray[1].charAt(0);
        } else {
            billNumber = commercialNameArray[0].charAt(0) + "" + commercialNameArray[0].charAt(1);
        }

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyMMdd");
        String dateTimeBillNumber = LocalDateTime.now().format(formatter);

        billNumber = billNumber + "" + dateTimeBillNumber + "-";

        return billNumber;
    }

    public static boolean isSendNotificationForRole(EntityUser issueEntityUser, EntityUser destinationEntityUser) {


        if (issueEntityUser.getId().equals(destinationEntityUser.getId())) {
            return true;
        }

        EntityUserType issueUserType = issueEntityUser.getEntityUserType();

        EntityUserType destinationUserType = destinationEntityUser.getEntityUserType();

        if (issueUserType.equals(EntityUserType.OFFICER) && !destinationUserType.equals(EntityUserType.OFFICER)) {
            return true;
        }

        if (issueUserType.equals(EntityUserType.SUPERVISOR)) {
            if (destinationUserType.equals(EntityUserType.SUPERVISOR_PLUS)
                    || destinationUserType.equals(EntityUserType.SUPER_ADMIN)
                    || destinationUserType.equals(EntityUserType.SUPER_ADMIN_PLUS)) {
                return true;
            }
        }

        if (issueUserType.equals(EntityUserType.SUPERVISOR_PLUS)) {
            if (destinationUserType.equals(EntityUserType.SUPER_ADMIN)
                    || destinationUserType.equals(EntityUserType.SUPER_ADMIN_PLUS)) {
                return true;
            }
        }

        if (issueUserType.equals(EntityUserType.SUPER_ADMIN)) {
            if (destinationUserType.equals(EntityUserType.SUPER_ADMIN_PLUS)) {
                return true;
            }
        }

        if (issueUserType.equals(EntityUserType.SUPER_ADMIN_PLUS)) {
            if (destinationUserType.equals(EntityUserType.SUPER_ADMIN_PLUS)) {
                return true;
            }
        }

        return false;
    }


    public static boolean isMinimumPartialAmountAllowed(Bill bill) {

        if (bill.getEntity().getIsPartialAllowed().equals(BooleanFlag.NO)) {
            return true;
        }

        if (bill.getIsPartialAllowed() != null && bill.getIsPartialAllowed().equals(BooleanFlag.NO)) {
            return true;
        }

        BigDecimal minimumPartialAmount = bill.getMiniPartialAmount();
        LOGGER.info("minimumPartialAmount: " + minimumPartialAmount);

        BigDecimal minimumActivityAmount = bill.getEntityActivity().getTheLowestAmountPerUploadedBill();

        LOGGER.info("minimumActivityAmount: " + minimumActivityAmount);

        Integer compareResult = minimumPartialAmount.compareTo(minimumActivityAmount);

        if (compareResult == -1) {
            return false;
        }

        return true;
    }

    public static BigDecimal roundDecimalHalfDown(BigDecimal amount) {
        try {
            amount = amount.setScale(2, BigDecimal.ROUND_HALF_DOWN);
            return amount;
        } catch (Exception ex) {
            return null;
        }
    }

    public static boolean isValidSize(String value, Integer min, Integer max) {
        if (value == null) {
            return true;
        }

        int size = value.length();

        return size >= min && size <= max;
    }

}
