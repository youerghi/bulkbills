package be.bslash.ibps.bills.bulkbills.dto.request;

import be.bslash.ibps.bills.bulkbills.entities.BillCustomField;
import be.bslash.ibps.bills.bulkbills.utils.Utils;

import java.time.LocalDateTime;
import java.util.List;

public class SheetLineDTO {
    private String serviceNameValue;
    private String activityValue;
    private String billNumberValue;
    private String mobileNumberValue;
    private String emailValue;
    private String customerNameValue;
    private String customerAddress;
    private String customerTaxNumber;
    private String previousBalance;
    private String vatValue;
    private String amountValue;
    private String creationDateValue;
    private String expireDateValue;
    private String descriptionValue;
    private String customerId;
    private String campaignId;
    private String discountValue;
    private String isPartialValue;
    private String minimumPartialValue;
    private LocalDateTime issueDateTime;
    private LocalDateTime expireDateTime;

    private final List<BillCustomField> billCustomFieldList;
    private Integer rowIndex;

    public SheetLineDTO(String serviceNameValue, String activityValue, String billNumberValue, String mobileNumberValue,
                        String emailValue, String customerNameValue, String customerAddress, String customerTaxNumber,
                        String previousBalance, String vatValue, String amountValue, String creationDateValue,
                        String expireDateValue, String descriptionValue, String customerId, String campaignId,
                        String discountValue, String isPartialValue, String minimumPartialValue,
                        List<BillCustomField> billCustomFieldList, Integer rowIndex) {
        this.serviceNameValue = serviceNameValue;
        this.activityValue = activityValue;
        this.billNumberValue = billNumberValue;
        this.mobileNumberValue = Utils.formatMobileNumber(mobileNumberValue);
        this.emailValue = emailValue;
        this.customerNameValue = customerNameValue;
        this.customerAddress = customerAddress;
        this.customerTaxNumber = customerTaxNumber;
        this.previousBalance = previousBalance;
        this.vatValue = vatValue;
        this.amountValue = amountValue;
        this.creationDateValue = creationDateValue;
        this.expireDateValue = expireDateValue;
        this.descriptionValue = descriptionValue;
        this.customerId = customerId;
        this.campaignId = campaignId;
        this.discountValue = discountValue;
        this.isPartialValue = isPartialValue;
        this.minimumPartialValue = minimumPartialValue;
        this.billCustomFieldList = billCustomFieldList;
        this.rowIndex = rowIndex;
    }

    public String getActivityValue() {
        return activityValue;
    }

    public void setActivityValue(String activityValue) {
        this.activityValue = activityValue;
    }

    public String getBillNumberValue() {
        return billNumberValue;
    }

    public void setBillNumberValue(String billNumberValue) {
        this.billNumberValue = billNumberValue;
    }

    public String getMobileNumberValue() {
        return mobileNumberValue;
    }

    public void setMobileNumberValue(String mobileNumberValue) {
        this.mobileNumberValue = Utils.formatMobileNumber(mobileNumberValue);
    }

    public String getEmailValue() {
        return emailValue;
    }

    public void setEmailValue(String emailValue) {
        this.emailValue = emailValue;
    }

    public String getCustomerNameValue() {
        return customerNameValue;
    }

    public void setCustomerNameValue(String customerNameValue) {
        this.customerNameValue = customerNameValue;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getCustomerTaxNumber() {
        return customerTaxNumber;
    }

    public void setCustomerTaxNumber(String customerTaxNumber) {
        this.customerTaxNumber = customerTaxNumber;
    }

    public String getPreviousBalance() {
        return previousBalance;
    }

    public void setPreviousBalance(String previousBalance) {
        this.previousBalance = previousBalance;
    }

    public String getVatValue() {
        return vatValue;
    }

    public void setVatValue(String vatValue) {
        this.vatValue = vatValue;
    }

    public String getAmountValue() {
        return amountValue;
    }

    public void setAmountValue(String amountValue) {
        this.amountValue = amountValue;
    }

    public String getCreationDateValue() {
        return creationDateValue;
    }

    public void setCreationDateValue(String creationDateValue) {
        this.creationDateValue = creationDateValue;
    }

    public String getExpireDateValue() {
        return expireDateValue;
    }

    public void setExpireDateValue(String expireDateValue) {
        this.expireDateValue = expireDateValue;
    }

    public String getDescriptionValue() {
        return descriptionValue;
    }

    public void setDescriptionValue(String descriptionValue) {
        this.descriptionValue = descriptionValue;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(String campaignId) {
        this.campaignId = campaignId;
    }

    public String getDiscountValue() {
        return discountValue;
    }

    public void setDiscountValue(String discountValue) {
        this.discountValue = discountValue;
    }

    public String getIsPartialValue() {
        return isPartialValue;
    }

    public void setIsPartialValue(String isPartialValue) {
        this.isPartialValue = isPartialValue;
    }

    public String getMinimumPartialValue() {
        return minimumPartialValue;
    }

    public void setMinimumPartialValue(String minimumPartialValue) {
        this.minimumPartialValue = minimumPartialValue;
    }


    public String getServiceNameValue() {
        return serviceNameValue;
    }

    public void setServiceNameValue(String serviceNameValue) {
        this.serviceNameValue = serviceNameValue;
    }

    public Integer getRowIndex() {
        return rowIndex;
    }

    public void setRowIndex(Integer rowIndex) {
        this.rowIndex = rowIndex;
    }

    public LocalDateTime getIssueDateTime() {
        return issueDateTime;
    }

    public void setIssueDateTime(LocalDateTime issueDateTime) {
        this.issueDateTime = issueDateTime;
    }

    public LocalDateTime getExpireDateTime() {
        return expireDateTime;
    }

    public void setExpireDateTime(LocalDateTime expireDateTime) {
        this.expireDateTime = expireDateTime;
    }

    public List<BillCustomField> getBillCustomFieldList() {
        return billCustomFieldList;
    }
}