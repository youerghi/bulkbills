package be.bslash.ibps.bills.bulkbills.enums;


public enum ValueType {
    ALPHA,
    NUMERIC,
    ALPHANUMERIC;


    public static ValueType getValue(String value) {
        try {
            return ValueType.valueOf(value);
        } catch (Exception ex) {
            return null;
        }
    }
}

