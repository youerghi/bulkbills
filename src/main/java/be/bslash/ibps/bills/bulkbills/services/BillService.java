package be.bslash.ibps.bills.bulkbills.services;

import be.bslash.ibps.bills.bulkbills.configurations.Utf8ResourceBundle;
import be.bslash.ibps.bills.bulkbills.dto.response.BillCustomFieldRsDto;
import be.bslash.ibps.bills.bulkbills.dto.response.PageDtoRs;
import be.bslash.ibps.bills.bulkbills.dto.response.SearchBillRsDto;
import be.bslash.ibps.bills.bulkbills.entities.Bill;
import be.bslash.ibps.bills.bulkbills.entities.BillCustomField;
import be.bslash.ibps.bills.bulkbills.entities.BulkBill;
import be.bslash.ibps.bills.bulkbills.exceptions.HttpServiceException;
import be.bslash.ibps.bills.bulkbills.repositories.BillCustomFieldRepo;
import be.bslash.ibps.bills.bulkbills.repositories.BillRepo;
import be.bslash.ibps.bills.bulkbills.repositories.BulkBillRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Service
@Transactional
public class BillService {

    @Autowired
    private BulkBillRepo bulkBillRepo;

    @Autowired
    private BillRepo billRepo;

    @Autowired
    private BillCustomFieldRepo billCustomFieldRepo;

    public List<Bill> findAllBillByBulkId(Long fileId, Locale locale) {

        BulkBill bulkBill = bulkBillRepo.findBulkBillById(fileId);


        return billRepo.findAllByBulkBill(bulkBill);
    }

    public PageDtoRs findBillsByFileId(Long fileId, String type, Integer page, Integer size, Locale locale) {
        if (page == null)
            page = (0);
        if (size == null)
            size = (10);

        BulkBill bulkBill = bulkBillRepo.findBulkBillById(fileId);

        if (bulkBill == null) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bulk_not_found", locale);
        }

        Pageable pageable = PageRequest.of(page, size, Sort.Direction.ASC, "id");

        Page<Bill> billPage = billRepo.findAllByBulkBill(bulkBill, pageable);

        List<Bill> billList = billPage.getContent();

        List<SearchBillRsDto> searchBillRsDtoList = new ArrayList<>();

        billList.forEach(bill -> {
            SearchBillRsDto searchBillRsDto = new SearchBillRsDto();
            searchBillRsDto.setId(bill.getId());
            searchBillRsDto.setBillNumber(bill.getBillNumber());
            searchBillRsDto.setSadadNumber(bill.getSadadNumber());
            searchBillRsDto.setServiceName(bill.getServiceName());
            searchBillRsDto.setServiceDescription(bill.getServiceDescription());
            searchBillRsDto.setIssueDate(bill.getIssueDate());
            searchBillRsDto.setExpireDate(bill.getExpireDate());
            searchBillRsDto.setCreateDate(bill.getCreateDate());
            searchBillRsDto.setSubAmount(bill.getSubAmount());
            searchBillRsDto.setVat(bill.getVat());
            searchBillRsDto.setDiscount(bill.getDiscount());
            searchBillRsDto.setTotalAmount(bill.getTotalRunningAmount());
            searchBillRsDto.setCustomerFullName(bill.getCustomerFullName());
            searchBillRsDto.setCustomerMobileNumber(bill.getCustomerMobileNumber());
            searchBillRsDto.setCustomerEmailAddress(bill.getCustomerEmailAddress());
            searchBillRsDto.setCustomerIdNumber(bill.getCustomerIdNumber());
            searchBillRsDto.setBillStatus(bill.getBillStatus());
            searchBillRsDto.setBillStatusText(Utf8ResourceBundle.getString(bill.getBillStatus().name(), locale));
            searchBillRsDto.setBillType(bill.getBillType());
            searchBillRsDto.setBillTypeText(Utf8ResourceBundle.getString(bill.getBillType().name(), locale));
            searchBillRsDto.setBillSource(bill.getBillSource());
            searchBillRsDto.setBillSourceText(Utf8ResourceBundle.getString(bill.getBillSource().name(), locale));
            searchBillRsDto.setActivityId(bill.getEntityActivity().getId());
            searchBillRsDto.setActivityName(bill.getEntityActivity().getName(locale));
            searchBillRsDto.setPreviousBalance(bill.getCustomerPreviousBalance());
            searchBillRsDto.setCustomerTaxNumber(bill.getCustomerTaxNumber());
            searchBillRsDto.setCustomerAddress(bill.getCustomerAddress());

            List<BillCustomField> billCustomFieldList = billCustomFieldRepo.findAllByBill(bill);

            List<BillCustomFieldRsDto> billCustomFieldRsDtoList = new ArrayList<>();

            if (billCustomFieldList != null && !billCustomFieldList.isEmpty()) {
                billCustomFieldList.forEach(billCustomField -> {
                    billCustomFieldRsDtoList.add((BillCustomFieldRsDto) billCustomField.toDto(locale));
                });
            }

            searchBillRsDto.setCustomFieldList(billCustomFieldRsDtoList);

            searchBillRsDtoList.add(searchBillRsDto);
        });

        if ((type != null && type.equals("P")) || (type != null && type.equals("F"))) {
            return new PageDtoRs(0L, 0, null);
        }

        return new PageDtoRs(billPage.getTotalElements(), billPage.getTotalPages(), searchBillRsDtoList);
    }

}
