package be.bslash.ibps.bills.bulkbills.repositories;

import be.bslash.ibps.bills.bulkbills.dto.projection.AccountProjection;
import be.bslash.ibps.bills.bulkbills.entities.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

public interface AccountRepo extends JpaRepository<Account, Long> {

    @Query(value = "SELECT id, status, account_category as accountCategory, phone_number as phoneNumber, email, " +
            "first_name as firstName, last_name as lastName, id_number as IdNumber, vat_number as vatNumber, " +
            "id_type as IdType, primary_contact_person_number as ContactPersonNumber, account_type as type, " +
            "primary_contact_person_email as ContactPersonEmail, company_name as CompanyName," +
            "type_id_document as TypeIdDocument, account_number as AccountNumber, " +
            "balance, mini_partial_amount as MiniPartialAmount " +
            "FROM account " +
            "WHERE account_number = :accountNumber",
            nativeQuery = true
    )
    AccountProjection findAccount(String accountNumber);


    @Modifying
    @Query("update Account acc set acc.cycleNumber=:cycleNumber " +
            "where acc.id=:accountId")
    void increaseCycleNumberCounter(
            @Param("cycleNumber") Integer cycleNumber,
            @Param("accountId") Long accountId);


    @Query(value = "update Account " +
            "SET cycle_number = cycle_number + 1 " +
            "output inserted.cycle_number " +
            "where id=:id", nativeQuery = true)
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    int increaseCycleNumberCounterAndGet(@Param("id") Long id);

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Modifying(flushAutomatically = true)
    @Query("update Account acc " +
            "set acc.miniPartialAmount=:miniPartialAmount " +
            "where acc.id=:id")
    void updateMiniPartialAmount(@Param("id") Long id, @Param("miniPartialAmount") BigDecimal miniPartialAmount);

    @Modifying
    @Query("update Account acc set acc.balance=:balance " +
            "where acc.id=:accountId")
    void updateBalance(
            @Param("balance") BigDecimal balance,
            @Param("accountId") Long accountId);

}
