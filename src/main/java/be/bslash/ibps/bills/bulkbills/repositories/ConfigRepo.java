package be.bslash.ibps.bills.bulkbills.repositories;

import be.bslash.ibps.bills.bulkbills.entities.Config;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ConfigRepo extends CrudRepository<Config, Long> {
    List<Config> findAll();

    Config findConfigByKey(String key);

}
