package be.bslash.ibps.bills.bulkbills.enums;


public enum Status {
    ACTIVE,
    INACTIVE,
    DELETED,
    PENDING,
    APPROVED,
    REJECTED,
    PAID,
    UNPAID,
    PARTIALLY_PAID;

    public static Status getValue(String value) {
        try {
            return Status.valueOf(value);
        } catch (Exception ex) {
            return null;
        }
    }
}

