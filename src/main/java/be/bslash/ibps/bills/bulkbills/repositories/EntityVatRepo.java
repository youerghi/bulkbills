package be.bslash.ibps.bills.bulkbills.repositories;

import be.bslash.ibps.bills.bulkbills.entities.Entity;
import be.bslash.ibps.bills.bulkbills.entities.EntityVat;
import org.springframework.data.repository.CrudRepository;

import java.math.BigDecimal;
import java.util.List;

public interface EntityVatRepo extends CrudRepository<EntityVat, Long> {
    List<EntityVat> findAll();

    List<EntityVat> findEntityVatByEntity(Entity entity);
    boolean existsEntityVatByEntityAndVat(Entity entity, BigDecimal vat);
}
