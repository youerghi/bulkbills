package be.bslash.ibps.bills.bulkbills.repositories;

import be.bslash.ibps.bills.bulkbills.entities.Bill;
import be.bslash.ibps.bills.bulkbills.entities.BillTimeline;
import be.bslash.ibps.bills.bulkbills.enums.BillStatus;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BillTimelineRepo extends CrudRepository<BillTimeline, Long> {
    List<BillTimeline> findAll();


    BillTimeline findFirstBillTimelineByBillAndBillStatus(Bill bill, BillStatus billStatus);

}
