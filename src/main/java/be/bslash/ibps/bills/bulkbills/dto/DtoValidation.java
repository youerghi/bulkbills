package be.bslash.ibps.bills.bulkbills.dto;

import java.util.Locale;

public interface DtoValidation {
    public void validate(Locale locale);
}
