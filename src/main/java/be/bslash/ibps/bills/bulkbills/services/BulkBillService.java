package be.bslash.ibps.bills.bulkbills.services;


import be.bslash.ibps.bills.bulkbills.configurations.Utf8ResourceBundle;
import be.bslash.ibps.bills.bulkbills.dto.request.BulkBillRqDto;
import be.bslash.ibps.bills.bulkbills.dto.request.SearchBulkBillRqDto;
import be.bslash.ibps.bills.bulkbills.dto.response.BulkBillReportEntityDtoRs;
import be.bslash.ibps.bills.bulkbills.dto.response.BulkBillResultTimelineDtoRs;
import be.bslash.ibps.bills.bulkbills.dto.response.BulkBillTimelineDtoRs;
import be.bslash.ibps.bills.bulkbills.dto.response.PageDtoRs;
import be.bslash.ibps.bills.bulkbills.dto.response.SearchBulkBillRsDto;
import be.bslash.ibps.bills.bulkbills.entities.AdCampaign;
import be.bslash.ibps.bills.bulkbills.entities.AdSettings;
import be.bslash.ibps.bills.bulkbills.entities.Bill;
import be.bslash.ibps.bills.bulkbills.entities.BillCustomField;
import be.bslash.ibps.bills.bulkbills.entities.BillItem;
import be.bslash.ibps.bills.bulkbills.entities.BillTimeline;
import be.bslash.ibps.bills.bulkbills.entities.BulkBill;
import be.bslash.ibps.bills.bulkbills.entities.BulkBillTimeline;
import be.bslash.ibps.bills.bulkbills.entities.Config;
import be.bslash.ibps.bills.bulkbills.entities.Customer;
import be.bslash.ibps.bills.bulkbills.entities.Entity;
import be.bslash.ibps.bills.bulkbills.entities.EntityActivity;
import be.bslash.ibps.bills.bulkbills.entities.EntityField;
import be.bslash.ibps.bills.bulkbills.entities.EntityItem;
import be.bslash.ibps.bills.bulkbills.entities.EntityUser;
import be.bslash.ibps.bills.bulkbills.entities.EntityVat;
import be.bslash.ibps.bills.bulkbills.enums.AdApprovalType;
import be.bslash.ibps.bills.bulkbills.enums.BillCategory;
import be.bslash.ibps.bills.bulkbills.enums.BillSource;
import be.bslash.ibps.bills.bulkbills.enums.BillStatus;
import be.bslash.ibps.bills.bulkbills.enums.BillType;
import be.bslash.ibps.bills.bulkbills.enums.BooleanFlag;
import be.bslash.ibps.bills.bulkbills.enums.BulkBillStatus;
import be.bslash.ibps.bills.bulkbills.enums.BulkType;
import be.bslash.ibps.bills.bulkbills.enums.EntityUserType;
import be.bslash.ibps.bills.bulkbills.enums.Status;
import be.bslash.ibps.bills.bulkbills.exceptions.HttpServiceException;
import be.bslash.ibps.bills.bulkbills.exceptions.ValidationMessageExcel;
import be.bslash.ibps.bills.bulkbills.ftp.FtpHandler;
import be.bslash.ibps.bills.bulkbills.repositories.AdCampaignRepo;
import be.bslash.ibps.bills.bulkbills.repositories.AddSettingsRepo;
import be.bslash.ibps.bills.bulkbills.repositories.BillCustomFieldRepo;
import be.bslash.ibps.bills.bulkbills.repositories.BillItemRepo;
import be.bslash.ibps.bills.bulkbills.repositories.BillRepo;
import be.bslash.ibps.bills.bulkbills.repositories.BillTimelineRepo;
import be.bslash.ibps.bills.bulkbills.repositories.BulkBillRepo;
import be.bslash.ibps.bills.bulkbills.repositories.BulkBillTimelineRepo;
import be.bslash.ibps.bills.bulkbills.repositories.ConfigRepo;
import be.bslash.ibps.bills.bulkbills.repositories.CustomerRepo;
import be.bslash.ibps.bills.bulkbills.repositories.EntityActivityRepo;
import be.bslash.ibps.bills.bulkbills.repositories.EntityFieldRepo;
import be.bslash.ibps.bills.bulkbills.repositories.EntityItemRepo;
import be.bslash.ibps.bills.bulkbills.repositories.EntityRepo;
import be.bslash.ibps.bills.bulkbills.repositories.EntityVatRepo;
import be.bslash.ibps.bills.bulkbills.utils.Utils;
import be.bslash.ibps.bills.bulkbills.ws.WebServiceHelper;
import be.bslash.ibps.bills.bulkbills.ws.WebServiceResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleXlsxReportConfiguration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.DataValidation;
import org.apache.poi.ss.usermodel.DataValidationConstraint;
import org.apache.poi.ss.usermodel.DataValidationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.xssf.usermodel.XSSFDataValidation;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.w3c.dom.Document;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

@Service
@Transactional
public class BulkBillService {

    private static final Logger LOGGER = LogManager.getLogger("BACKEND_LOGS");

    @Autowired
    private EntityActivityRepo entityActivityRepo;

    @Autowired
    private EntityVatRepo entityVatRepo;

    @Autowired
    private Environment environment;

    @Autowired
    private FtpHandler ftpHandler;

    @Autowired
    private EntityFieldRepo entityFieldRepo;

    @Autowired
    private BulkBillRepo bulkBillRepo;

    @Autowired
    private BulkBillTimelineRepo bulkBillTimelineRepo;

    @Autowired
    private BillRepo billRepo;

    @Autowired
    private BillTimelineRepo billTimelineRepo;

    @Autowired
    private ConfigRepo configRepo;

    @Autowired
    private BillCustomFieldRepo billCustomFieldRepo;

    @Autowired
    private WebServiceHelper webServiceHelper;

    @Autowired
    private BillNotificationService billNotificationService;

    @Autowired
    private EntityRepo entityRepo;

    @Autowired
    private AdCampaignRepo adCampaignRepo;

    @Autowired
    private CustomerRepo customerRepo;

    @Autowired
    private AddSettingsRepo addSettingsRepo;

    @Autowired
    private BillItemRepo billItemRepo;

    @Autowired
    private EntityItemRepo entityItemRepo;


    public String generateBulkBill(EntityUser entityUser, BulkType bulkType, Locale locale) {
        List<EntityActivity> entityActivityList = entityActivityRepo.findAllActiveActivityByEntity(entityUser.getEntity());

        //List<AdCampaign> adCampaignList = adCampaignRepo.findAdCampaignByStatusAndEntityAndCategoryType(Status.ACTIVE, entityUser.getEntity(), AdCategoryType.TARGETED);
        List<AdCampaign> adCampaignList = adCampaignRepo.findAdCampaignByApprovalTypeAndStatusAndEntitys(AdApprovalType.APPROVED, Status.ACTIVE, entityUser.getEntity(), LocalDate.now().atStartOfDay());
        List<Customer> customerList = customerRepo.findCustomerByStatusAndEntity(entityUser.getEntity(), Status.ACTIVE);
        List<EntityItem> entityItemList = entityItemRepo.findEntityItemByEntity(entityUser.getEntity());

        AdSettings adSettings = addSettingsRepo.findByEntity(entityUser.getEntity());

        String activityArray[] = Utils.fillActivityArray(entityActivityList, locale);

        String campaignArray[] = Utils.fillCampaignArray(adCampaignList, locale);

        String itemArray[] = Utils.fillItemArray(entityItemList, locale);

        String isPartialArray[] = {"YES", "NO"};

        List<EntityVat> entityVatList = entityVatRepo.findEntityVatByEntity(entityUser.getEntity());

        String vatArray[] = Utils.fillVatArray(entityVatList, locale);

        List<String> vatArrayList = new ArrayList<>();

        if (entityUser.getEntity().getVatExemptedFlag().equals(BooleanFlag.YES)) {
            vatArrayList.add(0, "EXE");
        }

        if (entityUser.getEntity().getVatNaFlag().equals(BooleanFlag.YES)) {
            vatArrayList.add(0, "NA");
        }

        for (String vatValue : vatArray) {
            vatArrayList.add(vatValue);
        }

        vatArray = new String[vatArrayList.size()];
        for (int index = 0; index < vatArray.length; index++) {
            vatArray[index] = vatArrayList.get(index);
        }

        String newFileName = System.currentTimeMillis() + ".xlsx";

        String fileReference = Utils.generateFileReference(entityUser);

        String finalNewFilePath = environment.getProperty("report.archive.path") + newFileName;


        BulkBill bulkBill = new BulkBill();
        bulkBill.setBulkBillStatus(BulkBillStatus.BULK_GENERATED);
        bulkBill.setBulkType(bulkType);
        bulkBill.setEntity(entityUser.getEntity());
        bulkBill.setEntityUser(entityUser);
        bulkBill.setInnerName(fileReference);
        bulkBill.setOuterName(newFileName);
        bulkBill.setNumberOfBills(0L);
        bulkBill.setPendingCount(0);
        bulkBill.setSuccessCount(0);
        bulkBill.setRejectCount(0);
        bulkBill.setAsyncRqUID("-");
        bulkBill.setReference(fileReference);
        bulkBill.setCreateDate(LocalDateTime.now());

        bulkBill = bulkBillRepo.save(bulkBill);

        BulkBillTimeline bulkBillTimeline = new BulkBillTimeline();
        bulkBillTimeline.setBulkBill(bulkBill);
        bulkBillTimeline.setActionDate(LocalDateTime.now());
        bulkBillTimeline.setBulkBillStatus(BulkBillStatus.BULK_GENERATED);
        bulkBillTimeline.setEntityUser(entityUser);
        bulkBillTimeline.setUserReference(entityUser.getUsername());

        bulkBillTimeline = bulkBillTimelineRepo.save(bulkBillTimeline);

        //----------------------------------------------------------------

        try {
            File orginalFile = Utils.getExcelTemplateFile(entityUser.getEntity(), bulkType, locale);

            LOGGER.info("file name: " + orginalFile.getPath());
            LOGGER.info("file name: " + orginalFile.getAbsolutePath());

            FileOutputStream fileOutputStream = new FileOutputStream(new File(finalNewFilePath));

            Files.copy(orginalFile.toPath(), fileOutputStream);

            Workbook workbook = WorkbookFactory.create(new File(finalNewFilePath));

            Sheet sheet = workbook.getSheetAt(0);

            Row fileReferenceRow = sheet.getRow(0);
            Row entityCodeRow = sheet.getRow(1);
            Row timeStampRow = sheet.getRow(2);

            Row billHeaderRow = sheet.getRow(3);

            Cell fileReferenceCell = fileReferenceRow.getCell(1);
            Cell entityCodeRowCell = entityCodeRow.getCell(1);
            Cell timeStampRowCell = timeStampRow.getCell(1);

            Cell fileTypeCell = fileReferenceRow.createCell(19);
            fileTypeCell.setCellValue(bulkType.name());

            fileReferenceCell.setCellValue(fileReference);
            entityCodeRowCell.setCellValue(entityUser.getEntity().getCode());
            timeStampRowCell.setCellValue(Utils.parseDateTime(LocalDateTime.now(), "yyyy-MM-dd hh:mm"));

            CellStyle descriptionCellStyle = billHeaderRow.getCell(10).getCellStyle();

            int cellIndexStart = 19;
            List<EntityField> entityFieldList = entityFieldRepo.findAllActiveEntityFieldByEntity(entityUser.getEntity());
            for (int index = 0; index < entityFieldList.size(); index++) {

                String fieldName = entityFieldList.get(index).getName(locale);
                BooleanFlag isRequired = entityFieldList.get(index).getIsRequired();

                if (isRequired != null && isRequired.equals(BooleanFlag.YES)) {
                    fieldName = fieldName + " * ";
                }

                Cell billField = billHeaderRow.createCell(cellIndexStart);
                billField.setCellValue(fieldName);
                billField.setCellStyle(descriptionCellStyle);

                sheet.setColumnWidth(cellIndexStart, 25 * 256);

                cellIndexStart++;
            }

            BooleanFlag isPartialAllowed = (entityUser.getEntity().getIsPartialAllowed());
            if (isPartialAllowed.equals(BooleanFlag.NO)) {
                Cell isPartialCell = billHeaderRow.getCell(17);
                Cell minimumPartialCell = billHeaderRow.getCell(18);

                isPartialCell.setCellValue("");
                minimumPartialCell.setCellValue("");
            }

            DataValidationHelper helper = sheet.getDataValidationHelper();

            DataValidationConstraint constraintActivity = helper.createExplicitListConstraint(activityArray);

            DataValidationConstraint constraintVat = helper.createExplicitListConstraint(vatArray);

            DataValidationConstraint constraintPartial = helper.createExplicitListConstraint(isPartialArray);


            CellRangeAddressList activityList = new CellRangeAddressList(4, 1000, 5, 5);

            CellRangeAddressList vatList = new CellRangeAddressList(4, 1000, 3, 3);

            CellRangeAddressList partialList = new CellRangeAddressList(4, 1000, 17, 17);


            DataValidation dataValidationActivity = helper.createValidation(constraintActivity, activityList);

            if (dataValidationActivity instanceof XSSFDataValidation) {
                dataValidationActivity.setSuppressDropDownArrow(true);
                dataValidationActivity.setShowErrorBox(true);
            } else {
                dataValidationActivity.setSuppressDropDownArrow(false);
            }

            sheet.addValidationData(dataValidationActivity);

            DataValidation dataValidationVat = helper.createValidation(constraintVat, vatList);

            if (dataValidationVat instanceof XSSFDataValidation) {
                dataValidationVat.setSuppressDropDownArrow(true);
                dataValidationVat.setShowErrorBox(true);
                dataValidationVat.createErrorBox("Error", "Not Valid");
            } else {
                dataValidationVat.setSuppressDropDownArrow(false);
            }

            sheet.addValidationData(dataValidationVat);


            if (itemArray != null) {
                CellRangeAddressList itemList = new CellRangeAddressList(4, 1000, 0, 0);

                DataValidationConstraint constraintItem = helper.createExplicitListConstraint(itemArray);

                DataValidation dataValidationItem = helper.createValidation(constraintItem, itemList);

                if (dataValidationItem instanceof XSSFDataValidation) {
                    dataValidationItem.setSuppressDropDownArrow(true);
                    dataValidationItem.setShowErrorBox(true);
                } else {
                    dataValidationItem.setSuppressDropDownArrow(false);
                }

                sheet.addValidationData(dataValidationItem);
            }

            if (adSettings != null && adSettings.getCampaignActive().equals(Status.ACTIVE) && entityUser.getEntity().getAdModuleEnable().equals(Status.ACTIVE) &&
                    !adSettings.getMasterAdvEnable().equals(Status.ACTIVE) && campaignArray != null) {

                DataValidationConstraint constraintAdCampaign = helper.createExplicitListConstraint(campaignArray);
                CellRangeAddressList campaignList = new CellRangeAddressList(4, 1000, 16, 16);
                DataValidation dataValidationCampaign = helper.createValidation(constraintAdCampaign, campaignList);

                if (dataValidationCampaign instanceof XSSFDataValidation) {
                    dataValidationCampaign.setSuppressDropDownArrow(true);
                    dataValidationCampaign.setShowErrorBox(true);
                } else {
                    dataValidationCampaign.setSuppressDropDownArrow(false);
                }
                sheet.addValidationData(dataValidationCampaign);
            }

            DataValidation dataValidationPartial = helper.createValidation(constraintPartial, partialList);

            if (isPartialAllowed.equals(BooleanFlag.YES) && dataValidationPartial instanceof XSSFDataValidation) {
                dataValidationPartial.setSuppressDropDownArrow(true);
                dataValidationPartial.setShowErrorBox(true);
            } else {
                dataValidationPartial.setSuppressDropDownArrow(false);
            }

            sheet.addValidationData(dataValidationPartial);

            workbook.write(fileOutputStream);
            workbook.close();
            fileOutputStream.close();

            File reportFile = new File(finalNewFilePath);

            ftpHandler.uploadFile(reportFile);

//            reportFile.delete();

        } catch (Exception ex) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "error_while_generating_bulk", ex.getMessage(), locale);
        }

        //----------------------------------------------------------------

        return environment.getProperty("report.archive.url") + newFileName;
    }

    public BulkBillRqDto validateMasterBulkBill(MultipartFile multipartFile, EntityUser entityUser, Locale locale) {

        List<ValidationMessageExcel> validationMessageExcelList = new ArrayList<>();

        BulkBillRqDto bulkBillRqDto = new BulkBillRqDto();

        Workbook workbook = null;
        try {
            workbook = WorkbookFactory.create(multipartFile.getInputStream());
        } catch (IOException e) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "error_while_reading_file", locale);
        }

        List<EntityField> entityFieldList = entityFieldRepo.findAllActiveEntityFieldByEntity(entityUser.getEntity());

        List<EntityActivity> entityActivityList = entityActivityRepo.findAllActiveActivityByEntity(entityUser.getEntity());
        Map<String, EntityActivity> entityActivityMap = new HashMap<>();
        entityActivityList.forEach(entityActivity -> {
            entityActivityMap.put(entityActivity.getCode(), entityActivity);
        });

        List<Bill> currentBillList = billRepo.findBillByEntity(entityUser.getEntity());
        Map<String, Bill> entityBillsMapMap = new HashMap<>();
        currentBillList.forEach(bill -> {
            if (bill.getBillNumber() != null && !bill.getBillNumber().isEmpty()) {
                entityBillsMapMap.put(bill.getBillNumber(), bill);
            }
        });

        List<EntityVat> entityVatList = entityVatRepo.findEntityVatByEntity(entityUser.getEntity());
        Map<String, EntityVat> entityVatMap = new HashMap<>();
        entityVatList.forEach(entityVat -> {
            entityVatMap.put(entityVat.getVat().toString(), entityVat);
        });


        Sheet billsSheet = workbook.getSheetAt(0);

        if (billsSheet == null) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "empty_sheet", locale);
        }

        if (billsSheet.getLastRowNum() - 3 <= 0) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "empty_bills", locale);
        }

        Row fileReferenceRow = billsSheet.getRow(0);

        Cell fileReferenceCell = fileReferenceRow.getCell(1);
        if (fileReferenceCell == null) {
            ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("fileReference", "invalid_reference_cell", 0, 1, locale);
            validationMessageExcelList.add(validationMessageExcel);
        }

        String fileReferenceValue = fileReferenceCell.getStringCellValue();

        String outerName = multipartFile.getOriginalFilename();

        BulkBill bulkBill = bulkBillRepo.findBulkBill(fileReferenceValue, outerName, entityUser.getEntity());

        if (bulkBill == null) {
            ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("fileReference", "file_reference_not_found", 0, 1, locale);
            validationMessageExcelList.add(validationMessageExcel);
        }

        if (bulkBill != null && !bulkBill.getBulkBillStatus().equals(BulkBillStatus.BULK_GENERATED)) {
            ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("fileReference", "file_reference_already_uploaded", 0, 1, locale);
            validationMessageExcelList.add(validationMessageExcel);
        }

        if (bulkBill != null) {
            bulkBillRqDto.setBulkBillId(bulkBill.getId());
        }

        Cell bulkTypeCell = fileReferenceRow.getCell(19);

        BulkType bulkType = BulkType.getValue(bulkTypeCell.getStringCellValue());

        List<Bill> billList = new ArrayList<>();

        if (bulkType == null || bulkType.name().equals("MASTER_BULK")) {

            BigDecimal sumAmount = new BigDecimal("0");

            Long billNumberSequence = entityUser.getEntity().getAutoGenerationBillNumberSequence();

            for (int rowIndex = 4; rowIndex <= billsSheet.getLastRowNum(); rowIndex++) {

                Row billRow = billsSheet.getRow(rowIndex);

                if (billRow == null) {
                    continue;
                }

//                if (billRow.getLastCellNum() <= 10) {
//                    ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("bill", "invalid_bill_cell_size", (rowIndex + 1), 1, locale);
//                    validationMessageExcelList.add(validationMessageExcel);
//                }

                DataFormatter formatter = new DataFormatter();

                Cell serviceNameCell = billRow.getCell(0);
                Cell mobileNumberCell = billRow.getCell(1);
                Cell amountCell = billRow.getCell(2);
                Cell vatCell = billRow.getCell(3);
                Cell discountCell = billRow.getCell(4);
                Cell activityCell = billRow.getCell(5);
                Cell billNumberCell = billRow.getCell(6);
                Cell customerNameCell = billRow.getCell(7);
                Cell emailCell = billRow.getCell(8);
                Cell customerIdCell = billRow.getCell(9);
                Cell customerAddressCell = billRow.getCell(10);
                Cell customerTaxNumberCell = billRow.getCell(11);
                Cell creationDateCell = billRow.getCell(12);
                Cell expireDateCell = billRow.getCell(13);
                Cell descriptionCell = billRow.getCell(14);
                Cell previousBalanceCell = billRow.getCell(15);
                Cell campaingCell = billRow.getCell(16);
                Cell isPartialCell = billRow.getCell(17);
                Cell minimumPartialCell = billRow.getCell(18);

                String serviceNameValue = (serviceNameCell == null) ? (null) : (formatter.formatCellValue(serviceNameCell));
                String activityValue = (activityCell == null) ? (null) : (formatter.formatCellValue(activityCell));
                String billNumberValue = (billNumberCell == null) ? (null) : (formatter.formatCellValue(billNumberCell));
                String mobileNumberValue = (mobileNumberCell == null) ? (null) : (formatter.formatCellValue(mobileNumberCell));
                String emailValue = (emailCell == null) ? (null) : (formatter.formatCellValue(emailCell));
                String customerNameValue = (customerNameCell == null) ? (null) : (formatter.formatCellValue(customerNameCell));
                String customerAddress = (customerAddressCell == null) ? (null) : (formatter.formatCellValue(customerAddressCell));
                String customerTaxNumber = (customerTaxNumberCell == null) ? (null) : (formatter.formatCellValue(customerTaxNumberCell));
                String previousBalance = (previousBalanceCell == null) ? "0" : (formatter.formatCellValue(previousBalanceCell));
                String vatValue = (vatCell == null) ? (null) : (formatter.formatCellValue(vatCell));
                String amountValue = (amountCell == null) ? (null) : (formatter.formatCellValue(amountCell));
                String creationDateValue = (creationDateCell == null) ? (null) : (formatter.formatCellValue(creationDateCell));
                String expireDateValue = (expireDateCell == null) ? (null) : (formatter.formatCellValue(expireDateCell));
                String descriptionValue = (descriptionCell == null) ? (null) : (formatter.formatCellValue(descriptionCell));
                String customerId = (customerIdCell == null) ? (null) : (formatter.formatCellValue(customerIdCell));
                String campaignId = (campaingCell == null) ? (null) : (formatter.formatCellValue(campaingCell));
                String discountValue = (discountCell == null) ? (null) : (formatter.formatCellValue(discountCell));
                String isPartialValue = (discountCell == null) ? (null) : (formatter.formatCellValue(isPartialCell));
                String minimumPartialValue = (discountCell == null) ? (null) : (formatter.formatCellValue(minimumPartialCell));


                if (!Utils.isNullOrEmpty(billNumberValue)) {
                    Bill bill = entityBillsMapMap.get(billNumberValue);
                    if (bill != null) {
                        ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("billNumber", "bill number already exist", (rowIndex + 1), 2, locale);
                        validationMessageExcelList.add(validationMessageExcel);
                    }
                }

                if (entityUser.getEntity().getIsAutoGenerateBillNumber().equals(BooleanFlag.YES) && Utils.isNullOrEmpty(billNumberValue)) {
                    billNumberSequence += 1;
                    billNumberValue = Utils.generateBillNumber(entityUser.getEntity()) + billNumberSequence;
                }

                if (Utils.isNullOrEmpty(serviceNameValue)) {
                    ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("serviceName", "service_name_empty", (rowIndex + 1), 2, locale);
                    validationMessageExcelList.add(validationMessageExcel);
                }

                if (Utils.isNullOrEmpty(customerNameValue)) {
                    ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("customerName", "invalid_customer_name", (rowIndex + 1), 3, locale);
                    validationMessageExcelList.add(validationMessageExcel);
                }

                if (Utils.isNullOrEmpty(mobileNumberValue)) {
                    ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("mobileNumber", "customer_mobile_number_empty", (rowIndex + 1), 4, locale);
                    validationMessageExcelList.add(validationMessageExcel);
                }

                if (!Utils.isNullOrEmpty(mobileNumberValue) && !Utils.isValidMobileNumber(mobileNumberValue)) {
                    ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("mobileNumber", "invalid_mobile_number", (rowIndex + 1), 4, locale);
                    validationMessageExcelList.add(validationMessageExcel);
                }

                if ((emailValue != null && !emailValue.equals("")) && !Utils.isValidEmail(emailValue)) {
                    ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("email", "invalid_email", (rowIndex + 1), 5, locale);
                    validationMessageExcelList.add(validationMessageExcel);
                }

                if (!Utils.isNullOrEmpty(customerId) && customerId.length() < 10) {
                    ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("customerId", "customer_id_number_not_valid", (rowIndex + 1), 6, locale);
                    validationMessageExcelList.add(validationMessageExcel);
                }

                if (Utils.convertToBigDecimal(amountValue) == null || Utils.isNullOrEmpty(amountValue)) {
                    ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("amount", "invalid_amount_value", (rowIndex + 1), 7, locale);
                    validationMessageExcelList.add(validationMessageExcel);
                }

                LocalDateTime issueDateTime = Utils.parseDateFromString(creationDateValue, "yyyy-MM-dd");
                if (issueDateTime == null) {
                    ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("issueDate", "issue_date_invalid", (rowIndex + 1), 11, locale);
                    validationMessageExcelList.add(validationMessageExcel);
                }

                if (!Utils.isDateCorrect(creationDateValue, "yyyy-MM-dd")) {
                    ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("issueDate", "date_invalid", (rowIndex + 1), 11, locale);
                    validationMessageExcelList.add(validationMessageExcel);
                }

                LocalDateTime expireDateTime = Utils.parseDateFromString(expireDateValue, "yyyy-MM-dd");
                if (expireDateTime == null) {
                    ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("expireDate", "expire_date_invalid", (rowIndex + 1), 12, locale);
                    validationMessageExcelList.add(validationMessageExcel);
                }

                if (!Utils.isDateCorrect(expireDateValue, "yyyy-MM-dd")) {
                    ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("expireDate", "date_invalid", (rowIndex + 1), 12, locale);
                    validationMessageExcelList.add(validationMessageExcel);
                }

                if (issueDateTime != null && expireDateTime != null && issueDateTime.isAfter(expireDateTime)) {
                    ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("expireDate", "expire_date_after_issue", (rowIndex + 1), 12, locale);
                    validationMessageExcelList.add(validationMessageExcel);
                }

                if (expireDateTime != null && expireDateTime.isBefore(LocalDate.now().atStartOfDay())) {
                    ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("expireDate", "expire_date_is_passed", (rowIndex + 1), 12, locale);
                    validationMessageExcelList.add(validationMessageExcel);
                }

                if (Utils.isNullOrEmpty(activityValue)) {
                    ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("activity", "activity_value_empty", (rowIndex + 1), 14, locale);
                    validationMessageExcelList.add(validationMessageExcel);
                }

                if (issueDateTime != null && expireDateTime != null && issueDateTime.equals(expireDateTime)) {
                    ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("expireDate", "issuance_expire_date_same", (rowIndex + 1), 12, locale);
                    validationMessageExcelList.add(validationMessageExcel);
                }

                if (Utils.isNullOrEmpty(vatValue)) {
                    ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("vat", "vat_value_empty", (rowIndex + 1), 15, locale);
                    validationMessageExcelList.add(validationMessageExcel);
                    vatValue = "0";
                }

                if ((customerTaxNumber != null) && (!customerTaxNumber.isEmpty()) && (customerTaxNumber.length() != 15)) {
                    ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("customerTaxNumber", "VAT number length not correct", (rowIndex + 1), 5, locale);
                    validationMessageExcelList.add(validationMessageExcel);
                }


                BooleanFlag vatExempted = BooleanFlag.NO;
                BooleanFlag vatNa = BooleanFlag.NO;
                BigDecimal vatAmount = null;
                switch (vatValue) {
                    case "EXE":
                        vatValue = "0";
                        vatAmount = new BigDecimal("0");
                        vatExempted = (BooleanFlag.YES);
                        vatNa = (BooleanFlag.NO);
                        break;
                    case "NA":
                        vatValue = "0";
                        vatAmount = new BigDecimal("0");
                        vatExempted = (BooleanFlag.NO);
                        vatNa = (BooleanFlag.YES);
                        break;
                    default:
                        vatExempted = (BooleanFlag.NO);
                        vatNa = (BooleanFlag.NO);
                        vatAmount = new BigDecimal(vatValue);
                        LOGGER.info("vatAmount: " + vatAmount);
//                        if (entityVatList == null || entityVatList.isEmpty()) {
//                            ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("vat", "vat_value_not_allowed", (rowIndex + 1), 15, locale);
//                            validationMessageExcelList.add(validationMessageExcel);
//                        }
                }

                EntityVat entityVat = entityVatMap.get(vatAmount.toString());
                if (entityVat == null && vatNa.equals(BooleanFlag.NO) && vatExempted.equals(BooleanFlag.NO)) {
                    ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("vat", "vat value is not part of selected vat list", (rowIndex + 1), 8, locale);
                    validationMessageExcelList.add(validationMessageExcel);
                }

                mobileNumberValue = Utils.formatMobileNumber(mobileNumberValue);

                BigDecimal billAmount = Utils.convertToBigDecimal(amountValue);

                BigDecimal discount = Utils.convertToBigDecimal(discountValue);

                BigDecimal previousBalanceAmount = Utils.convertToBigDecimal(previousBalance);

                BigDecimal minimumPartialAmount = Utils.convertToBigDecimal(minimumPartialValue);

                if (billAmount == null) {
                    billAmount = new BigDecimal("0");
                }

                if (discount == null) {
                    discount = new BigDecimal("0");
                }

                discount = discount.divide(new BigDecimal(100));

                if (previousBalanceAmount == null) {
                    previousBalanceAmount = new BigDecimal("0");
                }

                if (minimumPartialAmount == null) {
                    minimumPartialAmount = entityUser.getEntity().getMiniPartialDefaultValue();
                }

                String discountType = "PERC";

                BigDecimal discountAmount = billAmount.multiply(discount);

                BigDecimal subAmountAfterDiscount = Utils.calculateSubAmountDiscount(billAmount, discount, discountType);

                subAmountAfterDiscount = subAmountAfterDiscount.subtract(discountAmount);

                BigDecimal totalAmount = Utils.calculateTotalAmountFromVatAndPreviousBalance(subAmountAfterDiscount, vatAmount.toString(), previousBalanceAmount);

                sumAmount = sumAmount.add(totalAmount);

                List<BillCustomField> billCustomFieldList = new ArrayList<>();
                for (int customFieldIndex = 19, entityFieldIndex = 0; customFieldIndex <= 28; customFieldIndex++, entityFieldIndex++) {

                    Cell cell = billRow.getCell(customFieldIndex);


                    EntityField entityField = null;
                    try {
                        LOGGER.info("entityFieldIndex: " + entityFieldIndex);
                        entityField = entityFieldList.get(entityFieldIndex);
                    } catch (Exception ex) {
                        LOGGER.info("ex: " + ex.getMessage());
                        continue;
                    }


                    LOGGER.info("entityField: " + entityField);

                    if (cell == null) {
//                        if (entityField.getIsRequired().equals(BooleanFlag.YES)) {
//                            ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel(entityField.getFieldNameEn(), entityField.getFieldNameEn() + " empty", (rowIndex + 1), 3, locale);
//                            validationMessageExcelList.add(validationMessageExcel);
//                        }
                        continue;
                    }

                    String value = formatter.formatCellValue(cell);

                    LOGGER.info("value: " + value);
                    LOGGER.info("entityField: " + entityField.getId());

//                    if(!Utils.isValidCustomField(entityField,value)){
//                        ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel(entityField.getFieldNameEn(), entityField.getFieldNameEn() +" empty", (rowIndex + 1), 3, locale);
//                        validationMessageExcelList.add(validationMessageExcel);
//                    }
//
//                    if(!Utils.isValidCustomField(entityField.getValueType(),value)){
//                        ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel(entityField.getFieldNameEn(), entityField.getFieldNameEn() +" not valid", (rowIndex + 1), 3, locale);
//                        validationMessageExcelList.add(validationMessageExcel);
//                    }

                    BillCustomField billCustomField = new BillCustomField();
                    billCustomField.setEntityField(entityField);
                    billCustomField.setValue(value);

                    billCustomFieldList.add(billCustomField);

                }

                String activityId = "0";
                if (activityValue != null && !activityValue.isEmpty()) {
                    activityId = (activityValue.split("-")[0]);
                    if (activityId == null) {
                        ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("activity", "activity_value_empty", (rowIndex + 1), 14, locale);
                        validationMessageExcelList.add(validationMessageExcel);
                    }
                }

                EntityActivity entityActivity = entityActivityMap.get(activityId);

                if (entityActivity != null) {

                    LOGGER.info("************************************");
                    LOGGER.info("totalAmount: " + totalAmount);
                    LOGGER.info("BillMaxAmountPerBill: " + entityActivity.getTheHighestAmountPerUploadedBill());

                    if (entityActivity.getTheLowestAmountPerUploadedBill() != null) {
                        if (totalAmount.compareTo(entityActivity.getTheLowestAmountPerUploadedBill()) == -1) {
                            ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("amount", "bill_amount_limit_per_bill", (rowIndex + 1), 8, locale);
                            validationMessageExcelList.add(validationMessageExcel);
                        }
                    }

                    if (entityActivity.getTheHighestAmountPerUploadedBill() != null) {
                        //checking bill limit per bill
                        if (totalAmount.compareTo(entityActivity.getTheHighestAmountPerUploadedBill()) == 1) {
                            ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("amount", "bill_amount_limit_per_bill", (rowIndex + 1), 8, locale);
                            validationMessageExcelList.add(validationMessageExcel);
                        }
                    }

                    if (entityActivity.getMonthlyHighestValueOfExpectedUploadedBills() != null) {
                        //checking bill limit per month
                        LocalDateTime startDayOfMonth = Utils.getStartDayOfCurrentMonth();
                        LocalDateTime currentDay = Utils.getCurrentDateTime();

                        LOGGER.info("startDayOfMonth: " + startDayOfMonth);
                        LOGGER.info("currentDay: " + currentDay);

//                        BigDecimal totalAmountForCurrentMonth = billRepo.volumeByApprovedDate(startDayOfMonth, currentDay, entityActivity, entityUser.getEntity());
//                        if (totalAmountForCurrentMonth == null) {
//                            totalAmountForCurrentMonth = new BigDecimal("0");
//                        }
//
//                        LOGGER.info("************************************");
//                        LOGGER.info("totalAmountForCurrentMonth: " + totalAmountForCurrentMonth);
//                        LOGGER.info("BillMaxAmountPerMonth: " + entityActivity.getMonthlyHighestValueOfExpectedUploadedBills());
//
//                        if ((sumAmount.add(totalAmountForCurrentMonth)).compareTo(entityActivity.getMonthlyHighestValueOfExpectedUploadedBills()) == 1) {
//                            ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("amount", "bill_amount_limit_per_month", (rowIndex + 1), 8, locale);
//                            validationMessageExcelList.add(validationMessageExcel);
//                        }
                    }
                } else {
                    ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("activity", "activity_value_empty", (rowIndex + 1), 14, locale);
                    validationMessageExcelList.add(validationMessageExcel);
                }

                Bill bill = new Bill();
                bill.setServiceName(serviceNameValue);
                bill.setEntityActivity(entityActivity);
                bill.setBillNumber(billNumberValue);
                bill.setCustomerMobileNumber(mobileNumberValue);
                bill.setCustomerEmailAddress(emailValue);
                bill.setCustomerFullName(customerNameValue);
                bill.setCustomerAddress(customerAddress);
                bill.setCustomerTaxNumber(customerTaxNumber);
                bill.setCustomerIdNumber(customerId);
                bill.setVat(vatAmount != null ? vatAmount : new BigDecimal(0));
                bill.setSubAmount(billAmount);
                bill.setEntity(entityUser.getEntity());
                bill.setEntityUser(entityUser);
                bill.setDiscount(new BigDecimal(0));
                bill.setCustomerPreviousBalance(previousBalanceAmount);
                bill.setTotalAmount(totalAmount);
                bill.setIssueDate(issueDateTime);
                bill.setExpireDate(expireDateTime);
                bill.setBillStatus(BillStatus.MAKED);
                bill.setBillSource(BillSource.FILE);
                bill.setBillType(BillType.MASTER_BILL);
                bill.setBulkBill(bulkBill);
                bill.setServiceDescription(descriptionValue);
                bill.setBillCustomFieldList(billCustomFieldList);
                bill.setVatNaFlag(vatNa);
                bill.setVatExemptedFlag(vatExempted);
                bill.setDiscountType(discountType);
                bill.setDiscount(Utils.convertToBigDecimal(discountValue) != null ? Utils.convertToBigDecimal(discountValue) : BigDecimal.ZERO);
                bill.setIsPartialAllowed(BooleanFlag.getValue(isPartialValue));
                bill.setMiniPartialAmount(minimumPartialAmount);
                bill.setLogo(entityUser.getEntity().getLogo());
                bill.setStamp(entityUser.getEntity().getStamp());

                if (bill.getIsPartialAllowed() != null && bill.getIsPartialAllowed().equals(BooleanFlag.YES)) {
                    bill.setBillCategory(BillCategory.ONE_OFF_PARTIAL);
                } else {
                    bill.setBillCategory(BillCategory.INSTANTLY);
                    bill.setIsPartialAllowed(BooleanFlag.NO);
                }

                if (!Utils.isNullOrEmpty(campaignId)) {

                    if (campaignId.split("-").length == 1) {
                        ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("campaignName", "not_valid_campaign_name", (rowIndex + 1), 16, locale);
                        validationMessageExcelList.add(validationMessageExcel);
                    } else {
                        bill.setCampaignId(Integer.parseInt(campaignId.split("-")[0]));
                    }
                }

                if (entityUser.getEntity().getIsPartialAllowed().equals(BooleanFlag.NO)) {
                    bill.setIsPartialAllowed(BooleanFlag.NO);
                    bill.setBillCategory(BillCategory.INSTANTLY);
                }

                billList.add(bill);

                if (!Utils.isNullOrEmpty(bill.getBillNumber())) {
                    entityBillsMapMap.put(bill.getBillNumber(), bill);
                }


                if (!Utils.isMinimumPartialAmountAllowed(bill)) {
                    ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("minimumPartialAmount", "minimum partial amount should not less than activity minimum amount", (rowIndex + 1), 12, locale);
                    validationMessageExcelList.add(validationMessageExcel);
                }
            }

            entityRepo.increaseBillNumberCounter(billNumberSequence, entityUser.getEntity().getId());

        } else {
            BigDecimal sumAmount = new BigDecimal("0");

            Map<String, Bill> billMap = new HashMap<>();

            Long billNumberSequence = entityUser.getEntity().getAutoGenerationBillNumberSequence();

            for (int rowIndex = 4; rowIndex <= billsSheet.getLastRowNum(); rowIndex++) {

                Row billRow = billsSheet.getRow(rowIndex);

                if (billRow == null) {
                    continue;
                }

                DataFormatter formatter = new DataFormatter();

                Cell serviceNameCell = billRow.getCell(0);
                Cell mobileNumberCell = billRow.getCell(1);
                Cell amountCell = billRow.getCell(2);
                Cell vatCell = billRow.getCell(3);
                Cell discountCell = billRow.getCell(4);
                Cell activityCell = billRow.getCell(5);
                Cell billNumberCell = billRow.getCell(6);
                Cell customerNameCell = billRow.getCell(7);
                Cell emailCell = billRow.getCell(8);
                Cell customerIdCell = billRow.getCell(9);
                Cell customerAddressCell = billRow.getCell(10);
                Cell customerTaxNumberCell = billRow.getCell(11);
                Cell creationDateCell = billRow.getCell(12);
                Cell expireDateCell = billRow.getCell(13);
                Cell descriptionCell = billRow.getCell(14);
                Cell previousBalanceCell = billRow.getCell(15);
                Cell campaingCell = billRow.getCell(16);
                Cell isPartialCell = billRow.getCell(17);
                Cell minimumPartialCell = billRow.getCell(18);

                String serviceNameValue = (serviceNameCell == null) ? (null) : (formatter.formatCellValue(serviceNameCell));
                String activityValue = (activityCell == null) ? (null) : (formatter.formatCellValue(activityCell));
                String billNumberValue = (billNumberCell == null) ? (null) : (formatter.formatCellValue(billNumberCell));
                String mobileNumberValue = (mobileNumberCell == null) ? (null) : (formatter.formatCellValue(mobileNumberCell));
                String emailValue = (emailCell == null) ? (null) : (formatter.formatCellValue(emailCell));
                String customerNameValue = (customerNameCell == null) ? (null) : (formatter.formatCellValue(customerNameCell));
                String customerAddress = (customerAddressCell == null) ? (null) : (formatter.formatCellValue(customerAddressCell));
                String customerTaxNumber = (customerTaxNumberCell == null) ? (null) : (formatter.formatCellValue(customerTaxNumberCell));
                String previousBalance = (previousBalanceCell == null) ? "0" : (formatter.formatCellValue(previousBalanceCell));
                String vatValue = (vatCell == null) ? (null) : (formatter.formatCellValue(vatCell));
                String amountValue = (amountCell == null) ? (null) : (formatter.formatCellValue(amountCell));
                String creationDateValue = (creationDateCell == null) ? (null) : (formatter.formatCellValue(creationDateCell));
                String expireDateValue = (expireDateCell == null) ? (null) : (formatter.formatCellValue(expireDateCell));
                String descriptionValue = (descriptionCell == null) ? (null) : (formatter.formatCellValue(descriptionCell));
                String customerId = (customerIdCell == null) ? (null) : (formatter.formatCellValue(customerIdCell));
                String campaignId = (campaingCell == null) ? (null) : (formatter.formatCellValue(campaingCell));
                String discountValue = (discountCell == null) ? (null) : (formatter.formatCellValue(discountCell));
                String isPartialValue = (discountCell == null) ? (null) : (formatter.formatCellValue(isPartialCell));
                String minimumPartialValue = (discountCell == null) ? (null) : (formatter.formatCellValue(minimumPartialCell));

                mobileNumberValue = Utils.formatMobileNumber(mobileNumberValue);

                if (billMap.get(mobileNumberValue) == null) {
                    //Create New Bill Record

                    if (!Utils.isNullOrEmpty(billNumberValue)) {
                        Bill bill = entityBillsMapMap.get(billNumberValue);
                        if (bill != null) {
                            ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("billNumber", "bill number already exist", (rowIndex + 1), 2, locale);
                            validationMessageExcelList.add(validationMessageExcel);
                        }
                    }

                    if (entityUser.getEntity().getIsAutoGenerateBillNumber().equals(BooleanFlag.YES) && Utils.isNullOrEmpty(billNumberValue)) {
                        billNumberSequence += 1;
                        billNumberValue = Utils.generateBillNumber(entityUser.getEntity()) + billNumberSequence;
                    }

                    if (Utils.isNullOrEmpty(serviceNameValue)) {
                        ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("serviceName", "service_name_empty", (rowIndex + 1), 2, locale);
                        validationMessageExcelList.add(validationMessageExcel);
                    }

                    if (Utils.isNullOrEmpty(customerNameValue)) {
                        ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("customerName", "invalid_customer_name", (rowIndex + 1), 3, locale);
                        validationMessageExcelList.add(validationMessageExcel);
                    }

                    if (Utils.isNullOrEmpty(mobileNumberValue)) {
                        ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("mobileNumber", "customer_mobile_number_empty", (rowIndex + 1), 4, locale);
                        validationMessageExcelList.add(validationMessageExcel);
                    }

                    if (!Utils.isNullOrEmpty(mobileNumberValue) && !Utils.isValidMobileNumber(mobileNumberValue)) {
                        ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("mobileNumber", "invalid_mobile_number", (rowIndex + 1), 4, locale);
                        validationMessageExcelList.add(validationMessageExcel);
                    }

                    if ((emailValue != null && !emailValue.equals("")) && !Utils.isValidEmail(emailValue)) {
                        ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("email", "invalid_email", (rowIndex + 1), 5, locale);
                        validationMessageExcelList.add(validationMessageExcel);
                    }

                    if (!Utils.isNullOrEmpty(customerId) && customerId.length() < 10) {
                        ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("customerId", "customer_id_number_not_valid", (rowIndex + 1), 6, locale);
                        validationMessageExcelList.add(validationMessageExcel);
                    }

                    if (Utils.convertToBigDecimal(amountValue) == null || Utils.isNullOrEmpty(amountValue)) {
                        ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("amount", "invalid_amount_value", (rowIndex + 1), 7, locale);
                        validationMessageExcelList.add(validationMessageExcel);
                    }

                    LocalDateTime issueDateTime = Utils.parseDateFromString(creationDateValue, "yyyy-MM-dd");
                    if (issueDateTime == null) {
                        ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("issueDate", "issue_date_invalid", (rowIndex + 1), 11, locale);
                        validationMessageExcelList.add(validationMessageExcel);
                    }

                    if (!Utils.isDateCorrect(creationDateValue, "yyyy-MM-dd")) {
                        ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("issueDate", "date_invalid", (rowIndex + 1), 11, locale);
                        validationMessageExcelList.add(validationMessageExcel);
                    }

                    LocalDateTime expireDateTime = Utils.parseDateFromString(expireDateValue, "yyyy-MM-dd");
                    if (expireDateTime == null) {
                        ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("expireDate", "expire_date_invalid", (rowIndex + 1), 12, locale);
                        validationMessageExcelList.add(validationMessageExcel);
                    }

                    if (!Utils.isDateCorrect(expireDateValue, "yyyy-MM-dd")) {
                        ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("expireDate", "date_invalid", (rowIndex + 1), 12, locale);
                        validationMessageExcelList.add(validationMessageExcel);
                    }

                    if (issueDateTime != null && expireDateTime != null && issueDateTime.isAfter(expireDateTime)) {
                        ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("expireDate", "expire_date_after_issue", (rowIndex + 1), 12, locale);
                        validationMessageExcelList.add(validationMessageExcel);
                    }

                    if (expireDateTime != null && expireDateTime.isBefore(LocalDate.now().atStartOfDay().plusHours(23).plusMinutes(59))) {
                        ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("expireDate", "expire_date_is_passed", (rowIndex + 1), 12, locale);
                        validationMessageExcelList.add(validationMessageExcel);
                    }

                    if (Utils.isNullOrEmpty(activityValue)) {
                        ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("activity", "activity_value_empty", (rowIndex + 1), 14, locale);
                        validationMessageExcelList.add(validationMessageExcel);
                    }

                    if (issueDateTime != null && expireDateTime != null && issueDateTime.equals(expireDateTime)) {
                        ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("expireDate", "issuance_expire_date_same", (rowIndex + 1), 12, locale);
                        validationMessageExcelList.add(validationMessageExcel);
                    }

                    if (Utils.isNullOrEmpty(vatValue)) {
                        ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("vat", "vat_value_empty", (rowIndex + 1), 15, locale);
                        validationMessageExcelList.add(validationMessageExcel);
                        vatValue = "0";
                    }

                    if ((customerTaxNumber != null) && (!customerTaxNumber.isEmpty()) && (customerTaxNumber.length() != 15)) {
                        ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("customerTaxNumber", "VAT number length not correct", (rowIndex + 1), 5, locale);
                        validationMessageExcelList.add(validationMessageExcel);
                    }

                    BooleanFlag vatExempted = BooleanFlag.NO;
                    BooleanFlag vatNa = BooleanFlag.NO;
                    BigDecimal vatAmount = null;
                    switch (vatValue) {
                        case "EXE":
                            vatValue = "0";
                            vatAmount = new BigDecimal("0");
                            vatExempted = (BooleanFlag.YES);
                            vatNa = (BooleanFlag.NO);
                            break;
                        case "NA":
                            vatValue = "0";
                            vatAmount = new BigDecimal("0");
                            vatExempted = (BooleanFlag.NO);
                            vatNa = (BooleanFlag.YES);
                            break;
                        default:
                            vatExempted = (BooleanFlag.NO);
                            vatNa = (BooleanFlag.NO);
                            vatAmount = new BigDecimal(vatValue);
                            LOGGER.info("vatAmount: " + vatAmount);
//                            if (entityVatList == null || entityVatList.isEmpty()) {
//                                ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("vat", "vat_value_not_allowed", (rowIndex + 1), 15, locale);
//                                validationMessageExcelList.add(validationMessageExcel);
//                            }
                    }

                    EntityVat entityVat = entityVatMap.get(vatAmount.toString());
                    if (entityVat == null && vatNa.equals(BooleanFlag.NO) && vatExempted.equals(BooleanFlag.NO)) {
                        ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("vat", "vat value is not part of selected vat list", (rowIndex + 1), 8, locale);
                        validationMessageExcelList.add(validationMessageExcel);
                    }


                    BigDecimal billAmount = Utils.convertToBigDecimal(amountValue);

                    BigDecimal discount = Utils.convertToBigDecimal(discountValue);

                    BigDecimal previousBalanceAmount = Utils.convertToBigDecimal(previousBalance);

                    BigDecimal minimumPartialAmount = Utils.convertToBigDecimal(minimumPartialValue);

                    if (billAmount == null) {
                        billAmount = new BigDecimal("0");
                    }

                    if (discount == null) {
                        discount = new BigDecimal("0");
                    }

                    discount = discount.divide(new BigDecimal("100"));

                    if (previousBalanceAmount == null) {
                        previousBalanceAmount = new BigDecimal("0");
                    }

                    if (minimumPartialAmount == null) {
                        minimumPartialAmount = entityUser.getEntity().getMiniPartialDefaultValue();
                    }

                    String discountType = "PERC";

                    BigDecimal discountAmount = billAmount.multiply(discount);

                    BigDecimal subAmountAfterDiscount = Utils.calculateSubAmountDiscount(billAmount, discount, discountType);

                    subAmountAfterDiscount = subAmountAfterDiscount.subtract(discountAmount);


                    BigDecimal totalAmount = Utils.calculateTotalAmountFromVatAndPreviousBalance(subAmountAfterDiscount, vatAmount.toString(), BigDecimal.ZERO);

                    LOGGER.info("billAmount: " + billAmount);
                    LOGGER.info("discount: " + discount);
                    LOGGER.info("discountAmount: " + discountAmount);
                    LOGGER.info("subAmountAfterDiscount: " + subAmountAfterDiscount);
                    LOGGER.info("vat: " + vatAmount);
                    LOGGER.info("totalAmount: " + totalAmount);

                    sumAmount = sumAmount.add(totalAmount);

                    List<BillCustomField> billCustomFieldList = new ArrayList<>();
                    for (int customFieldIndex = 19, entityFieldIndex = 0; customFieldIndex <= 28; customFieldIndex++, entityFieldIndex++) {

                        Cell cell = billRow.getCell(customFieldIndex);


                        EntityField entityField = null;
                        try {
                            LOGGER.info("entityFieldIndex: " + entityFieldIndex);
                            entityField = entityFieldList.get(entityFieldIndex);
                        } catch (Exception ex) {
                            LOGGER.info("ex: " + ex.getMessage());
                            continue;
                        }

                        LOGGER.info("entityField: " + entityField);

                        if (cell == null) {
//                            if (entityField.getIsRequired().equals(BooleanFlag.YES)) {
//                                ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel(entityField.getFieldNameEn(), entityField.getFieldNameEn() + " empty", (rowIndex + 1), 3, locale);
//                                validationMessageExcelList.add(validationMessageExcel);
//                            }
                            continue;
                        }

                        String value = formatter.formatCellValue(cell);

                        LOGGER.info("value: " + value);
                        LOGGER.info("entityField: " + entityField.getId());

//                        if(!Utils.isValidCustomField(entityField,value)){
//                            ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel(entityField.getFieldNameEn(), entityField.getFieldNameEn() +" empty", (rowIndex + 1), 3, locale);
//                            validationMessageExcelList.add(validationMessageExcel);
//                        }
//
//                        if(!Utils.isValidCustomField(entityField.getValueType(),value)){
//                            ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel(entityField.getFieldNameEn(), entityField.getFieldNameEn() +" not valid", (rowIndex + 1), 3, locale);
//                            validationMessageExcelList.add(validationMessageExcel);
//                        }

                        BillCustomField billCustomField = new BillCustomField();
                        billCustomField.setEntityField(entityField);
                        billCustomField.setValue(value);

                        billCustomFieldList.add(billCustomField);

                    }

                    String activityId = "0";
                    if (activityValue != null && !activityValue.isEmpty()) {
                        activityId = (activityValue.split("-")[0]);
                        if (activityId == null) {
                            ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("activity", "activity_value_empty", (rowIndex + 1), 14, locale);
                            validationMessageExcelList.add(validationMessageExcel);
                        }
                    }

                    EntityActivity entityActivity = entityActivityMap.get(activityId);
                    if (entityActivity != null) {
                        //logic for limitation
                    } else {
                        ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("activity", "activity_value_empty", (rowIndex + 1), 14, locale);
                        validationMessageExcelList.add(validationMessageExcel);
                    }

                    Bill bill = new Bill();
                    bill.setServiceName(serviceNameValue);
                    bill.setEntityActivity(entityActivity);
                    bill.setBillNumber(billNumberValue);
                    bill.setCustomerMobileNumber(mobileNumberValue);
                    bill.setCustomerEmailAddress(emailValue);
                    bill.setCustomerFullName(customerNameValue);
                    bill.setCustomerAddress(customerAddress);
                    bill.setCustomerTaxNumber(customerTaxNumber);
                    bill.setCustomerIdNumber(customerId);
                    bill.setVat(BigDecimal.ZERO);
                    bill.setSubAmount(totalAmount);
                    bill.setEntity(entityUser.getEntity());
                    bill.setEntityUser(entityUser);
                    bill.setDiscount(new BigDecimal(0));
                    bill.setCustomerPreviousBalance(previousBalanceAmount);
                    bill.setTotalAmount(totalAmount);
                    bill.setIssueDate(issueDateTime);
                    bill.setExpireDate(expireDateTime);
                    bill.setBillStatus(BillStatus.MAKED);
                    bill.setBillSource(BillSource.FILE);
                    bill.setBillType(BillType.DETAIL_BILL);
                    bill.setBulkBill(bulkBill);
                    bill.setServiceDescription(descriptionValue);
                    bill.setBillCustomFieldList(billCustomFieldList);
                    bill.setVatNaFlag(BooleanFlag.NO);
                    bill.setVatExemptedFlag(BooleanFlag.NO);
                    bill.setDiscountType(discountType);
                    bill.setDiscount(discountAmount);
                    bill.setIsPartialAllowed(BooleanFlag.getValue(isPartialValue));
                    bill.setMiniPartialAmount(minimumPartialAmount);
                    bill.setLogo(entityUser.getEntity().getLogo());
                    bill.setStamp(entityUser.getEntity().getStamp());

                    if (bill.getIsPartialAllowed() != null && bill.getIsPartialAllowed().equals(BooleanFlag.YES)) {
                        bill.setBillCategory(BillCategory.ONE_OFF_PARTIAL);
                    } else {
                        bill.setBillCategory(BillCategory.INSTANTLY);
                        bill.setIsPartialAllowed(BooleanFlag.NO);
                    }


                    if (!Utils.isNullOrEmpty(campaignId)) {

                        if (campaignId.split("-").length == 1) {
                            ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("campaignName", "not_valid_campaign_name", (rowIndex + 1), 16, locale);
                            validationMessageExcelList.add(validationMessageExcel);
                        } else {
                            bill.setCampaignId(Integer.parseInt(campaignId.split("-")[0]));
                        }
                    }

                    if (entityUser.getEntity().getIsPartialAllowed().equals(BooleanFlag.NO)) {
                        bill.setIsPartialAllowed(BooleanFlag.NO);
                        bill.setBillCategory(BillCategory.INSTANTLY);
                    }

                    List<BillItem> billItemList = new ArrayList<>();

                    BillItem billItem = new BillItem();

                    billItem.setBill(bill);
                    billItem.setNumber(1);
                    billItem.setQuantity(new BigDecimal("1"));
                    billItem.setName(serviceNameValue);
                    billItem.setDiscountType(discountType);
                    billItem.setDiscount(Utils.convertToBigDecimal(discountValue) != null ? Utils.convertToBigDecimal(discountValue) : BigDecimal.ZERO);
                    billItem.setUnitPrice(billAmount);
                    billItem.setTotalPrice(billAmount);
                    billItem.setVat(vatAmount);
                    billItem.setVatExemptedFlag(vatExempted);
                    billItem.setVatNaFlag(vatNa);

                    billItemList.add(billItem);

                    bill.setBillItemList(billItemList);

                    billMap.put(mobileNumberValue, bill);

                    if (!Utils.isNullOrEmpty(bill.getBillNumber())) {
                        entityBillsMapMap.put(bill.getBillNumber(), bill);
                    }

                    if (!Utils.isMinimumPartialAmountAllowed(bill)) {
                        ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("minimumPartialAmount", "minimum partial amount should not less than activity minimum amount", (rowIndex + 1), 12, locale);
                        validationMessageExcelList.add(validationMessageExcel);
                    }

                } else {
                    //Current Item Exist
                    Bill bill = billMap.get(mobileNumberValue);

                    BooleanFlag vatExempted = BooleanFlag.NO;
                    BooleanFlag vatNa = BooleanFlag.NO;
                    BigDecimal vatAmount = null;
                    switch (vatValue) {
                        case "EXE":
                            vatValue = "0";
                            vatAmount = new BigDecimal("0");
                            vatExempted = (BooleanFlag.YES);
                            vatNa = (BooleanFlag.NO);
                            break;
                        case "NA":
                            vatValue = "0";
                            vatAmount = new BigDecimal("0");
                            vatExempted = (BooleanFlag.NO);
                            vatNa = (BooleanFlag.YES);
                            break;
                        default:
                            vatExempted = (BooleanFlag.NO);
                            vatNa = (BooleanFlag.NO);
                            vatAmount = new BigDecimal(vatValue);
                            LOGGER.info("vatAmount: " + vatAmount);
//                            if (entityVatList == null || entityVatList.isEmpty()) {
//                                ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("vat", "vat_value_not_allowed", (rowIndex + 1), 15, locale);
//                                validationMessageExcelList.add(validationMessageExcel);
//                            }
                    }

                    EntityVat entityVat = entityVatMap.get(vatAmount.toString());
                    if (entityVat == null && vatNa.equals(BooleanFlag.NO) && vatExempted.equals(BooleanFlag.NO)) {
                        ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("vat", "vat value is not part of selected vat list", (rowIndex + 1), 8, locale);
                        validationMessageExcelList.add(validationMessageExcel);
                    }


                    BigDecimal billAmount = Utils.convertToBigDecimal(amountValue);

                    BigDecimal discount = Utils.convertToBigDecimal(discountValue);

                    BigDecimal previousBalanceAmount = Utils.convertToBigDecimal(previousBalance);

                    BigDecimal minimumPartialAmount = Utils.convertToBigDecimal(minimumPartialValue);

                    if (billAmount == null) {
                        billAmount = new BigDecimal("0");
                    }

                    if (discount == null) {
                        discount = new BigDecimal("0");
                    }

                    discount = discount.divide(new BigDecimal("100"));

                    if (previousBalanceAmount == null) {
                        previousBalanceAmount = new BigDecimal("0");
                    }

                    if (minimumPartialAmount == null) {
                        minimumPartialAmount = new BigDecimal("0");
                    }

                    String discountType = "PERC";

                    BigDecimal discountAmount = billAmount.multiply(discount);

                    BigDecimal subAmountAfterDiscount = Utils.calculateSubAmountDiscount(billAmount, discount, discountType);

                    subAmountAfterDiscount = subAmountAfterDiscount.subtract(discountAmount);


                    BigDecimal totalAmount = Utils.calculateTotalAmountFromVatAndPreviousBalance(subAmountAfterDiscount, vatAmount.toString(), BigDecimal.ZERO);

                    LOGGER.info("billAmount: " + billAmount);
                    LOGGER.info("discount: " + discount);
                    LOGGER.info("discountAmount: " + discountAmount);
                    LOGGER.info("subAmountAfterDiscount: " + subAmountAfterDiscount);
                    LOGGER.info("vat: " + vatAmount);
                    LOGGER.info("totalAmount: " + totalAmount);

                    sumAmount = sumAmount.add(totalAmount);

                    bill.setSubAmount(bill.getTotalAmount().add(totalAmount));
                    bill.setTotalAmount(bill.getTotalAmount().add(totalAmount));
                    bill.setDiscount(bill.getDiscount().add(discountAmount));

                    LOGGER.info("Current subAmount: " + bill.getSubAmount());
                    LOGGER.info("Current Total amount: " + bill.getSubAmount());

                    BillItem billItem = new BillItem();

                    billItem.setBill(bill);
                    billItem.setNumber(bill.getBillItemList().size() + 1);
                    billItem.setQuantity(new BigDecimal("1"));
                    billItem.setName(serviceNameValue);
                    billItem.setDiscountType(discountType);
                    billItem.setDiscount(Utils.convertToBigDecimal(discountValue) != null ? Utils.convertToBigDecimal(discountValue) : BigDecimal.ZERO);
                    billItem.setUnitPrice(billAmount);
                    billItem.setTotalPrice(billAmount);
                    billItem.setVat(vatAmount);
                    billItem.setVatExemptedFlag(vatExempted);
                    billItem.setVatNaFlag(vatNa);

                    bill.getBillItemList().add(billItem);

                    if (bill.getBillItemList().size() > 10) {
                        ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("itemSise", "item should not be more than 10", (rowIndex + 1), 1, locale);
                        validationMessageExcelList.add(validationMessageExcel);
                    }
                }
            }

            entityRepo.increaseBillNumberCounter(billNumberSequence, entityUser.getEntity().getId());

            for (Map.Entry billRecord : billMap.entrySet()) {
                LOGGER.info("Key: " + billRecord.getKey() + " & Value: " + billRecord.getValue());
            }

            billMap.forEach((key, value) -> {
                LOGGER.info("Key: " + key + " & Value: " + value);
                billList.add(value);
            });


            billList.forEach(bill -> {
                bill.setTotalAmount(bill.getTotalAmount().add(bill.getCustomerPreviousBalance()));
            });
        }

        billList.forEach(bill -> {
            BigDecimal totalAmount = bill.getTotalAmount();
            BigDecimal minimumPartialAMount = bill.getMiniPartialAmount();
            Integer compareResult = totalAmount.compareTo(minimumPartialAMount);

            if (compareResult == -1) {
                ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("minimumPartialAmount", "total amount should not less than minimum partial amount", (1), 1, locale);
                validationMessageExcelList.add(validationMessageExcel);
            }
        });

        bulkBillRqDto.setBillList(billList);

        if (workbook != null) {
            try {
                workbook.close();
            } catch (IOException e) {
                LOGGER.error(e);
            }
        }

        if (!validationMessageExcelList.isEmpty()) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "invalid_file_data", validationMessageExcelList, locale);
        }

        return bulkBillRqDto;
    }

    public void uploadMasterBulkBillRefactored(BulkBillRqDto bulkBillRqDto, EntityUser entityUser, Locale locale) {

        if (!entityUser.getEntityUserType().equals(EntityUserType.OFFICER) && entityUser.getEntityUserType().equals(EntityUserType.SUPERVISOR)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "access_maker_user", locale);
        }

        BulkBill bulkBill = bulkBillRepo.findBulkBillById(bulkBillRqDto.getBulkBillId());

        bulkBill.setBulkBillStatus(BulkBillStatus.BULK_MAKED);

        BulkBillTimeline bulkBillTimeline = new BulkBillTimeline();
        bulkBillTimeline.setUserReference(entityUser.getUsername());
        bulkBillTimeline.setActionDate(LocalDateTime.now());
        bulkBillTimeline.setBulkBillStatus(BulkBillStatus.BULK_MAKED);
        bulkBillTimeline.setBulkBill(bulkBill);
        bulkBillTimeline.setEntityUser(entityUser);

        bulkBillTimeline = bulkBillTimelineRepo.save(bulkBillTimeline);

        Map<String, List<BillCustomField>> customFieldListMap = new HashMap<String, List<BillCustomField>>();
        Map<String, BillTimeline> billTimeLineMap = new HashMap<String, BillTimeline>();

        Map<String, List<BillItem>> billItemMap = new HashMap<String, List<BillItem>>();

        List<Bill> billList = bulkBillRqDto.getBillList();

        int x = 1;

        for (Bill bill : billList) {

            if (billTimeLineMap.get(bill.getBillNumber()) != null) {
//                List<ValidationMessageExcel> validationMessageExcelList = new ArrayList<>();
//
//                ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("billNumber", "bill_already_exist", (x + 4), 1, locale);
//                validationMessageExcelList.add(validationMessageExcel);
//
//                throw new HttpServiceException(HttpStatus.BAD_REQUEST, "invalid_file_data", validationMessageExcelList, locale);
            }


            LOGGER.info("Excel Row ----------> " + x);

            bill.setBulkBill(bulkBill);
            bill.setCreateDate(LocalDateTime.now());

            List<BillCustomField> billCustomFieldList = bill.getBillCustomFieldList();
            if (billCustomFieldList != null && billCustomFieldList.size() > 0) {
                customFieldListMap.put(bill.getBillNumber(), billCustomFieldList);
            }

            BillTimeline billTimeline = new BillTimeline();
            billTimeline.setUserReference(entityUser.getUsername());
            billTimeline.setActionDate(LocalDateTime.now());
            billTimeline.setEntityUser(entityUser);
            billTimeline.setBillStatus(BillStatus.MAKED);
            billTimeLineMap.put(bill.getBillNumber(), billTimeline);

            if (bill.getBillItemList() != null && !bill.getBillItemList().isEmpty()) {
                LOGGER.info("bill item: " + bill.getBillItemList().size());
                billItemMap.put(bill.getBillNumber(), bill.getBillItemList());
            } else {
                billItemMap.put(bill.getBillNumber(), new ArrayList<>());
            }


            x++;
        }

        LOGGER.info("Before Start the  Bill Saving ===============================================================>");
        billList = (List<Bill>) billRepo.saveAll(billList);
        LOGGER.info("After the  Bill Saving ===============================================================> ");

        List<BillCustomField> completeCustomeFieldList = new ArrayList<BillCustomField>();
        List<BillTimeline> completeBillTimeLineList = new ArrayList<BillTimeline>();
        List<BillItem> completeBillItemList = new ArrayList<BillItem>();

        for (Bill bill : billList) {

            if (customFieldListMap.containsKey(bill.getBillNumber())) {
                for (BillCustomField billCustomField : customFieldListMap.get(bill.getBillNumber())) {
                    billCustomField.setBill(bill);
                    completeCustomeFieldList.add(billCustomField);
                }
            }
            if (billTimeLineMap.containsKey(bill.getBillNumber())) {

                BillTimeline billTimeline = billTimeLineMap.get(bill.getBillNumber());
                billTimeline.setBill(bill);
                completeBillTimeLineList.add(billTimeline);
            }

            if (billItemMap.containsKey(bill.getBillNumber())) {

                List<BillItem> billItemList = billItemMap.get(bill.getBillNumber());
                for (BillItem billItem : billItemList) {
                    billItem.setBill(bill);
                    completeBillItemList.add(billItem);
                }
            }
        }

        LOGGER.info("Before Start the  BillCutom Field Saving  ===============================================================>");

        if (completeCustomeFieldList.size() > 0) {
            billCustomFieldRepo.saveAll(completeCustomeFieldList);
        }

        LOGGER.info("End the  BillCutom Field Saving  ===============================================================>");

        LOGGER.info("Before Start the  BillTimeLine Saving  ===============================================================>");

        if (completeBillTimeLineList.size() > 0) {
            billTimelineRepo.saveAll(completeBillTimeLineList);
        }

        LOGGER.info("End Start the  BillTimeLine Saving  ===============================================================>");

        if (completeBillItemList.size() > 0) {
            billItemRepo.saveAll(completeBillItemList);
        }

        bulkBill.setNumberOfBills(billList.size() + 0L);

        if (entityUser.getEntityUserType().equals(EntityUserType.SUPER_ADMIN_PLUS)
                || entityUser.getEntityUserType().equals(EntityUserType.SUPER_ADMIN)
                || entityUser.getEntityUserType().equals(EntityUserType.SUPERVISOR_PLUS)) {
            this.checkBulkBillRefactored(bulkBill.getId(), entityUser, locale, billList);
        }
    }

    public void checkBulkBillRefactored(Long bulkBillId, EntityUser entityUser, Locale locale, List<Bill> billList) {

        if (!entityUser.getEntityUserType().equals(EntityUserType.SUPERVISOR) && entityUser.getEntityUserType().equals(EntityUserType.OFFICER)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "cannot_access_by_officer_user", locale);
        }

        BulkBill bulkBill = bulkBillRepo.findBulkBillById(bulkBillId);

        if (bulkBill == null) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bulk_not_found", locale);
        }

        if (bulkBill.getBulkBillStatus().equals(BulkBillStatus.BULK_DELETED)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bulk_not_found", locale);
        }

        if (!bulkBill.getBulkBillStatus().equals(BulkBillStatus.BULK_MAKED)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bulk_not_maked", locale);
        }

        if (!bulkBill.getEntity().getId().equals(entityUser.getEntity().getId())) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bulk_not_for_company", locale);
        }

        if (isBulkBillStatusExist(bulkBill, BulkBillStatus.BULK_CHECKED)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bulk_already_checked", locale);
        }

        bulkBill.setBulkBillStatus(BulkBillStatus.BULK_CHECKED);

        BulkBillTimeline checkedBulkBillTimeline = new BulkBillTimeline();
        checkedBulkBillTimeline.setBulkBillStatus(BulkBillStatus.BULK_CHECKED);
        checkedBulkBillTimeline.setBulkBill(bulkBill);
        checkedBulkBillTimeline.setActionDate(LocalDateTime.now());
        checkedBulkBillTimeline.setEntityUser(entityUser);
        checkedBulkBillTimeline.setUserReference(entityUser.getUsername());

        bulkBillTimelineRepo.save(checkedBulkBillTimeline);

        List<BillTimeline> fullBillTimeLineList = new ArrayList<BillTimeline>();

        Long sadadNumberSeq = entityUser.getEntity().getBillSequence();

        for (Bill bill : billList) {

            LOGGER.info("Extract the BillTime Line info  ===============================================================>");

//            if (billTimelineRepo.findFirstBillTimelineByBillAndBillStatus(bill, BillStatus.CHECKED) != null) {
//                continue;
//            }

            LOGGER.info("Extract the BillTime Line info End  ===============================================================>");

            bill.setBillStatus(BillStatus.CHECKED);

            BillTimeline checkedBillTimeline = new BillTimeline();
            checkedBillTimeline.setBillStatus(BillStatus.CHECKED);
            checkedBillTimeline.setBill(bill);
            checkedBillTimeline.setActionDate(LocalDateTime.now());
            checkedBillTimeline.setEntityUser(entityUser);
            checkedBillTimeline.setUserReference(entityUser.getUsername());

            // billTimelineRepo.save(checkedBillTimeline);
            fullBillTimeLineList.add(checkedBillTimeline);

            sadadNumberSeq += 1;

            String sadadNumber = (bill.getEntity().getCode()) + (bill.getEntityActivity().getCode()) + (sadadNumberSeq);

            bill.setSadadNumber(sadadNumber);

            bill.setBillStatus(BillStatus.UPLOADED_TO_SADAD);

            BillTimeline uploadedToSadadBillTimeline = new BillTimeline();
            uploadedToSadadBillTimeline.setBillStatus(BillStatus.UPLOADED_TO_SADAD);
            uploadedToSadadBillTimeline.setBill(bill);
            uploadedToSadadBillTimeline.setActionDate(LocalDateTime.now());
            uploadedToSadadBillTimeline.setEntityUser(entityUser);
            uploadedToSadadBillTimeline.setUserReference(entityUser.getUsername());

            //billTimelineRepo.save(uploadedToSadadBillTimeline);
            fullBillTimeLineList.add(uploadedToSadadBillTimeline);

        }

        entityRepo.increaseSadadNumberCounter(sadadNumberSeq, entityUser.getEntity().getId());

        LOGGER.info("Saving BillTimeLine Saving Start 1  ===============================================================>");

        billTimelineRepo.saveAll(fullBillTimeLineList);

        LOGGER.info("Saving BillTimeLine Saving End 1  ===============================================================>");

        bulkBill.setBulkBillStatus(BulkBillStatus.BULK_UPLOADED_TO_SADAD);

        String asyncRqUid = UUID.randomUUID().toString();
        bulkBill.setAsyncRqUID(asyncRqUid);

        BulkBillTimeline uploadedToSadadBulkBillTimeline = new BulkBillTimeline();
        uploadedToSadadBulkBillTimeline.setBulkBillStatus(BulkBillStatus.BULK_UPLOADED_TO_SADAD);
        uploadedToSadadBulkBillTimeline.setBulkBill(bulkBill);
        uploadedToSadadBulkBillTimeline.setActionDate(LocalDateTime.now());
        uploadedToSadadBulkBillTimeline.setEntityUser(entityUser);
        uploadedToSadadBulkBillTimeline.setUserReference(entityUser.getUsername());
        bulkBillTimelineRepo.save(uploadedToSadadBulkBillTimeline);

        Config config = configRepo.findConfigByKey("INTG_FLAG");


        boolean isIntegrationAllowed = false;

        if (config != null && config.getValue().equalsIgnoreCase("true")) {

            isIntegrationAllowed = true;

            WebServiceResponse webServiceResponse = webServiceHelper.uploadBulk(bulkBill, billList);

            if (!webServiceResponse.getStatus().equals(200)) {
                Document documentXmlResponse = Utils.convertStringToXmlDocument(new JSONObject(webServiceResponse.getBody()).getString("data"));
                String message = Utils.getElementValueByTagName(documentXmlResponse, "faultstring");
                throw new HttpServiceException(HttpStatus.BAD_REQUEST, message, webServiceResponse.getBody(), locale);
            }
//
//            // This part is for mocking service .......................
//            bulkBill.setBulkBillStatus(BulkBillStatus.BULK_APPROVED_BY_SADAD);
//
//            BulkBillTimeline approvedBulkBillTimeline = new BulkBillTimeline();
//            approvedBulkBillTimeline.setBulkBillStatus(BulkBillStatus.BULK_APPROVED_BY_SADAD);
//            approvedBulkBillTimeline.setBulkBill(bulkBill);
//            approvedBulkBillTimeline.setActionDate(LocalDateTime.now());
//            approvedBulkBillTimeline.setEntityUser(entityUser);
//            approvedBulkBillTimeline.setUserReference(entityUser.getUsername());
//
//            bulkBillTimelineRepo.save(approvedBulkBillTimeline);
//
//            fullBillTimeLineList = new ArrayList<BillTimeline>();
//
//            for (Bill bill : billList) {
//
//                bill.setBillStatus(BillStatus.APPROVED_BY_SADAD);
//
//                BillTimeline approvedBillTimeline = new BillTimeline();
//                approvedBillTimeline.setBillStatus(BillStatus.APPROVED_BY_SADAD);
//                approvedBillTimeline.setBill(bill);
//                approvedBillTimeline.setActionDate(LocalDateTime.now());
//                approvedBillTimeline.setEntityUser(entityUser);
//                approvedBillTimeline.setUserReference(entityUser.getUsername());
//
//                fullBillTimeLineList.add(approvedBillTimeline);
//
//                bill.setApproveDateBySadad(approvedBillTimeline.getActionDate());
//            }
//
//            bulkBill.setSuccessCount(billList.size());
//            bulkBill.setRejectCount(0);
//            bulkBill.setPendingCount(0);
//
//            billRepo.saveAll(billList);
//
//            LOGGER.info("Saving BillTimeLine Saving Start 2  ===============================================================>");
//            billTimelineRepo.saveAll(fullBillTimeLineList);
//            LOGGER.info("Saving BillTimeLine Saving End 2  ===============================================================>");
//
//
//            new Thread() {
//                public void run() {
//                    billNotificationService.sendFcmNotificationBulkIssue(bulkBill.getId(), entityUser, locale);
//
//                    billNotificationService.sendBillNotificationIssueBulk(bulkBill.getId(), entityUser, locale);
//                }
//            }.start();
        } else {
            // This part is for mocking service .......................
            bulkBill.setBulkBillStatus(BulkBillStatus.BULK_APPROVED_BY_SADAD);

            BulkBillTimeline approvedBulkBillTimeline = new BulkBillTimeline();
            approvedBulkBillTimeline.setBulkBillStatus(BulkBillStatus.BULK_APPROVED_BY_SADAD);
            approvedBulkBillTimeline.setBulkBill(bulkBill);
            approvedBulkBillTimeline.setActionDate(LocalDateTime.now());
            approvedBulkBillTimeline.setEntityUser(entityUser);
            approvedBulkBillTimeline.setUserReference(entityUser.getUsername());

            bulkBillTimelineRepo.save(approvedBulkBillTimeline);

            fullBillTimeLineList = new ArrayList<BillTimeline>();

            for (Bill bill : billList) {

                bill.setBillStatus(BillStatus.APPROVED_BY_SADAD);

                BillTimeline approvedBillTimeline = new BillTimeline();
                approvedBillTimeline.setBillStatus(BillStatus.APPROVED_BY_SADAD);
                approvedBillTimeline.setBill(bill);
                approvedBillTimeline.setActionDate(LocalDateTime.now());
                approvedBillTimeline.setEntityUser(entityUser);
                approvedBillTimeline.setUserReference(entityUser.getUsername());

                fullBillTimeLineList.add(approvedBillTimeline);

                bill.setApproveDateBySadad(approvedBillTimeline.getActionDate());
            }

            bulkBill.setSuccessCount(billList.size());
            bulkBill.setRejectCount(0);
            bulkBill.setPendingCount(0);

            billRepo.saveAll(billList);

            LOGGER.info("Saving BillTimeLine Saving Start 2  ===============================================================>");
            billTimelineRepo.saveAll(fullBillTimeLineList);
            LOGGER.info("Saving BillTimeLine Saving End 2  ===============================================================>");


            new Thread() {
                public void run() {
                    billNotificationService.sendFcmNotificationBulkIssue(bulkBill.getId(), entityUser, locale);

                    billNotificationService.sendBillNotificationIssueBulk(bulkBill.getId(), entityUser, locale);
                }
            }.start();
        }
    }

    public void checkBulkBill(Long bulkBillId, EntityUser entityUser, Locale locale) {

        if (!entityUser.getEntityUserType().equals(EntityUserType.SUPERVISOR) && entityUser.getEntityUserType().equals(EntityUserType.OFFICER)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "cannot_access_by_officer_user", locale);
        }

        BulkBill bulkBill = bulkBillRepo.findBulkBillById(bulkBillId);

        if (bulkBill == null) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bulk_not_found", locale);
        }

        if (bulkBill.getBulkBillStatus().equals(BulkBillStatus.BULK_DELETED)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bulk_not_found", locale);
        }

        if (!bulkBill.getBulkBillStatus().equals(BulkBillStatus.BULK_MAKED)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bulk_not_maked", locale);
        }

        if (!bulkBill.getEntity().getId().equals(entityUser.getEntity().getId())) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bulk_not_for_company", locale);
        }

        if (isBulkBillStatusExist(bulkBill, BulkBillStatus.BULK_CHECKED)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bulk_already_checked", locale);
        }

        bulkBill.setBulkBillStatus(BulkBillStatus.BULK_CHECKED);

        BulkBillTimeline checkedBulkBillTimeline = new BulkBillTimeline();
        checkedBulkBillTimeline.setBulkBillStatus(BulkBillStatus.BULK_CHECKED);
        checkedBulkBillTimeline.setBulkBill(bulkBill);
        checkedBulkBillTimeline.setActionDate(LocalDateTime.now());
        checkedBulkBillTimeline.setEntityUser(entityUser);
        checkedBulkBillTimeline.setUserReference(entityUser.getUsername());

        bulkBillTimelineRepo.save(checkedBulkBillTimeline);

        List<Bill> billList = billRepo.findAllByBulkBill(bulkBill);

        Long sadadNumberSeq = entityUser.getEntity().getBillSequence();

        for (Bill bill : billList) {

            if (billTimelineRepo.findFirstBillTimelineByBillAndBillStatus(bill, BillStatus.CHECKED) != null) {
                continue;
            }

            bill.setBillStatus(BillStatus.CHECKED);

            BillTimeline checkedBillTimeline = new BillTimeline();
            checkedBillTimeline.setBillStatus(BillStatus.CHECKED);
            checkedBillTimeline.setBill(bill);
            checkedBillTimeline.setActionDate(LocalDateTime.now());
            checkedBillTimeline.setEntityUser(entityUser);
            checkedBillTimeline.setUserReference(entityUser.getUsername());

            billTimelineRepo.save(checkedBillTimeline);

            sadadNumberSeq += 1;

            String sadadNumber = (bill.getEntity().getCode()) + (bill.getEntityActivity().getCode()) + (sadadNumberSeq);

            bill.setSadadNumber(sadadNumber);

            bill.setBillStatus(BillStatus.UPLOADED_TO_SADAD);

            BillTimeline uploadedToSadadBillTimeline = new BillTimeline();
            uploadedToSadadBillTimeline.setBillStatus(BillStatus.UPLOADED_TO_SADAD);
            uploadedToSadadBillTimeline.setBill(bill);
            uploadedToSadadBillTimeline.setActionDate(LocalDateTime.now());
            uploadedToSadadBillTimeline.setEntityUser(entityUser);
            uploadedToSadadBillTimeline.setUserReference(entityUser.getUsername());
            billTimelineRepo.save(uploadedToSadadBillTimeline);
        }

        entityRepo.increaseSadadNumberCounter(sadadNumberSeq, entityUser.getEntity().getId());

        bulkBill.setBulkBillStatus(BulkBillStatus.BULK_UPLOADED_TO_SADAD);

        String asyncRqUid = UUID.randomUUID().toString();
        bulkBill.setAsyncRqUID(asyncRqUid);

        BulkBillTimeline uploadedToSadadBulkBillTimeline = new BulkBillTimeline();
        uploadedToSadadBulkBillTimeline.setBulkBillStatus(BulkBillStatus.BULK_UPLOADED_TO_SADAD);
        uploadedToSadadBulkBillTimeline.setBulkBill(bulkBill);
        uploadedToSadadBulkBillTimeline.setActionDate(LocalDateTime.now());
        uploadedToSadadBulkBillTimeline.setEntityUser(entityUser);
        uploadedToSadadBulkBillTimeline.setUserReference(entityUser.getUsername());
        bulkBillTimelineRepo.save(uploadedToSadadBulkBillTimeline);

        Config config = configRepo.findConfigByKey("INTG_FLAG");

        if (config != null && config.getValue().equalsIgnoreCase("true")) {
            WebServiceResponse webServiceResponse = webServiceHelper.uploadBulk(bulkBill, billList);

            if (!webServiceResponse.getStatus().equals(200)) {
                Document documentXmlResponse = Utils.convertStringToXmlDocument(new JSONObject(webServiceResponse.getBody()).getString("data"));
                String message = Utils.getElementValueByTagName(documentXmlResponse, "faultstring");
                throw new HttpServiceException(HttpStatus.BAD_REQUEST, message, webServiceResponse.getBody(), locale);
            }
        } else {
//            new Thread() {
//                public void run() {
            bulkBill.setBulkBillStatus(BulkBillStatus.BULK_APPROVED_BY_SADAD);

            BulkBillTimeline approvedBulkBillTimeline = new BulkBillTimeline();
            approvedBulkBillTimeline.setBulkBillStatus(BulkBillStatus.BULK_APPROVED_BY_SADAD);
            approvedBulkBillTimeline.setBulkBill(bulkBill);
            approvedBulkBillTimeline.setActionDate(LocalDateTime.now());
            approvedBulkBillTimeline.setEntityUser(entityUser);
            approvedBulkBillTimeline.setUserReference(entityUser.getUsername());

            bulkBillTimelineRepo.save(approvedBulkBillTimeline);

            billList = billRepo.findAllByBulkBill(bulkBill);

            for (Bill bill : billList) {

                bill.setBillStatus(BillStatus.APPROVED_BY_SADAD);

                BillTimeline approvedBillTimeline = new BillTimeline();
                approvedBillTimeline.setBillStatus(BillStatus.APPROVED_BY_SADAD);
                approvedBillTimeline.setBill(bill);
                approvedBillTimeline.setActionDate(LocalDateTime.now());
                approvedBillTimeline.setEntityUser(entityUser);
                approvedBillTimeline.setUserReference(entityUser.getUsername());

                billTimelineRepo.save(approvedBillTimeline);

                bill.setApproveDateBySadad(approvedBillTimeline.getActionDate());

                new Thread() {
                    public void run() {
                        billNotificationService.sendBillNotificationIssue(bill, entityUser, locale);
                    }
                }.start();
            }

            bulkBill.setSuccessCount(billList.size());
            bulkBill.setRejectCount(0);
            bulkBill.setPendingCount(0);

//                }
//            }.start();
        }
    }

    public boolean isBulkBillStatusExist(BulkBill bulkBill, BulkBillStatus bulkBillStatus) {

        BulkBillTimeline checkTimeline = bulkBillTimelineRepo.findBulkBillTimelineByBulkBillAndBulkBillStatus(bulkBill, bulkBillStatus);

        if (checkTimeline == null) {
            return false;
        } else
            return true;
    }

    public void deleteBill(Long fileId, EntityUser entityUser, Locale locale) {

        BulkBill bulkBill = bulkBillRepo.findBulkBillById(fileId);

        if (bulkBill == null) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bulk_not_found", locale);
        }

        if (bulkBill.getBulkBillStatus().equals(BulkBillStatus.BULK_DELETED)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bulk_not_found", locale);
        }

        if (!bulkBill.getEntity().getId().equals(entityUser.getEntity().getId())) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bulk_not_for_company", locale);
        }

        if (!bulkBill.getBulkBillStatus().equals(BulkBillStatus.BULK_MAKED) && !bulkBill.getBulkBillStatus().equals(BulkBillStatus.BULK_GENERATED)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "can_delete_maked_bulk_only", locale);
        }

        bulkBill.setBulkBillStatus(BulkBillStatus.BULK_DELETED);

        BulkBillTimeline deletedBulkBillTimeline = new BulkBillTimeline();
        deletedBulkBillTimeline.setBulkBillStatus(BulkBillStatus.BULK_DELETED);
        deletedBulkBillTimeline.setBulkBill(bulkBill);
        deletedBulkBillTimeline.setActionDate(LocalDateTime.now());
        deletedBulkBillTimeline.setEntityUser(entityUser);
        deletedBulkBillTimeline.setUserReference(entityUser.getUsername());

        bulkBillTimelineRepo.save(deletedBulkBillTimeline);

        List<Bill> billList = billRepo.findAllByBulkBill(bulkBill);

        for (Bill bill : billList) {

            if (billTimelineRepo.findFirstBillTimelineByBillAndBillStatus(bill, BillStatus.DELETED) != null) {
                continue;
            }

            bill.setBillStatus(BillStatus.DELETED);

            BillTimeline deletedBillTimeline = new BillTimeline();
            deletedBillTimeline.setBillStatus(BillStatus.DELETED);
            deletedBillTimeline.setBill(bill);
            deletedBillTimeline.setActionDate(LocalDateTime.now());
            deletedBillTimeline.setEntityUser(entityUser);
            deletedBillTimeline.setUserReference(entityUser.getUsername());

            billTimelineRepo.save(deletedBillTimeline);
        }

    }

    public BulkBillResultTimelineDtoRs findBulkBillTimeline(Long fileId, Locale locale) {
        BulkBill bulkBill = bulkBillRepo.findBulkBillById(fileId);

        if (bulkBill == null) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bulk_not_found", locale);
        }

        if (bulkBill.getBulkBillStatus().equals(BulkBillStatus.BULK_DELETED)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bulk_not_found", locale);
        }

        List<BulkBillTimeline> bulkBillTimelineList = bulkBillTimelineRepo.findBulkBillTimelineByBulkBill(bulkBill);

        List<BulkBillTimelineDtoRs> bulkBillTimelineDtoRsList = new ArrayList<>();
        bulkBillTimelineList.forEach(bulkBillTimeline -> {
            bulkBillTimelineDtoRsList.add((BulkBillTimelineDtoRs) bulkBillTimeline.toDto(locale));
        });

        BulkBillResultTimelineDtoRs bulkBillResultTimelineDtoRs = new BulkBillResultTimelineDtoRs();

        bulkBillResultTimelineDtoRs.setBulkBillTimeline(bulkBillTimelineDtoRsList);
        bulkBillResultTimelineDtoRs.setInnerName(bulkBill.getInnerName());
        bulkBillResultTimelineDtoRs.setOuterName(bulkBill.getOuterName());
        bulkBillResultTimelineDtoRs.setReference(bulkBill.getReference());
        bulkBillResultTimelineDtoRs.setAsyncRqUID(bulkBill.getAsyncRqUID());
        bulkBillResultTimelineDtoRs.setNumberOfBills(bulkBill.getNumberOfBills());
        bulkBillResultTimelineDtoRs.setSuccessCount(bulkBill.getSuccessCount());
        bulkBillResultTimelineDtoRs.setRejectCount(bulkBill.getRejectCount());
        bulkBillResultTimelineDtoRs.setPendingCount(bulkBill.getPendingCount());

        Long numberOfPaidBills = billRepo.countAllPaidBillByBulk(bulkBill);

        if (numberOfPaidBills == null) {
            numberOfPaidBills = 0L;
        }

        Long numberOfBills = bulkBill.getNumberOfBills();
        if (numberOfBills == null) {
            numberOfBills = 0L;
        }

        bulkBillResultTimelineDtoRs.setNumberOfPaid(numberOfPaidBills);
        bulkBillResultTimelineDtoRs.setNumberOfUnPaid(Math.abs(numberOfBills - numberOfPaidBills));

        return bulkBillResultTimelineDtoRs;
    }

    public PageDtoRs searchBulkBill(SearchBulkBillRqDto searchBulkBillRqDto, EntityUser entityUser, Locale locale) {

        if (searchBulkBillRqDto.getPage() == null)
            searchBulkBillRqDto.setPage(0);
        if (searchBulkBillRqDto.getSize() == null)
            searchBulkBillRqDto.setSize(10);

        Pageable pageable = PageRequest.of(searchBulkBillRqDto.getPage(), searchBulkBillRqDto.getSize(), Sort.Direction.DESC, "id");

        Page<BulkBill> bulkBillPage = bulkBillRepo.findAll(new Specification<BulkBill>() {


            public Predicate toPredicate(Root<BulkBill> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

                List<Predicate> predicates = new ArrayList<>();

                if (!Utils.isNullOrEmpty(searchBulkBillRqDto.getReference())) {
                    predicates.add(cb.like(cb.lower(root.get("reference")), "%" + searchBulkBillRqDto.getReference().toLowerCase() + "%"));
                }

                if (!Utils.isNullOrEmpty(searchBulkBillRqDto.getInnerName())) {
                    predicates.add(cb.like(cb.lower(root.get("innerName")), "%" + searchBulkBillRqDto.getInnerName().toLowerCase() + "%"));
                }

                if (!Utils.isNullOrEmpty(searchBulkBillRqDto.getOuterName())) {
                    predicates.add(cb.like(cb.lower(root.get("outerName")), "%" + searchBulkBillRqDto.getOuterName().toLowerCase() + "%"));
                }

                if (entityUser != null && entityUser.getEntity() != null) {
                    predicates.add(cb.equal(root.get("entity"), entityUser.getEntity()));
                }

                Entity entity = null;
                if (searchBulkBillRqDto.getEntityCode() != null && !searchBulkBillRqDto.getEntityCode().isEmpty()) {
                    entity = entityRepo.findEntityByCode(searchBulkBillRqDto.getEntityCode());
                    predicates.add(cb.equal(root.get("entity"), entity));
                }

                LocalDateTime issueDateFrom = Utils.parseDateFromString(searchBulkBillRqDto.getIssueDateFrom(), "yyyy-MM-dd");

                if (issueDateFrom != null) {
                    predicates.add(cb.greaterThanOrEqualTo(root.get("createDate"), issueDateFrom));
                }

                LocalDateTime issueDateTo = Utils.parseDateFromString(searchBulkBillRqDto.getIssueDateTo(), "yyyy-MM-dd");

                if (issueDateTo != null) {
                    predicates.add(cb.lessThanOrEqualTo(root.get("createDate"), issueDateTo.plusHours(24)));
                }

                predicates.add(cb.notEqual(root.get("bulkBillStatus"), BulkBillStatus.BULK_DELETED));
                predicates.add(cb.notEqual(root.get("bulkBillStatus"), BulkBillStatus.BULK_GENERATED));


                if (searchBulkBillRqDto.getBulkStatus() != null && !searchBulkBillRqDto.getBulkStatus().isEmpty()) {
                    BulkBillStatus bulkBillStatus = BulkBillStatus.getValue(searchBulkBillRqDto.getBulkStatus().get(0));
                    if (bulkBillStatus != null) {
                        predicates.add(cb.equal(root.get("bulkBillStatus"), bulkBillStatus));
                    }
                }

                Predicate andPredicate = cb.and(predicates.toArray(new Predicate[0]));

                return andPredicate;
            }
        }, pageable);

        List<BulkBill> bulkBillList = bulkBillPage.getContent();

        List<SearchBulkBillRsDto> searchBulkBillRsDtoList = new ArrayList<>();

        bulkBillList.forEach(bulkBill -> {
            SearchBulkBillRsDto searchBulkBillRsDto = new SearchBulkBillRsDto();
            searchBulkBillRsDto.setId(bulkBill.getId());
            searchBulkBillRsDto.setReference(bulkBill.getReference());
            searchBulkBillRsDto.setInnerName(bulkBill.getInnerName());
            searchBulkBillRsDto.setOuterName(bulkBill.getOuterName());
            searchBulkBillRsDto.setNumberOfBills(bulkBill.getNumberOfBills());
            searchBulkBillRsDto.setCreateDate(bulkBill.getCreateDate());
            searchBulkBillRsDto.setAsyncRqUID(bulkBill.getAsyncRqUID());
            searchBulkBillRsDto.setSuccessCount(bulkBill.getSuccessCount());
            searchBulkBillRsDto.setRejectCount(bulkBill.getRejectCount());
            searchBulkBillRsDto.setPendingCount(bulkBill.getPendingCount());

            searchBulkBillRsDto.setBulkBillStatus(bulkBill.getBulkBillStatus());
            searchBulkBillRsDto.setBulkBillStatusText(Utf8ResourceBundle.getString(bulkBill.getBulkBillStatus().name(), locale));

            searchBulkBillRsDto.setEntityCode(bulkBill.getEntity().getCode());
            searchBulkBillRsDto.setEntityName(bulkBill.getEntity().getBrandName(locale));
            searchBulkBillRsDtoList.add(searchBulkBillRsDto);
        });

        return new PageDtoRs(bulkBillPage.getTotalElements(), bulkBillPage.getTotalPages(), searchBulkBillRsDtoList);

    }

    public String exportPdf(SearchBulkBillRqDto searchBulkBillRqDto, EntityUser entityUser, Locale locale) {

        List<BulkBill> bulkBillList = bulkBillRepo.findAll(new Specification<BulkBill>() {


            public Predicate toPredicate(Root<BulkBill> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

                List<Predicate> predicates = new ArrayList<>();

                if (!Utils.isNullOrEmpty(searchBulkBillRqDto.getReference())) {
                    predicates.add(cb.like(cb.lower(root.get("reference")), "%" + searchBulkBillRqDto.getReference().toLowerCase() + "%"));
                }

                if (!Utils.isNullOrEmpty(searchBulkBillRqDto.getInnerName())) {
                    predicates.add(cb.like(cb.lower(root.get("innerName")), "%" + searchBulkBillRqDto.getInnerName().toLowerCase() + "%"));
                }

                if (!Utils.isNullOrEmpty(searchBulkBillRqDto.getOuterName())) {
                    predicates.add(cb.like(cb.lower(root.get("outerName")), "%" + searchBulkBillRqDto.getOuterName().toLowerCase() + "%"));
                }

                if (entityUser != null && entityUser.getEntity() != null) {
                    predicates.add(cb.equal(root.get("entity"), entityUser.getEntity()));
                }

                LocalDateTime issueDateFrom = Utils.parseDateFromString(searchBulkBillRqDto.getIssueDateFrom(), "yyyy-MM-dd");

                if (issueDateFrom != null) {
                    predicates.add(cb.greaterThanOrEqualTo(root.get("createDate"), issueDateFrom));
                }

                LocalDateTime issueDateTo = Utils.parseDateFromString(searchBulkBillRqDto.getIssueDateTo(), "yyyy-MM-dd");

                if (issueDateTo != null) {
                    predicates.add(cb.lessThanOrEqualTo(root.get("createDate"), issueDateTo.plusHours(24)));
                }

                predicates.add(cb.notEqual(root.get("bulkBillStatus"), BulkBillStatus.BULK_DELETED));

                List<Predicate> bulkBillStatusPredicateList = new ArrayList<>();

                if (searchBulkBillRqDto.getBulkStatus() != null && !searchBulkBillRqDto.getBulkStatus().isEmpty()) {

                    for (String bulkBillStatusCode : searchBulkBillRqDto.getBulkStatus()) {

                        if (Utils.isEnumValueExist(bulkBillStatusCode, BulkBillStatus.class)) {
                            Predicate bulkBillStatusPredicate = cb.equal(root.get("bulkBillStatus"), BulkBillStatus.valueOf(bulkBillStatusCode));
                            bulkBillStatusPredicateList.add(bulkBillStatusPredicate);
                        }
                    }
                }

                Predicate andPredicate = cb.and(predicates.toArray(new Predicate[0]));

                Predicate statusPredicate = cb.or(bulkBillStatusPredicateList.toArray(new Predicate[0]));

                Predicate finalPredicate = null;
                if (!bulkBillStatusPredicateList.isEmpty()) {
                    finalPredicate = cb.and(andPredicate, statusPredicate);
                } else {
                    finalPredicate = cb.and(andPredicate);
                }
                return finalPredicate;
            }
        });

        if (bulkBillList == null || bulkBillList.isEmpty()) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "no_bulks_found", locale);
        }

        JasperReport jasperReport = Utils.findReportResourceFile("BulkReportEntity", locale);
        if (jasperReport == null) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "error_while_generating_report", locale);
        }

        List<BulkBillReportEntityDtoRs> bulkBillReportEntityDtoRsList = Utils.fetchBulkReportEntityList(bulkBillList, locale);

        JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(bulkBillReportEntityDtoRsList);

        Map<String, Object> parameters = new HashMap<>();

        parameters.put("entityName", entityUser.getEntity().getBrandName(locale));
        parameters.put("entityCode", entityUser.getEntity().getCode());
        parameters.put("userName", entityUser.getUsername());
        parameters.put("numberOfBulks", bulkBillList.size() + "");

        JasperPrint jasperPrint = Utils.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
        if (jasperPrint == null) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "error_while_generating_report", locale);
        }

        String reportName = System.currentTimeMillis() + ".pdf";

        String finalReportPath = environment.getProperty("report.archive.path") + reportName;

        try {
            JasperExportManager.exportReportToPdfFile(jasperPrint, finalReportPath);

            File reportFile = new File(finalReportPath);

            ftpHandler.uploadFile(reportFile);

            reportFile.delete();

        } catch (JRException e) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "error_while_generating_report", locale);
        } catch (Exception e) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "error_while_generating_report", locale);
        }


        return environment.getProperty("report.archive.url") + reportName;
    }

    public String exportExcel(SearchBulkBillRqDto searchBulkBillRqDto, EntityUser entityUser, Locale locale) {
        List<BulkBill> bulkBillList = bulkBillRepo.findAll(new Specification<BulkBill>() {


            public Predicate toPredicate(Root<BulkBill> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

                List<Predicate> predicates = new ArrayList<>();

                if (!Utils.isNullOrEmpty(searchBulkBillRqDto.getReference())) {
                    predicates.add(cb.like(cb.lower(root.get("reference")), "%" + searchBulkBillRqDto.getReference().toLowerCase() + "%"));
                }

                if (!Utils.isNullOrEmpty(searchBulkBillRqDto.getInnerName())) {
                    predicates.add(cb.like(cb.lower(root.get("innerName")), "%" + searchBulkBillRqDto.getInnerName().toLowerCase() + "%"));
                }

                if (!Utils.isNullOrEmpty(searchBulkBillRqDto.getOuterName())) {
                    predicates.add(cb.like(cb.lower(root.get("outerName")), "%" + searchBulkBillRqDto.getOuterName().toLowerCase() + "%"));
                }

                if (entityUser != null && entityUser.getEntity() != null) {
                    predicates.add(cb.equal(root.get("entity"), entityUser.getEntity()));
                }

                LocalDateTime issueDateFrom = Utils.parseDateFromString(searchBulkBillRqDto.getIssueDateFrom(), "yyyy-MM-dd");

                if (issueDateFrom != null) {
                    predicates.add(cb.greaterThanOrEqualTo(root.get("createDate"), issueDateFrom));
                }

                LocalDateTime issueDateTo = Utils.parseDateFromString(searchBulkBillRqDto.getIssueDateTo(), "yyyy-MM-dd");

                if (issueDateTo != null) {
                    predicates.add(cb.lessThanOrEqualTo(root.get("createDate"), issueDateTo.plusHours(24)));
                }

                predicates.add(cb.notEqual(root.get("bulkBillStatus"), BulkBillStatus.BULK_DELETED));

                List<Predicate> bulkBillStatusPredicateList = new ArrayList<>();

                if (searchBulkBillRqDto.getBulkStatus() != null && !searchBulkBillRqDto.getBulkStatus().isEmpty()) {

                    for (String bulkBillStatusCode : searchBulkBillRqDto.getBulkStatus()) {

                        if (Utils.isEnumValueExist(bulkBillStatusCode, BulkBillStatus.class)) {
                            Predicate bulkBillStatusPredicate = cb.equal(root.get("bulkBillStatus"), BulkBillStatus.valueOf(bulkBillStatusCode));
                            bulkBillStatusPredicateList.add(bulkBillStatusPredicate);
                        }
                    }
                }

                Predicate andPredicate = cb.and(predicates.toArray(new Predicate[0]));

                Predicate statusPredicate = cb.or(bulkBillStatusPredicateList.toArray(new Predicate[0]));

                Predicate finalPredicate = null;
                if (!bulkBillStatusPredicateList.isEmpty()) {
                    finalPredicate = cb.and(andPredicate, statusPredicate);
                } else {
                    finalPredicate = cb.and(andPredicate);
                }
                return finalPredicate;
            }
        });

        if (bulkBillList == null || bulkBillList.isEmpty()) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "no_bulks_found", locale);
        }

        JasperReport jasperReport = Utils.findReportResourceFile("BulkReportEntity", locale);
        if (jasperReport == null) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "error_while_generating_report", locale);
        }

        List<BulkBillReportEntityDtoRs> bulkBillReportEntityDtoRsList = Utils.fetchBulkReportEntityList(bulkBillList, locale);

        JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(bulkBillReportEntityDtoRsList);

        Map<String, Object> parameters = new HashMap<>();

        parameters.put("entityName", entityUser.getEntity().getBrandName(locale));
        parameters.put("entityCode", entityUser.getEntity().getCode());
        parameters.put("userName", entityUser.getUsername());
        parameters.put("numberOfBulks", bulkBillList.size() + "");

        JasperPrint jasperPrint = Utils.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
        if (jasperPrint == null) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "error_while_generating_report", locale);
        }

        String reportName = System.currentTimeMillis() + ".xlsx";

        String finalReportPath = environment.getProperty("report.archive.path") + reportName;

        try {
            JRXlsxExporter exporter = new JRXlsxExporter();

            SimpleXlsxReportConfiguration reportConfig
                    = new SimpleXlsxReportConfiguration();
            reportConfig.setSheetNames(new String[]{"Bulks"});

            exporter.setConfiguration(reportConfig);
            exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(finalReportPath));
            exporter.exportReport();

            File reportFile = new File(finalReportPath);

            ftpHandler.uploadFile(reportFile);

            reportFile.delete();

        } catch (JRException e) {
            LOGGER.error(e);
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "error_while_generating_report", locale);
        } catch (Exception e) {
            LOGGER.error(e);
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "error_while_generating_report", locale);
        }


        return environment.getProperty("report.archive.url") + reportName;
    }

}
