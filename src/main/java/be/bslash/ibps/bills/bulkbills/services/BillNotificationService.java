package be.bslash.ibps.bills.bulkbills.services;

import be.bslash.ibps.bills.bulkbills.entities.Bill;
import be.bslash.ibps.bills.bulkbills.entities.BillEmail;
import be.bslash.ibps.bills.bulkbills.entities.BillMobileNumber;
import be.bslash.ibps.bills.bulkbills.entities.BillNotificationEmail;
import be.bslash.ibps.bills.bulkbills.entities.BillNotificationSms;
import be.bslash.ibps.bills.bulkbills.entities.BillNotificationTemplate;
import be.bslash.ibps.bills.bulkbills.entities.BulkBill;
import be.bslash.ibps.bills.bulkbills.entities.Entity;
import be.bslash.ibps.bills.bulkbills.entities.EntityUser;
import be.bslash.ibps.bills.bulkbills.entities.FcmNotification;
import be.bslash.ibps.bills.bulkbills.enums.BillNotificationType;
import be.bslash.ibps.bills.bulkbills.enums.BillStatus;
import be.bslash.ibps.bills.bulkbills.enums.BooleanFlag;
import be.bslash.ibps.bills.bulkbills.enums.Language;
import be.bslash.ibps.bills.bulkbills.enums.NotificationMedia;
import be.bslash.ibps.bills.bulkbills.enums.NotificationStatus;
import be.bslash.ibps.bills.bulkbills.enums.Status;
import be.bslash.ibps.bills.bulkbills.repositories.BillEmailRepo;
import be.bslash.ibps.bills.bulkbills.repositories.BillMobileNumberRepo;
import be.bslash.ibps.bills.bulkbills.repositories.BillNotificationEmailRepo;
import be.bslash.ibps.bills.bulkbills.repositories.BillNotificationSmsRepo;
import be.bslash.ibps.bills.bulkbills.repositories.BillNotificationTemplateRepo;
import be.bslash.ibps.bills.bulkbills.repositories.BillRepo;
import be.bslash.ibps.bills.bulkbills.repositories.BulkBillRepo;
import be.bslash.ibps.bills.bulkbills.repositories.EntityRepo;
import be.bslash.ibps.bills.bulkbills.repositories.EntityUserRepo;
import be.bslash.ibps.bills.bulkbills.repositories.FcmNotificationRepo;
import be.bslash.ibps.bills.bulkbills.utils.AES256;
import be.bslash.ibps.bills.bulkbills.utils.Utils;
import be.bslash.ibps.bills.bulkbills.ws.WebServiceHelper;
import be.bslash.ibps.bills.bulkbills.ws.WebServiceResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Service
@Transactional
public class BillNotificationService {

    private static final Logger LOGGER = LogManager.getLogger(BillNotificationService.class);

    @Autowired
    private BillNotificationEmailRepo billNotificationEmailRepo;

    @Autowired
    private BillNotificationSmsRepo billNotificationSmsRepo;

    @Autowired
    private BillNotificationTemplateRepo billNotificationTemplateRepo;

    @Autowired
    private BillRepo billRepo;


    @Autowired
    private WebServiceHelper webServiceHelper;

    @Autowired
    private Environment environment;

    @Autowired
    private AES256 aes256;

    @Autowired
    private BulkBillRepo bulkBillRepo;

    @Autowired
    private BillMobileNumberRepo billMobileNumberRepo;

    @Autowired
    private BillEmailRepo billEmailRepo;

    @Autowired
    private EntityRepo entityRepo;

    @Autowired
    private EntityUserRepo entityUserRepo;

    @Autowired
    private FcmNotificationRepo fcmNotificationRepo;


    public void sendBillNotificationIssue(Bill bill, EntityUser entityUser, Locale locale) {

        LocalDateTime issueDateInterval = LocalDate.now().atStartOfDay().plusHours(23).plusMinutes(59).plusSeconds(59);
        LOGGER.info("issueDateInterval: " + issueDateInterval + " bill: " + bill.getIssueDate());

        if (bill.getIssueDate().isAfter(issueDateInterval)) {
            return;
        }

        Entity entity = bill.getEntity();


        //Check if entity allow SMS send
        if (entity.getSendBillSmsBooleanFlag().equals(BooleanFlag.YES)) {

            Long totalSmsPoint = 0L;

            List<BillNotificationSms> billNotificationSmsList = new ArrayList<>();

            BooleanFlag smsIssueFlag = bill.getSmsIssueFlag();

            if (smsIssueFlag != null && smsIssueFlag.equals(BooleanFlag.YES)) {
                return;
            }

            Language notificationLanguage = entity.getSendBillLanguage();

            BillNotificationTemplate billNotificationTemplate = billNotificationTemplateRepo.find(NotificationMedia.SMS, notificationLanguage, BillNotificationType.ISSUE);

            String entityNameAr = entity.getBrandNameAr();
            String entityNameEn = entity.getBrandNameEn();
            String totalAmount = Utils.roundDecimal(bill.getTotalAmount());
            String sadadNumber = bill.getSadadNumber();
            String billNumberEncoded = aes256.encrypt(sadadNumber + "_" + bill.getCycleNumber());
            String dateTime = Utils.parseDateTime(LocalDateTime.now(), "yyyy-MM-dd hh:mm:ss");

            String body = Utils.getSmsBody(billNotificationTemplate.getLayout(),
                    entityNameAr,
                    entityNameEn,
                    totalAmount,
                    sadadNumber,
                    billNumberEncoded,
                    dateTime);

            WebServiceResponse costSmsResponse = webServiceHelper.calculateSmsPoint(billNotificationTemplate.getTitle(), body, bill.getCustomerMobileNumber());
            LOGGER.info("costSmsResponse: " + costSmsResponse.getBody());

            Long smsPoint = Utils.getCostSms(costSmsResponse.getBody());
            LOGGER.info("smsPoint: " + smsPoint);

            WebServiceResponse sensSmsWebServiceResponse = webServiceHelper.sendSmsNotification(billNotificationTemplate.getTitle(), body, bill.getCustomerMobileNumber());
            LOGGER.info("sensSmsWebServiceResponse: " + sensSmsWebServiceResponse.getBody());

            String responseCode = Utils.getSmsResponseCode(sensSmsWebServiceResponse.getBody());
            LOGGER.info("smsResponseCode: " + responseCode);

            BillNotificationSms billNotificationSms = new BillNotificationSms();
            billNotificationSms.setBill(bill);
            billNotificationSms.setTitle(billNotificationTemplate.getTitle());
            billNotificationSms.setNotificationLanguage(notificationLanguage);
            billNotificationSms.setMobileNumber(bill.getCustomerMobileNumber());
            billNotificationSms.setUserReference(entityUser.getUsername());
            billNotificationSms.setCreateDate(LocalDateTime.now());
            billNotificationSms.setBody(body);
            billNotificationSms.setResponseCode(responseCode);
            billNotificationSms.setBillNotificationType(BillNotificationType.ISSUE);

            billRepo.updateSmsFlag(bill.getId());

            if (responseCode.equals("1") || responseCode.equals("M0000")) {
                totalSmsPoint += smsPoint;
                billNotificationSms.setPoint(smsPoint);
            } else {
                billNotificationSms.setPoint(0L);
            }

            billNotificationSmsList.add(billNotificationSms);

            List<BillMobileNumber> billMobileNumberList = billMobileNumberRepo.findAllBillMobileNumber(bill);
            if (billMobileNumberList != null && !billMobileNumberList.isEmpty()) {

                for (BillMobileNumber billMobileNumber : billMobileNumberList) {

                    WebServiceResponse costSmsResponseOther = webServiceHelper.calculateSmsPoint(billNotificationTemplate.getTitle(), body, billMobileNumber.getMobileNumber());
                    LOGGER.info("costSmsResponseOther: " + costSmsResponseOther.getBody());

                    Long smsPointOther = Utils.getCostSms(costSmsResponseOther.getBody());
                    LOGGER.info("smsPointOther: " + smsPointOther);

                    WebServiceResponse sensSmsWebServiceResponseOther = webServiceHelper.sendSmsNotification(billNotificationTemplate.getTitle(), body, billMobileNumber.getMobileNumber());
                    LOGGER.info("sensSmsWebServiceResponseOther: " + sensSmsWebServiceResponseOther.getBody());

                    String responseCodeOther = Utils.getSmsResponseCode(sensSmsWebServiceResponseOther.getBody());
                    LOGGER.info("smsResponseCode: " + responseCodeOther);

                    BillNotificationSms billNotificationSmsOther = new BillNotificationSms();
                    billNotificationSmsOther.setBill(bill);
                    billNotificationSmsOther.setTitle(billNotificationTemplate.getTitle());
                    billNotificationSmsOther.setNotificationLanguage(notificationLanguage);
                    billNotificationSmsOther.setMobileNumber(billMobileNumber.getMobileNumber());
                    billNotificationSmsOther.setUserReference(entityUser.getUsername());
                    billNotificationSmsOther.setCreateDate(LocalDateTime.now());
                    billNotificationSmsOther.setBody(body);
                    billNotificationSmsOther.setPoint(smsPointOther);
                    billNotificationSmsOther.setResponseCode(responseCodeOther);
                    billNotificationSmsOther.setBillNotificationType(BillNotificationType.ISSUE);

                    if (responseCodeOther.equals("1") || responseCodeOther.equals("M0000")) {
                        totalSmsPoint += smsPointOther;
                        billNotificationSmsOther.setPoint(smsPointOther);
                    } else {
                        billNotificationSmsOther.setPoint(0L);
                    }

                    billNotificationSmsList.add(billNotificationSmsOther);
                }
            }
            LOGGER.info("SMS LIST: " + billNotificationSmsList.size());

            billNotificationSmsRepo.saveAll(billNotificationSmsList);

            Entity updateEntity = entityRepo.findEntityForUpdate(entity.getId());
            Long point = updateEntity.getSmsPointBalance() - totalSmsPoint;
            entityRepo.updateSmsPointBalance(point, updateEntity.getId());
        }

        //Check if entity allow Email send
        if (entity.getSendBillEmailBooleanFlag().equals(BooleanFlag.YES)) {

            List<BillNotificationEmail> billNotificationEmailList = new ArrayList<>();


            if (bill.getCustomerEmailAddress() != null && !bill.getCustomerEmailAddress().isEmpty()) {
                Language notificationLanguage = entity.getSendBillLanguage();

                String fromEmail = "efaa.info@dci.sa";
                String subject = "New Invoice";
                String toEmail = bill.getCustomerEmailAddress();
                String type = "text/html";

                String layout = "لقد تم اصدار فاتورة جديدة من ENTITY_NAME_AR بمبلغ BILL_AMOUNT ريال وبرقم سداد SADAD_NUMBER, يمكنك دفع الفاتورة من رقم المفوتر 902(مدفوعات ايفاء).للإطلاع على الفاتورة يرجى الضغط على الرابط ادناه:";

                String entityNameAr = entity.getBrandNameAr();
                String entityNameEn = entity.getBrandNameEn();
                String totalAmount = Utils.roundDecimal(bill.getTotalAmount());
                String sadadNumber = bill.getSadadNumber();
                String billNumberEncoded = aes256.encrypt(sadadNumber + "_" + bill.getCycleNumber());
                String dateTime = Utils.parseDateTime(LocalDateTime.now(), "yyyy-MM-dd hh:mm:ss");

                String body = Utils.getSmsBody(layout,
                        entityNameAr,
                        entityNameEn,
                        totalAmount,
                        sadadNumber,
                        billNumberEncoded,
                        dateTime);

                String smsViewUrl = environment.getProperty("sms_view_url") + billNumberEncoded;

                String html = "<!DOCTYPE html>\n" +
                        "\n" +
                        "<title></title>\n" +
                        "\n" +
                        "<body style=\"direction: rtl;\">\n" +
                        "\n" +
                        "    <div style=\"width: 100%; text-align: center;\">\n" +
                        "        <img alt=\"logo\" src=\"" + bill.getLogo() + "\">\n" +
                        "    </div>\n" +
                        "\n" +
                        "    <h1>عزيزي عميل شركة " + entityNameAr + "</h1>\n" +
                        "\n" +
                        "    <p>\n" +
                        "        نأمل أن تحوز خدماتنا المقدمة اليكم \"الفاتورة الالكترونية - الدفع الالكتروني\" على رضاكم واستحسانكم.. \n" +
                        "    </p>\n" +
                        "\n" +
                        "    <p>\n" +
                        " نود إشعاركم بصدور فاتورة جديده بمبلغ " + totalAmount + " ريال - ورقم سداد " + sadadNumber + "،\n" +
                        "        يمكنك دفع الفاتورة الخاصة بكم من خلال رقم المفوتر 902 \"مدفوعات ايفاء\"..\n" +
                        "    </p>\n" +
                        "\n" +
                        "    <p>\n" +
                        "        وللاطلاع على تفاصيل الفاتورة يرجى الضغط على الرابط ادناه:\n" +
                        "    </p>\n" +
                        "    <p>\n" +
                        "        <a href=\"" + smsViewUrl + "\">اضغط هنا</a>\n" +
                        "    </p>\n" +
                        "\n" +
                        "    <p>\n" +
                        "        وشكرا لكم..\n" +
                        "    </p>\n" +
                        "\n" +
                        "</body>\n" +
                        "\n" +
                        "</html>";

                WebServiceResponse sensEmailWebServiceResponse = webServiceHelper.sendEmailNotification(fromEmail, subject, toEmail, type, html);
                LOGGER.info("sensEmailWebServiceResponse: " + sensEmailWebServiceResponse.getBody());

                LOGGER.info("smsResponseCode: " + sensEmailWebServiceResponse.getStatus());

                BillNotificationEmail issueEmailBillNotification = new BillNotificationEmail();
                issueEmailBillNotification.setBill(bill);
                issueEmailBillNotification.setTitle(subject);
                issueEmailBillNotification.setNotificationLanguage(Language.ALL);
                issueEmailBillNotification.setEmail(toEmail);
                issueEmailBillNotification.setUserReference(entityUser.getUsername());
                issueEmailBillNotification.setBody(html);
                issueEmailBillNotification.setResponseCode(sensEmailWebServiceResponse.getStatus().toString());
                issueEmailBillNotification.setCreateDate(LocalDateTime.now());
                issueEmailBillNotification.setBillNotificationType(BillNotificationType.ISSUE);

                billNotificationEmailList.add(issueEmailBillNotification);
            }


            List<BillEmail> billEmailList = billEmailRepo.findAllBillEmail(bill);
            if (billEmailList != null && !billEmailList.isEmpty()) {

                for (BillEmail billEmail : billEmailList) {

                    Language notificationLanguage = entity.getSendBillLanguage();

                    String fromEmail = "efaa.info@dci.sa";
                    String subject = "New Invoice";
                    String toEmail = billEmail.getEmail();
                    String type = "text/html";

                    String layout = "لقد تم اصدار فاتورة جديدة من ENTITY_NAME_AR بمبلغ BILL_AMOUNT ريال وبرقم سداد SADAD_NUMBER, يمكنك دفع الفاتورة من رقم المفوتر 902(مدفوعات ايفاء).للإطلاع على الفاتورة يرجى الضغط على الرابط ادناه:";

                    String entityNameAr = entity.getBrandNameAr();
                    String entityNameEn = entity.getBrandNameEn();
                    String totalAmount = Utils.roundDecimal(bill.getTotalAmount());
                    String sadadNumber = bill.getSadadNumber();
                    String billNumberEncoded = aes256.encrypt(sadadNumber + "_" + bill.getCycleNumber());
                    String dateTime = Utils.parseDateTime(LocalDateTime.now(), "yyyy-MM-dd hh:mm:ss");

                    String body = Utils.getSmsBody(layout,
                            entityNameAr,
                            entityNameEn,
                            totalAmount,
                            sadadNumber,
                            billNumberEncoded,
                            dateTime);

                    String smsViewUrl = environment.getProperty("sms_view_url") + billNumberEncoded;

                    String html = "<!DOCTYPE html>\n" +
                            "\n" +
                            "<title></title>\n" +
                            "\n" +
                            "<body style=\"direction: rtl;\">\n" +
                            "\n" +
                            "    <div style=\"width: 100%; text-align: center;\">\n" +
                            "        <img alt=\"logo\" src=\"" + bill.getLogo() + "\">\n" +
                            "    </div>\n" +
                            "\n" +
                            "    <h1>عزيزي عميل شركة " + entityNameAr + "</h1>\n" +
                            "\n" +
                            "    <p>\n" +
                            "        نأمل أن تحوز خدماتنا المقدمة اليكم \"الفاتورة الالكترونية - الدفع الالكتروني\" على رضاكم واستحسانكم.. \n" +
                            "    </p>\n" +
                            "\n" +
                            "    <p>\n" +
                            " نود إشعاركم بصدور فاتورة جديده بمبلغ " + totalAmount + " ريال - ورقم سداد " + sadadNumber + "،\n" +
                            "        يمكنك دفع الفاتورة الخاصة بكم من خلال رقم المفوتر 902 \"مدفوعات ايفاء\"..\n" +
                            "    </p>\n" +
                            "\n" +
                            "    <p>\n" +
                            "        وللاطلاع على تفاصيل الفاتورة يرجى الضغط على الرابط ادناه:\n" +
                            "    </p>\n" +
                            "    <p>\n" +
                            "        <a href=\"" + smsViewUrl + "\">اضغط هنا</a>\n" +
                            "    </p>\n" +
                            "\n" +
                            "    <p>\n" +
                            "        وشكرا لكم..\n" +
                            "    </p>\n" +
                            "\n" +
                            "</body>\n" +
                            "\n" +
                            "</html>";

                    WebServiceResponse sensEmailWebServiceResponse = webServiceHelper.sendEmailNotification(fromEmail, subject, toEmail, type, html);
                    LOGGER.info("sensEmailWebServiceResponse: " + sensEmailWebServiceResponse.getBody());

                    LOGGER.info("smsResponseCode: " + sensEmailWebServiceResponse.getStatus());

                    BillNotificationEmail issueEmailBillNotification = new BillNotificationEmail();
                    issueEmailBillNotification.setBill(bill);
                    issueEmailBillNotification.setTitle(subject);
                    issueEmailBillNotification.setNotificationLanguage(Language.ALL);
                    issueEmailBillNotification.setEmail(billEmail.getEmail());
                    issueEmailBillNotification.setUserReference(entityUser.getUsername());
                    issueEmailBillNotification.setBody(html);
                    issueEmailBillNotification.setResponseCode(sensEmailWebServiceResponse.getStatus().toString());
                    issueEmailBillNotification.setCreateDate(LocalDateTime.now());
                    issueEmailBillNotification.setBillNotificationType(BillNotificationType.ISSUE);

                    billNotificationEmailList.add(issueEmailBillNotification);
                }
            }
            LOGGER.info("EMAIL LIST: " + billNotificationEmailList.size());

            billNotificationEmailRepo.saveAll(billNotificationEmailList);
        }
    }

    public void sendBillNotificationReminderBulk(Long fileId, EntityUser entityUser, Locale locale) {

        BulkBill bulkBill = bulkBillRepo.findBulkBillById(fileId);

        if (bulkBill == null) {
            return;
        }

        Entity entity = bulkBill.getEntity();

        List<Bill> billList = billRepo.findAllByBulkBillAndBillStatus(bulkBill, BillStatus.APPROVED_BY_SADAD);

        List<BillMobileNumber> billMobileNumberList = billMobileNumberRepo.findAllBillMobileNumberByBulk(bulkBill);

        List<BillEmail> billEmailList = billEmailRepo.findAllBillEmailByBulk(bulkBill);

        List<BillNotificationEmail> billNotificationEmailList = new ArrayList<>();

        List<BillNotificationSms> billNotificationSmsList = new ArrayList<>();

        Long totalSmsPoint = 0L;

        if (billList != null && !billList.isEmpty()) {

            Language notificationLanguage = entity.getSendBillLanguage();

            BillNotificationTemplate billNotificationTemplate = billNotificationTemplateRepo.find(NotificationMedia.SMS, notificationLanguage, BillNotificationType.REMINDER);

            for (Bill bill : billList) {

                //Check if entity allow SMS send
                if (entity.getSendBillSmsBooleanFlag().equals(BooleanFlag.YES)) {

                    BooleanFlag smsIssueFlag = bill.getSmsIssueFlag();

                    String entityNameAr = entity.getBrandNameAr();
                    String entityNameEn = entity.getBrandNameEn();
                    String totalAmount = Utils.roundDecimal(bill.getTotalAmount());
                    String sadadNumber = bill.getSadadNumber();
                    String billNumberEncoded = aes256.encrypt(sadadNumber + "_" + bill.getCycleNumber());
                    String dateTime = Utils.parseDateTime(LocalDateTime.now(), "yyyy-MM-dd hh:mm:ss");

                    String body = Utils.getSmsBody(billNotificationTemplate.getLayout(),
                            entityNameAr,
                            entityNameEn,
                            totalAmount,
                            sadadNumber,
                            billNumberEncoded,
                            dateTime);

                    WebServiceResponse costSmsResponse = webServiceHelper.calculateSmsPoint(billNotificationTemplate.getTitle(), body, bill.getCustomerMobileNumber());
                    LOGGER.info("costSmsResponse: " + costSmsResponse.getBody());

                    Long smsPoint = Utils.getCostSms(costSmsResponse.getBody());
                    LOGGER.info("smsPoint: " + smsPoint);

                    WebServiceResponse sensSmsWebServiceResponse = webServiceHelper.sendSmsNotification(billNotificationTemplate.getTitle(), body, bill.getCustomerMobileNumber());
                    LOGGER.info("sensSmsWebServiceResponse: " + sensSmsWebServiceResponse.getBody());

                    String responseCode = Utils.getSmsResponseCode(sensSmsWebServiceResponse.getBody());
                    LOGGER.info("smsResponseCode: " + responseCode);

                    BillNotificationSms billNotificationSms = new BillNotificationSms();
                    billNotificationSms.setBill(bill);
                    billNotificationSms.setTitle(billNotificationTemplate.getTitle());
                    billNotificationSms.setNotificationLanguage(notificationLanguage);
                    billNotificationSms.setMobileNumber(bill.getCustomerMobileNumber());
                    billNotificationSms.setUserReference(entityUser.getUsername());
                    billNotificationSms.setCreateDate(LocalDateTime.now());
                    billNotificationSms.setBody(body);
                    billNotificationSms.setResponseCode(responseCode);
                    billNotificationSms.setBillNotificationType(BillNotificationType.REMINDER);

                    if (responseCode.equals("1") || responseCode.equals("M0000")) {
                        totalSmsPoint += smsPoint;
                        billNotificationSms.setPoint(smsPoint);
                    } else {
                        billNotificationSms.setPoint(0L);
                    }

                    billNotificationSmsList.add(billNotificationSms);
                }

                //Check if entity allow Email send
                if (entity.getSendBillEmailBooleanFlag().equals(BooleanFlag.YES)) {


                    if (bill.getCustomerEmailAddress() != null && !bill.getCustomerEmailAddress().isEmpty()) {

                        String fromEmail = "efaa.info@dci.sa";
                        String subject = "New Invoice";
                        String toEmail = bill.getCustomerEmailAddress();
                        String type = "text/html";

                        String layout = "تذكير اصدار فاتورة جديدة من ENTITY_NAME_AR بمبلغ BILL_AMOUNT ريال وبرقم سداد SADAD_NUMBER, يمكنك دفع الفاتورة من رقم المفوتر 902(مدفوعات ايفاء).للإطلاع على الفاتورة يرجى الضغط على الرابط ادناه:";

                        String entityNameAr = entity.getBrandNameAr();
                        String entityNameEn = entity.getBrandNameEn();
                        String totalAmount = Utils.roundDecimal(bill.getTotalAmount());
                        String sadadNumber = bill.getSadadNumber();
                        String billNumberEncoded = aes256.encrypt(sadadNumber + "_" + bill.getCycleNumber());
                        String dateTime = Utils.parseDateTime(LocalDateTime.now(), "yyyy-MM-dd hh:mm:ss");

                        String body = Utils.getSmsBody(layout,
                                entityNameAr,
                                entityNameEn,
                                totalAmount,
                                sadadNumber,
                                billNumberEncoded,
                                dateTime);

                        String smsViewUrl = environment.getProperty("sms_view_url") + billNumberEncoded;

                        String html = "<!DOCTYPE html>\n" +
                                "\n" +
                                "<title></title>\n" +
                                "\n" +
                                "<body style=\"direction: rtl;\">\n" +
                                "\n" +
                                "    <div style=\"width: 100%; text-align: center;\">\n" +
                                "        <img alt=\"logo\" src=\"" + bill.getLogo() + "\">\n" +
                                "    </div>\n" +
                                "\n" +
                                "    <h1>عزيزي عميل شركة " + entityNameAr + "</h1>\n" +
                                "\n" +
                                "    <p>\n" +
                                "        نأمل أن تحوز خدماتنا المقدمة اليكم \"الفاتورة الالكترونية - الدفع الالكتروني\" على رضاكم واستحسانكم.. \n" +
                                "    </p>\n" +
                                "\n" +
                                "    <p>\n" +
                                " نود تذكيركم بصدور فاتورة جديده بمبلغ " + totalAmount + " ريال - ورقم سداد " + sadadNumber + "،\n" +
                                "        يمكنك دفع الفاتورة الخاصة بكم من خلال رقم المفوتر 902 \"مدفوعات ايفاء\"..\n" +
                                "    </p>\n" +
                                "\n" +
                                "    <p>\n" +
                                "        وللاطلاع على تفاصيل الفاتورة يرجى الضغط على الرابط ادناه:\n" +
                                "    </p>\n" +
                                "    <p>\n" +
                                "        <a href=\"" + smsViewUrl + "\">اضغط هنا</a>\n" +
                                "    </p>\n" +
                                "\n" +
                                "    <p>\n" +
                                "        وشكرا لكم..\n" +
                                "    </p>\n" +
                                "\n" +
                                "</body>\n" +
                                "\n" +
                                "</html>";

                        WebServiceResponse sensEmailWebServiceResponse = webServiceHelper.sendEmailNotification(fromEmail, subject, toEmail, type, html);
                        LOGGER.info("sensEmailWebServiceResponse: " + sensEmailWebServiceResponse.getBody());

                        LOGGER.info("smsResponseCode: " + sensEmailWebServiceResponse.getStatus());

                        BillNotificationEmail issueEmailBillNotification = new BillNotificationEmail();
                        issueEmailBillNotification.setBill(bill);
                        issueEmailBillNotification.setTitle(subject);
                        issueEmailBillNotification.setNotificationLanguage(Language.ALL);
                        issueEmailBillNotification.setEmail(toEmail);
                        issueEmailBillNotification.setUserReference(entityUser.getUsername());
                        issueEmailBillNotification.setBody(html);
                        issueEmailBillNotification.setResponseCode(sensEmailWebServiceResponse.getStatus().toString());
                        issueEmailBillNotification.setCreateDate(LocalDateTime.now());
                        issueEmailBillNotification.setBillNotificationType(BillNotificationType.REMINDER);

                        billNotificationEmailList.add(issueEmailBillNotification);
                    }
                }
            }

            if (billMobileNumberList != null && !billMobileNumberList.isEmpty()) {

                for (BillMobileNumber billMobileNumber : billMobileNumberList) {

                    String entityNameAr = entity.getBrandNameAr();
                    String entityNameEn = entity.getBrandNameEn();
                    String totalAmount = Utils.roundDecimal(billMobileNumber.getBill().getTotalAmount());
                    String sadadNumber = billMobileNumber.getBill().getSadadNumber();
                    String billNumberEncoded = aes256.encrypt(sadadNumber + "_" + billMobileNumber.getBill().getCycleNumber());
                    String dateTime = Utils.parseDateTime(LocalDateTime.now(), "yyyy-MM-dd hh:mm:ss");

                    String body = Utils.getSmsBody(billNotificationTemplate.getLayout(),
                            entityNameAr,
                            entityNameEn,
                            totalAmount,
                            sadadNumber,
                            billNumberEncoded,
                            dateTime);

                    WebServiceResponse costSmsResponseOther = webServiceHelper.calculateSmsPoint(billNotificationTemplate.getTitle(), body, billMobileNumber.getMobileNumber());
                    LOGGER.info("costSmsResponseOther: " + costSmsResponseOther.getBody());

                    Long smsPointOther = Utils.getCostSms(costSmsResponseOther.getBody());
                    LOGGER.info("smsPointOther: " + smsPointOther);

                    WebServiceResponse sensSmsWebServiceResponseOther = webServiceHelper.sendSmsNotification(billNotificationTemplate.getTitle(), body, billMobileNumber.getMobileNumber());
                    LOGGER.info("sensSmsWebServiceResponseOther: " + sensSmsWebServiceResponseOther.getBody());

                    String responseCodeOther = Utils.getSmsResponseCode(sensSmsWebServiceResponseOther.getBody());
                    LOGGER.info("smsResponseCode: " + responseCodeOther);

                    BillNotificationSms billNotificationSmsOther = new BillNotificationSms();
                    billNotificationSmsOther.setBill(billMobileNumber.getBill());
                    billNotificationSmsOther.setTitle(billNotificationTemplate.getTitle());
                    billNotificationSmsOther.setNotificationLanguage(notificationLanguage);
                    billNotificationSmsOther.setMobileNumber(billMobileNumber.getMobileNumber());
                    billNotificationSmsOther.setUserReference(entityUser.getUsername());
                    billNotificationSmsOther.setCreateDate(LocalDateTime.now());
                    billNotificationSmsOther.setBody(body);
                    billNotificationSmsOther.setPoint(smsPointOther);
                    billNotificationSmsOther.setResponseCode(responseCodeOther);
                    billNotificationSmsOther.setBillNotificationType(BillNotificationType.REMINDER);

                    if (responseCodeOther.equals("1") || responseCodeOther.equals("M0000")) {
                        totalSmsPoint += smsPointOther;
                        billNotificationSmsOther.setPoint(smsPointOther);
                    } else {
                        billNotificationSmsOther.setPoint(0L);
                    }
                    billNotificationSmsList.add(billNotificationSmsOther);
                }
            }


            if (billEmailList != null && !billEmailList.isEmpty()) {

                for (BillEmail billEmail : billEmailList) {


                    String fromEmail = "efaa.info@dci.sa";
                    String subject = "New Invoice";
                    String toEmail = billEmail.getEmail();
                    String type = "text/html";

                    String layout = "تذكير اصدار فاتورة جديدة من ENTITY_NAME_AR بمبلغ BILL_AMOUNT ريال وبرقم سداد SADAD_NUMBER, يمكنك دفع الفاتورة من رقم المفوتر 902(مدفوعات ايفاء).للإطلاع على الفاتورة يرجى الضغط على الرابط ادناه:";

                    String entityNameAr = entity.getBrandNameAr();
                    String entityNameEn = entity.getBrandNameEn();
                    String totalAmount = Utils.roundDecimal(billEmail.getBill().getTotalAmount());
                    String sadadNumber = billEmail.getBill().getSadadNumber();
                    String billNumberEncoded = aes256.encrypt(sadadNumber + "_" + billEmail.getBill().getCycleNumber());
                    String dateTime = Utils.parseDateTime(LocalDateTime.now(), "yyyy-MM-dd hh:mm:ss");

                    String body = Utils.getSmsBody(layout,
                            entityNameAr,
                            entityNameEn,
                            totalAmount,
                            sadadNumber,
                            billNumberEncoded,
                            dateTime);

                    String smsViewUrl = environment.getProperty("sms_view_url") + billNumberEncoded;

                    String html = "<!DOCTYPE html>\n" +
                            "\n" +
                            "<title></title>\n" +
                            "\n" +
                            "<body style=\"direction: rtl;\">\n" +
                            "\n" +
                            "    <div style=\"width: 100%; text-align: center;\">\n" +
                            "        <img alt=\"logo\" src=\"" + entity.getLogo() + "\">\n" +
                            "    </div>\n" +
                            "\n" +
                            "    <h1>عزيزي عميل شركة " + entityNameAr + "</h1>\n" +
                            "\n" +
                            "    <p>\n" +
                            "        نأمل أن تحوز خدماتنا المقدمة اليكم \"الفاتورة الالكترونية - الدفع الالكتروني\" على رضاكم واستحسانكم.. \n" +
                            "    </p>\n" +
                            "\n" +
                            "    <p>\n" +
                            " نود تذكيركم بصدور فاتورة جديده بمبلغ " + totalAmount + " ريال - ورقم سداد " + sadadNumber + "،\n" +
                            "        يمكنك دفع الفاتورة الخاصة بكم من خلال رقم المفوتر 902 \"مدفوعات ايفاء\"..\n" +
                            "    </p>\n" +
                            "\n" +
                            "    <p>\n" +
                            "        وللاطلاع على تفاصيل الفاتورة يرجى الضغط على الرابط ادناه:\n" +
                            "    </p>\n" +
                            "    <p>\n" +
                            "        <a href=\"" + smsViewUrl + "\">اضغط هنا</a>\n" +
                            "    </p>\n" +
                            "\n" +
                            "    <p>\n" +
                            "        وشكرا لكم..\n" +
                            "    </p>\n" +
                            "\n" +
                            "</body>\n" +
                            "\n" +
                            "</html>";


                    WebServiceResponse sensEmailWebServiceResponse = webServiceHelper.sendEmailNotification(fromEmail, subject, toEmail, type, html);
                    LOGGER.info("sensEmailWebServiceResponse: " + sensEmailWebServiceResponse.getBody());

                    LOGGER.info("smsResponseCode: " + sensEmailWebServiceResponse.getStatus());

                    BillNotificationEmail issueEmailBillNotification = new BillNotificationEmail();
                    issueEmailBillNotification.setBill(billEmail.getBill());
                    issueEmailBillNotification.setTitle(subject);
                    issueEmailBillNotification.setNotificationLanguage(Language.ALL);
                    issueEmailBillNotification.setEmail(billEmail.getEmail());
                    issueEmailBillNotification.setUserReference(entityUser.getUsername());
                    issueEmailBillNotification.setBody(html);
                    issueEmailBillNotification.setResponseCode(sensEmailWebServiceResponse.getStatus().toString());
                    issueEmailBillNotification.setCreateDate(LocalDateTime.now());
                    issueEmailBillNotification.setBillNotificationType(BillNotificationType.REMINDER);

                    billNotificationEmailList.add(issueEmailBillNotification);
                }
            }

            billNotificationSmsRepo.saveAll(billNotificationSmsList);
            billNotificationEmailRepo.saveAll(billNotificationEmailList);

            Entity updateEntity = entityRepo.findEntityForUpdate(entity.getId());
            Long point = updateEntity.getSmsPointBalance() - totalSmsPoint;
            entityRepo.updateSmsPointBalance(point, updateEntity.getId());
        }
    }

    public void sendFcmNotificationBulkIssue(Long fileId, EntityUser issueEntityUser, Locale locale) {

        BulkBill bulkBill = bulkBillRepo.findBulkBillById(fileId);

        if (bulkBill == null) {
            return;
        }

        Entity entity = bulkBill.getEntity();

        List<Bill> billList = billRepo.findAllByBulkBillAndBillStatus(bulkBill, BillStatus.APPROVED_BY_SADAD);

        if (entity.getIsEntityNotificationAllowed() == null || entity.equals(BooleanFlag.NO)) {
            return;
        }

        List<EntityUser> entityUserList = entityUserRepo.findEntityUserByEntity(entity);

        List<FcmNotification> fcmNotificationList = new ArrayList<>();


        for (EntityUser entityUser : entityUserList) {

            if (!Utils.isSendNotificationForRole(issueEntityUser, entityUser)) {
                continue;
            }

            for (Bill bill : billList) {
                String title = "Issue Invoice";
                String body = "Dear " + entityUser.getUsername() + "\n" +
                        "New bill of amount " + bill.getTotalAmount() + " SAR \n" +
                        "Has been issued";

                List<String> tokenList = new ArrayList<>();
                tokenList.add(entityUser.getFcmToken());

                JSONObject dataJsonObject = new JSONObject();
                dataJsonObject.put("type", "BILL_ISSUE");
                dataJsonObject.put("referenceId", bill.getId());

                if (entityUser.getEntity().getIsEntityNotificationAllowed().equals(BooleanFlag.NO)) {
                    continue;
                }

                FcmNotification fcmNotification = new FcmNotification();
                fcmNotification.setTitle(title);
                fcmNotification.setBody(body);
                fcmNotification.setCreateDate(LocalDateTime.now());
                fcmNotification.setIsReadFlag(BooleanFlag.NO);
                fcmNotification.setNotificationLanguage(Language.ALL);
                fcmNotification.setUserId(entityUser.getId());
                fcmNotification.setUserType(1);
                fcmNotification.setData(dataJsonObject.toString());
                fcmNotification.setStatus(Status.ACTIVE);
                fcmNotification.setCategory("ISSUE");
                fcmNotification.setNotificationStatus(NotificationStatus.PENDING);

                fcmNotificationList.add(fcmNotification);
            }
        }

//        WebServiceResponse fcmWebServiceResponse = webServiceHelper.sendFcmNotification(title, body, dataJsonObject.toString(), tokenList);
//
//        LOGGER.info("fcmWebServiceStatus: " + fcmWebServiceResponse.getStatus());
//        LOGGER.info("fcmWebServiceResponse: " + fcmWebServiceResponse.getBody());


        fcmNotificationRepo.saveAll(fcmNotificationList);

    }

    public void sendBillNotificationIssueBulk(Long fileId, EntityUser entityUser, Locale locale) {

        BulkBill bulkBill = bulkBillRepo.findBulkBillById(fileId);

        if (bulkBill == null) {
            return;
        }

        Entity entity = bulkBill.getEntity();

        List<Bill> billList = billRepo.findAllByBulkBillAndBillStatus(bulkBill, BillStatus.APPROVED_BY_SADAD);

        List<BillMobileNumber> billMobileNumberList = billMobileNumberRepo.findAllBillMobileNumberByBulk(bulkBill);

        List<BillEmail> billEmailList = billEmailRepo.findAllBillEmailByBulk(bulkBill);

        List<BillNotificationEmail> billNotificationEmailList = new ArrayList<>();

        List<BillNotificationSms> billNotificationSmsList = new ArrayList<>();

        Long totalSmsPoint = 0L;

        if (billList != null && !billList.isEmpty()) {

            Language notificationLanguage = entity.getSendBillLanguage();

            BillNotificationTemplate billNotificationTemplate = billNotificationTemplateRepo.find(NotificationMedia.SMS, notificationLanguage, BillNotificationType.ISSUE);

            for (Bill bill : billList) {

                //Check if entity allow SMS send
                if (entity.getSendBillSmsBooleanFlag().equals(BooleanFlag.YES)) {

                    BooleanFlag smsIssueFlag = bill.getSmsIssueFlag();

                    String entityNameAr = entity.getBrandNameAr();
                    String entityNameEn = entity.getBrandNameEn();
                    String totalAmount = Utils.roundDecimal(bill.getTotalAmount());
                    String sadadNumber = bill.getSadadNumber();
                    String billNumberEncoded = aes256.encrypt(sadadNumber + "_" + bill.getCycleNumber());
                    String dateTime = Utils.parseDateTime(LocalDateTime.now(), "yyyy-MM-dd hh:mm:ss");

                    String body = Utils.getSmsBody(billNotificationTemplate.getLayout(),
                            entityNameAr,
                            entityNameEn,
                            totalAmount,
                            sadadNumber,
                            billNumberEncoded,
                            dateTime);

                    WebServiceResponse costSmsResponse = webServiceHelper.calculateSmsPoint(billNotificationTemplate.getTitle(), body, bill.getCustomerMobileNumber());
                    LOGGER.info("costSmsResponse: " + costSmsResponse.getBody());

                    Long smsPoint = Utils.getCostSms(costSmsResponse.getBody());
                    LOGGER.info("smsPoint: " + smsPoint);

                    WebServiceResponse sensSmsWebServiceResponse = webServiceHelper.sendSmsNotification(billNotificationTemplate.getTitle(), body, bill.getCustomerMobileNumber());
                    LOGGER.info("sensSmsWebServiceResponse: " + sensSmsWebServiceResponse.getBody());

                    String responseCode = Utils.getSmsResponseCode(sensSmsWebServiceResponse.getBody());
                    LOGGER.info("smsResponseCode: " + responseCode);

                    BillNotificationSms billNotificationSms = new BillNotificationSms();
                    billNotificationSms.setBill(bill);
                    billNotificationSms.setTitle(billNotificationTemplate.getTitle());
                    billNotificationSms.setNotificationLanguage(notificationLanguage);
                    billNotificationSms.setMobileNumber(bill.getCustomerMobileNumber());
                    billNotificationSms.setUserReference(entityUser.getUsername());
                    billNotificationSms.setCreateDate(LocalDateTime.now());
                    billNotificationSms.setBody(body);
                    billNotificationSms.setResponseCode(responseCode);
                    billNotificationSms.setBillNotificationType(BillNotificationType.ISSUE);

                    if (responseCode.equals("1") || responseCode.equals("M0000")) {
                        totalSmsPoint += smsPoint;
                        billNotificationSms.setPoint(smsPoint);
                    } else {
                        billNotificationSms.setPoint(0L);
                    }

                    bill.setSmsIssueFlag(BooleanFlag.YES);

                    billNotificationSmsList.add(billNotificationSms);
                }

                //Check if entity allow Email send
                if (entity.getSendBillEmailBooleanFlag().equals(BooleanFlag.YES)) {


                    if (bill.getCustomerEmailAddress() != null && !bill.getCustomerEmailAddress().isEmpty()) {

                        String fromEmail = "efaa.info@dci.sa";
                        String subject = "New Invoice";
                        String toEmail = bill.getCustomerEmailAddress();
                        String type = "text/html";

                        String layout = "تم اصدار فاتورة جديدة من ENTITY_NAME_AR بمبلغ BILL_AMOUNT ريال وبرقم سداد SADAD_NUMBER, يمكنك دفع الفاتورة من رقم المفوتر 902(مدفوعات ايفاء).للإطلاع على الفاتورة يرجى الضغط على الرابط ادناه:";

                        String entityNameAr = entity.getBrandNameAr();
                        String entityNameEn = entity.getBrandNameEn();
                        String totalAmount = Utils.roundDecimal(bill.getTotalAmount());
                        String sadadNumber = bill.getSadadNumber();
                        String billNumberEncoded = aes256.encrypt(sadadNumber + "_" + bill.getCycleNumber());
                        String dateTime = Utils.parseDateTime(LocalDateTime.now(), "yyyy-MM-dd hh:mm:ss");

                        String body = Utils.getSmsBody(layout,
                                entityNameAr,
                                entityNameEn,
                                totalAmount,
                                sadadNumber,
                                billNumberEncoded,
                                dateTime);

                        String smsViewUrl = environment.getProperty("sms_view_url") + billNumberEncoded;

                        String html = "<!DOCTYPE html>\n" +
                                "\n" +
                                "<title></title>\n" +
                                "\n" +
                                "<body style=\"direction: rtl;\">\n" +
                                "\n" +
                                "    <div style=\"width: 100%; text-align: center;\">\n" +
                                "        <img alt=\"logo\" src=\"" + bill.getLogo() + "\">\n" +
                                "    </div>\n" +
                                "\n" +
                                "    <h1>عزيزي عميل شركة " + entityNameAr + "</h1>\n" +
                                "\n" +
                                "    <p>\n" +
                                "        نأمل أن تحوز خدماتنا المقدمة اليكم \"الفاتورة الالكترونية - الدفع الالكتروني\" على رضاكم واستحسانكم.. \n" +
                                "    </p>\n" +
                                "\n" +
                                "    <p>\n" +
                                " نود اشعاركم بصدور فاتورة جديده بمبلغ " + totalAmount + " ريال - ورقم سداد " + sadadNumber + "،\n" +
                                "        يمكنك دفع الفاتورة الخاصة بكم من خلال رقم المفوتر 902 \"مدفوعات ايفاء\"..\n" +
                                "    </p>\n" +
                                "\n" +
                                "    <p>\n" +
                                "        وللاطلاع على تفاصيل الفاتورة يرجى الضغط على الرابط ادناه:\n" +
                                "    </p>\n" +
                                "    <p>\n" +
                                "        <a href=\"" + smsViewUrl + "\">اضغط هنا</a>\n" +
                                "    </p>\n" +
                                "\n" +
                                "    <p>\n" +
                                "        وشكرا لكم..\n" +
                                "    </p>\n" +
                                "\n" +
                                "</body>\n" +
                                "\n" +
                                "</html>";

                        WebServiceResponse sensEmailWebServiceResponse = webServiceHelper.sendEmailNotification(fromEmail, subject, toEmail, type, html);
                        LOGGER.info("sensEmailWebServiceResponse: " + sensEmailWebServiceResponse.getBody());

                        LOGGER.info("smsResponseCode: " + sensEmailWebServiceResponse.getStatus());

                        BillNotificationEmail issueEmailBillNotification = new BillNotificationEmail();
                        issueEmailBillNotification.setBill(bill);
                        issueEmailBillNotification.setTitle(subject);
                        issueEmailBillNotification.setNotificationLanguage(Language.ALL);
                        issueEmailBillNotification.setEmail(toEmail);
                        issueEmailBillNotification.setUserReference(entityUser.getUsername());
                        issueEmailBillNotification.setBody(html);
                        issueEmailBillNotification.setResponseCode(sensEmailWebServiceResponse.getStatus().toString());
                        issueEmailBillNotification.setCreateDate(LocalDateTime.now());
                        issueEmailBillNotification.setBillNotificationType(BillNotificationType.ISSUE);

                        billNotificationEmailList.add(issueEmailBillNotification);
                    }
                }
            }

            if (billMobileNumberList != null && !billMobileNumberList.isEmpty()) {

                for (BillMobileNumber billMobileNumber : billMobileNumberList) {

                    String entityNameAr = entity.getBrandNameAr();
                    String entityNameEn = entity.getBrandNameEn();
                    String totalAmount = Utils.roundDecimal(billMobileNumber.getBill().getTotalAmount());
                    String sadadNumber = billMobileNumber.getBill().getSadadNumber();
                    String billNumberEncoded = aes256.encrypt(sadadNumber + "_" + billMobileNumber.getBill().getCycleNumber());
                    String dateTime = Utils.parseDateTime(LocalDateTime.now(), "yyyy-MM-dd hh:mm:ss");

                    String body = Utils.getSmsBody(billNotificationTemplate.getLayout(),
                            entityNameAr,
                            entityNameEn,
                            totalAmount,
                            sadadNumber,
                            billNumberEncoded,
                            dateTime);

                    WebServiceResponse costSmsResponseOther = webServiceHelper.calculateSmsPoint(billNotificationTemplate.getTitle(), body, billMobileNumber.getMobileNumber());
                    LOGGER.info("costSmsResponseOther: " + costSmsResponseOther.getBody());

                    Long smsPointOther = Utils.getCostSms(costSmsResponseOther.getBody());
                    LOGGER.info("smsPointOther: " + smsPointOther);

                    WebServiceResponse sensSmsWebServiceResponseOther = webServiceHelper.sendSmsNotification(billNotificationTemplate.getTitle(), body, billMobileNumber.getMobileNumber());
                    LOGGER.info("sensSmsWebServiceResponseOther: " + sensSmsWebServiceResponseOther.getBody());

                    String responseCodeOther = Utils.getSmsResponseCode(sensSmsWebServiceResponseOther.getBody());
                    LOGGER.info("smsResponseCode: " + responseCodeOther);

                    BillNotificationSms billNotificationSmsOther = new BillNotificationSms();
                    billNotificationSmsOther.setBill(billMobileNumber.getBill());
                    billNotificationSmsOther.setTitle(billNotificationTemplate.getTitle());
                    billNotificationSmsOther.setNotificationLanguage(notificationLanguage);
                    billNotificationSmsOther.setMobileNumber(billMobileNumber.getMobileNumber());
                    billNotificationSmsOther.setUserReference(entityUser.getUsername());
                    billNotificationSmsOther.setCreateDate(LocalDateTime.now());
                    billNotificationSmsOther.setBody(body);
                    billNotificationSmsOther.setPoint(smsPointOther);
                    billNotificationSmsOther.setResponseCode(responseCodeOther);
                    billNotificationSmsOther.setBillNotificationType(BillNotificationType.ISSUE);

                    if (responseCodeOther.equals("1") || responseCodeOther.equals("M0000")) {
                        totalSmsPoint += smsPointOther;
                        billNotificationSmsOther.setPoint(smsPointOther);
                    } else {
                        billNotificationSmsOther.setPoint(0L);
                    }
                    billNotificationSmsList.add(billNotificationSmsOther);
                }
            }


            if (billEmailList != null && !billEmailList.isEmpty()) {

                for (BillEmail billEmail : billEmailList) {


                    String fromEmail = "efaa.info@dci.sa";
                    String subject = "New Invoice";
                    String toEmail = billEmail.getEmail();
                    String type = "text/html";

                    String layout = "تم اصدار فاتورة جديدة من ENTITY_NAME_AR بمبلغ BILL_AMOUNT ريال وبرقم سداد SADAD_NUMBER, يمكنك دفع الفاتورة من رقم المفوتر 902(مدفوعات ايفاء).للإطلاع على الفاتورة يرجى الضغط على الرابط ادناه:";

                    String entityNameAr = entity.getBrandNameAr();
                    String entityNameEn = entity.getBrandNameEn();
                    String totalAmount = Utils.roundDecimal(billEmail.getBill().getTotalAmount());
                    String sadadNumber = billEmail.getBill().getSadadNumber();
                    String billNumberEncoded = aes256.encrypt(sadadNumber + "_" + billEmail.getBill().getCycleNumber());
                    String dateTime = Utils.parseDateTime(LocalDateTime.now(), "yyyy-MM-dd hh:mm:ss");

                    String body = Utils.getSmsBody(layout,
                            entityNameAr,
                            entityNameEn,
                            totalAmount,
                            sadadNumber,
                            billNumberEncoded,
                            dateTime);

                    String smsViewUrl = environment.getProperty("sms_view_url") + billNumberEncoded;

                    String html = "<!DOCTYPE html>\n" +
                            "\n" +
                            "<title></title>\n" +
                            "\n" +
                            "<body style=\"direction: rtl;\">\n" +
                            "\n" +
                            "    <div style=\"width: 100%; text-align: center;\">\n" +
                            "        <img alt=\"logo\" src=\"" + entity.getLogo() + "\">\n" +
                            "    </div>\n" +
                            "\n" +
                            "    <h1>عزيزي عميل شركة " + entityNameAr + "</h1>\n" +
                            "\n" +
                            "    <p>\n" +
                            "        نأمل أن تحوز خدماتنا المقدمة اليكم \"الفاتورة الالكترونية - الدفع الالكتروني\" على رضاكم واستحسانكم.. \n" +
                            "    </p>\n" +
                            "\n" +
                            "    <p>\n" +
                            " نود اشعاركم بصدور فاتورة جديده بمبلغ " + totalAmount + " ريال - ورقم سداد " + sadadNumber + "،\n" +
                            "        يمكنك دفع الفاتورة الخاصة بكم من خلال رقم المفوتر 902 \"مدفوعات ايفاء\"..\n" +
                            "    </p>\n" +
                            "\n" +
                            "    <p>\n" +
                            "        وللاطلاع على تفاصيل الفاتورة يرجى الضغط على الرابط ادناه:\n" +
                            "    </p>\n" +
                            "    <p>\n" +
                            "        <a href=\"" + smsViewUrl + "\">اضغط هنا</a>\n" +
                            "    </p>\n" +
                            "\n" +
                            "    <p>\n" +
                            "        وشكرا لكم..\n" +
                            "    </p>\n" +
                            "\n" +
                            "</body>\n" +
                            "\n" +
                            "</html>";


                    WebServiceResponse sensEmailWebServiceResponse = webServiceHelper.sendEmailNotification(fromEmail, subject, toEmail, type, html);
                    LOGGER.info("sensEmailWebServiceResponse: " + sensEmailWebServiceResponse.getBody());

                    LOGGER.info("smsResponseCode: " + sensEmailWebServiceResponse.getStatus());

                    BillNotificationEmail issueEmailBillNotification = new BillNotificationEmail();
                    issueEmailBillNotification.setBill(billEmail.getBill());
                    issueEmailBillNotification.setTitle(subject);
                    issueEmailBillNotification.setNotificationLanguage(Language.ALL);
                    issueEmailBillNotification.setEmail(billEmail.getEmail());
                    issueEmailBillNotification.setUserReference(entityUser.getUsername());
                    issueEmailBillNotification.setBody(html);
                    issueEmailBillNotification.setResponseCode(sensEmailWebServiceResponse.getStatus().toString());
                    issueEmailBillNotification.setCreateDate(LocalDateTime.now());
                    issueEmailBillNotification.setBillNotificationType(BillNotificationType.ISSUE);

                    billNotificationEmailList.add(issueEmailBillNotification);
                }
            }

            billRepo.saveAll(billList);
            billNotificationSmsRepo.saveAll(billNotificationSmsList);
            billNotificationEmailRepo.saveAll(billNotificationEmailList);

            Entity updateEntity = entityRepo.findEntityForUpdate(entity.getId());
            Long point = updateEntity.getSmsPointBalance() - totalSmsPoint;
            entityRepo.updateSmsPointBalance(point, updateEntity.getId());
        }
    }

}
