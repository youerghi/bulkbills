package be.bslash.ibps.bills.bulkbills.repositories;

import be.bslash.ibps.bills.bulkbills.entities.BillItem;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BillItemRepo extends CrudRepository<BillItem, Long> {

    List<BillItem> findAll();

}
