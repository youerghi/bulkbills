package be.bslash.ibps.bills.bulkbills.dto.response;

import java.time.LocalDateTime;

public class BulkBillReportEntityDtoRs {

    private String reference;
    private String innerName;
    private String outerName;
    private LocalDateTime createDate;
    private Long numberOfBills;
    private Integer successCount;
    private Integer rejectCount;
    private String bulkBillStatus;

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getInnerName() {
        return innerName;
    }

    public void setInnerName(String innerName) {
        this.innerName = innerName;
    }

    public String getOuterName() {
        return outerName;
    }

    public void setOuterName(String outerName) {
        this.outerName = outerName;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public Long getNumberOfBills() {
        return numberOfBills;
    }

    public void setNumberOfBills(Long numberOfBills) {
        this.numberOfBills = numberOfBills;
    }

    public Integer getSuccessCount() {
        return successCount;
    }

    public void setSuccessCount(Integer successCount) {
        this.successCount = successCount;
    }

    public Integer getRejectCount() {
        return rejectCount;
    }

    public void setRejectCount(Integer rejectCount) {
        this.rejectCount = rejectCount;
    }

    public String getBulkBillStatus() {
        return bulkBillStatus;
    }

    public void setBulkBillStatus(String bulkBillStatus) {
        this.bulkBillStatus = bulkBillStatus;
    }
}
