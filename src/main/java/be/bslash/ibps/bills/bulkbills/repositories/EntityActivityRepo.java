package be.bslash.ibps.bills.bulkbills.repositories;

import be.bslash.ibps.bills.bulkbills.dto.projection.EntityActivityProjection;
import be.bslash.ibps.bills.bulkbills.entities.Entity;
import be.bslash.ibps.bills.bulkbills.entities.EntityActivity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface EntityActivityRepo extends JpaRepository<EntityActivity, Long> {
    List<EntityActivity> findAll();

    @Query("SELECT ea.id as id, ea.status as status, ea.entity.id as entityId, " +
            "ea.theLowestAmountPerUploadedBill as theLowestAmountPerUploadedBill, " +
            "ea.theHighestAmountPerUploadedBill as theHighestAmountPerUploadedBill, " +
            "ea.monthlyHighestValueOfExpectedUploadedBills as monthlyHighestValueOfExpectedUploadedBills " +
            "FROM EntityActivity ea " +
            "WHERE ea.id = :id")
    EntityActivityProjection findEntityActivityById(Long id);

    @Query("SELECT ea.id as id, ea.status as status, ea.entity.id as entityId, " +
            "ea.theLowestAmountPerUploadedBill as theLowestAmountPerUploadedBill, " +
            "ea.theHighestAmountPerUploadedBill as theHighestAmountPerUploadedBill, " +
            "ea.monthlyHighestValueOfExpectedUploadedBills as monthlyHighestValueOfExpectedUploadedBills " +
            "FROM EntityActivity ea " +
            "WHERE ea.code = :code " +
            "AND ea.status = 'ACTIVE'" +
            "AND ea.entity.id = :entityId")
    EntityActivityProjection findEntityActivity(
            @Param("code") String code,
            @Param("entityId") Long entityId);

    @Query("select entAct from EntityActivity entAct " +
            "where entAct.entity=:entity " +
            "and entAct.status ='ACTIVE' ")
    List<EntityActivity> findAllActiveActivityByEntity(@Param("entity") Entity entity);


}
