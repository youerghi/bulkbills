package be.bslash.ibps.bills.bulkbills.dto.request;

import be.bslash.ibps.bills.bulkbills.entities.Bill;
import be.bslash.ibps.bills.bulkbills.entities.BulkBill;

import java.util.List;

public class BulkBillRqDto {

    private Long bulkBillId;
    private List<Bill> billList;
    private BulkBill bulkBill;

    public Long getBulkBillId() {
        return bulkBillId;
    }

    public void setBulkBillId(Long bulkBillId) {
        this.bulkBillId = bulkBillId;
    }

    public List<Bill> getBillList() {
        return billList;
    }

    public void setBillList(List<Bill> billList) {
        this.billList = billList;
    }

    public BulkBill getBulkBill() {
        return bulkBill;
    }

    public void setBulkBill(BulkBill bulkBill) {
        this.bulkBill = bulkBill;
    }
}
