package be.bslash.ibps.bills.bulkbills.dto.projection;

import be.bslash.ibps.bills.bulkbills.enums.AccountCategory;
import be.bslash.ibps.bills.bulkbills.enums.IdType;
import be.bslash.ibps.bills.bulkbills.enums.Status;
import be.bslash.ibps.bills.bulkbills.enums.TypeIdDocument;

import java.math.BigDecimal;

public interface AccountProjection {

    Long getId();

    int getType();

    Status getStatus();

    AccountCategory getAccountCategory();

    String getPhoneNumber();

    String getEmail();

    String getFirstName();

    String getLastName();

    String getIdNumber();

    String getVatNumber();

    IdType getIdType();

    String getContactPersonNumber();

    String getContactPersonEmail();

    String getCompanyName();

    TypeIdDocument getTypeIdDocument();

    String getAccountNumber();

    BigDecimal getBalance();

    BigDecimal getMiniPartialAmount();
}
