package be.bslash.ibps.bills.bulkbills.services;

import be.bslash.ibps.bills.bulkbills.dto.request.BulkBillRqDto;
import be.bslash.ibps.bills.bulkbills.dto.request.SheetDTO;
import be.bslash.ibps.bills.bulkbills.dto.request.SheetLineDTO;
import be.bslash.ibps.bills.bulkbills.entities.Bill;
import be.bslash.ibps.bills.bulkbills.entities.BillCustomField;
import be.bslash.ibps.bills.bulkbills.entities.BillItem;
import be.bslash.ibps.bills.bulkbills.entities.BillTimeline;
import be.bslash.ibps.bills.bulkbills.entities.BulkBill;
import be.bslash.ibps.bills.bulkbills.entities.BulkBillTimeline;
import be.bslash.ibps.bills.bulkbills.entities.Config;
import be.bslash.ibps.bills.bulkbills.entities.EntityActivity;
import be.bslash.ibps.bills.bulkbills.entities.EntityField;
import be.bslash.ibps.bills.bulkbills.entities.EntityUser;
import be.bslash.ibps.bills.bulkbills.entities.EntityVat;
import be.bslash.ibps.bills.bulkbills.enums.BillCategory;
import be.bslash.ibps.bills.bulkbills.enums.BillSource;
import be.bslash.ibps.bills.bulkbills.enums.BillStatus;
import be.bslash.ibps.bills.bulkbills.enums.BillType;
import be.bslash.ibps.bills.bulkbills.enums.BooleanFlag;
import be.bslash.ibps.bills.bulkbills.enums.BulkBillStatus;
import be.bslash.ibps.bills.bulkbills.enums.BulkType;
import be.bslash.ibps.bills.bulkbills.enums.EntityUserType;
import be.bslash.ibps.bills.bulkbills.exceptions.HttpServiceException;
import be.bslash.ibps.bills.bulkbills.exceptions.ValidationMessageExcel;
import be.bslash.ibps.bills.bulkbills.repositories.BillCustomFieldRepo;
import be.bslash.ibps.bills.bulkbills.repositories.BillItemRepo;
import be.bslash.ibps.bills.bulkbills.repositories.BillRepo;
import be.bslash.ibps.bills.bulkbills.repositories.BillTimelineRepo;
import be.bslash.ibps.bills.bulkbills.repositories.BulkBillRepo;
import be.bslash.ibps.bills.bulkbills.repositories.BulkBillTimelineRepo;
import be.bslash.ibps.bills.bulkbills.repositories.ConfigRepo;
import be.bslash.ibps.bills.bulkbills.repositories.EntityActivityRepo;
import be.bslash.ibps.bills.bulkbills.repositories.EntityFieldRepo;
import be.bslash.ibps.bills.bulkbills.repositories.EntityRepo;
import be.bslash.ibps.bills.bulkbills.repositories.EntityVatRepo;
import be.bslash.ibps.bills.bulkbills.utils.Utils;
import be.bslash.ibps.bills.bulkbills.ws.WebServiceHelper;
import be.bslash.ibps.bills.bulkbills.ws.WebServiceResponse;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.w3c.dom.Document;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@Service
@Transactional
public class BulkBillUploadService {

    private final EntityActivityRepo entityActivityRepo;
    private final BulkBillRepo bulkBillRepo;
    private final BillRepo billRepo;
    private final EntityVatRepo entityVatRepo;
    private final EntityRepo entityRepo;
    private final BillCustomFieldRepo billCustomFieldRepo;
    private final BillItemRepo billItemRepo;
    private final BillTimelineRepo billTimelineRepo;
    private final BulkBillTimelineRepo bulkBillTimelineRepo;
    private final BillNotificationService billNotificationService;
    private final ConfigRepo configRepo;
    private final WebServiceHelper webServiceHelper;
    private final EntityFieldRepo entityFieldRepo;


    public BulkBillUploadService(EntityActivityRepo entityActivityRepo, BulkBillRepo bulkBillRepo,
                                 BillRepo billRepo, EntityVatRepo entityVatRepo, EntityRepo entityRepo,
                                 BillCustomFieldRepo billCustomFieldRepo, BillItemRepo billItemRepo,
                                 BillTimelineRepo billTimelineRepo, BulkBillTimelineRepo bulkBillTimelineRepo,
                                 BillNotificationService billNotificationService, ConfigRepo configRepo,
                                 WebServiceHelper webServiceHelper, EntityFieldRepo entityFieldRepo) {
        this.entityActivityRepo = entityActivityRepo;
        this.bulkBillRepo = bulkBillRepo;
        this.billRepo = billRepo;
        this.entityVatRepo = entityVatRepo;
        this.entityRepo = entityRepo;
        this.billCustomFieldRepo = billCustomFieldRepo;
        this.billItemRepo = billItemRepo;
        this.billTimelineRepo = billTimelineRepo;
        this.bulkBillTimelineRepo = bulkBillTimelineRepo;
        this.billNotificationService = billNotificationService;
        this.configRepo = configRepo;
        this.webServiceHelper = webServiceHelper;
        this.entityFieldRepo = entityFieldRepo;
    }

    public void uploadMasterBulkBillRefactored(BulkBillRqDto bulkBillRqDto, EntityUser entityUser, Locale locale) {

        if (!entityUser.getEntityUserType().equals(EntityUserType.OFFICER) && entityUser.getEntityUserType().equals(EntityUserType.SUPERVISOR)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "access_maker_user", locale);
        }
        System.out.println("Starting uploading");

        BulkBill bulkBill = bulkBillRqDto.getBulkBill();

        bulkBill.setBulkBillStatus(BulkBillStatus.BULK_MAKED);

        BulkBillTimeline bulkBillTimeline = new BulkBillTimeline();
        bulkBillTimeline.setUserReference(entityUser.getUsername());
        bulkBillTimeline.setActionDate(LocalDateTime.now());
        bulkBillTimeline.setBulkBillStatus(BulkBillStatus.BULK_MAKED);
        bulkBillTimeline.setBulkBill(bulkBill);
        bulkBillTimeline.setEntityUser(entityUser);

        bulkBillTimelineRepo.save(bulkBillTimeline);

        Map<String, List<BillCustomField>> customFieldListMap = new HashMap<>();
        Map<String, BillTimeline> billTimeLineMap = new HashMap<>();
        Map<String, List<BillItem>> billItemMap = new HashMap<>();
        List<Bill> billList = bulkBillRqDto.getBillList();

        List<BillCustomField> completeCustomsFieldList = new ArrayList<>();
        List<BillTimeline> completeBillTimeLineList = new ArrayList<>();
        List<BillItem> completeBillItemList = new ArrayList<>();

        billList
                .stream()
                .parallel()
                .forEach(bill -> {
                    bill.setBulkBill(bulkBill);
                    bill.setCreateDate(LocalDateTime.now());
                    List<BillCustomField> billCustomFieldList = bill.getBillCustomFieldList();
                    if (billCustomFieldList != null && billCustomFieldList.size() > 0) {
                        customFieldListMap.put(bill.getBillNumber(), billCustomFieldList);
                    }
                    BillTimeline billTimeline = new BillTimeline();
                    billTimeline.setUserReference(entityUser.getUsername());
                    billTimeline.setActionDate(LocalDateTime.now());
                    billTimeline.setEntityUser(entityUser);
                    billTimeline.setBillStatus(BillStatus.MAKED);
                    billTimeLineMap.put(bill.getBillNumber(), billTimeline);
                    if (bill.getBillItemList() != null && !bill.getBillItemList().isEmpty()) {
                        billItemMap.put(bill.getBillNumber(), bill.getBillItemList());
                    } else {
                        billItemMap.put(bill.getBillNumber(), new ArrayList<>());
                    }
                });

        billList = billRepo.saveAll(billList);

        for (Bill bill : billList) {
            if (customFieldListMap.containsKey(bill.getBillNumber())) {
                for (BillCustomField billCustomField : customFieldListMap.get(bill.getBillNumber())) {
                    billCustomField.setBill(bill);
                    completeCustomsFieldList.add(billCustomField);
                }
            }

            if (billTimeLineMap.containsKey(bill.getBillNumber())) {

                BillTimeline billTimeline = billTimeLineMap.get(bill.getBillNumber());
                billTimeline.setBill(bill);
                completeBillTimeLineList.add(billTimeline);
            }

            if (billItemMap.containsKey(bill.getBillNumber())) {
                List<BillItem> billItemList = billItemMap.get(bill.getBillNumber());
                for (BillItem billItem : billItemList) {
                    billItem.setBill(bill);
                    completeBillItemList.add(billItem);
                }
            }
        }

        if (completeCustomsFieldList.size() > 0) {
            billCustomFieldRepo.saveAll(completeCustomsFieldList);
        }

        if (completeBillTimeLineList.size() > 0) {
            billTimelineRepo.saveAll(completeBillTimeLineList);
        }

        if (completeBillItemList.size() > 0) {
            billItemRepo.saveAll(completeBillItemList);
        }

        bulkBill.setNumberOfBills((long) billList.size());

        if (entityUser.getEntityUserType().equals(EntityUserType.SUPER_ADMIN_PLUS)
                || entityUser.getEntityUserType().equals(EntityUserType.SUPER_ADMIN)
                || entityUser.getEntityUserType().equals(EntityUserType.SUPERVISOR_PLUS)) {
            this.checkBulkBillRefactored(bulkBill, entityUser, locale, billList);
        }
    }

    public void checkBulkBillRefactored(BulkBill bulkBill, EntityUser entityUser, Locale locale, List<Bill> billList) {
        if (!entityUser.getEntityUserType().equals(EntityUserType.SUPERVISOR) && entityUser.getEntityUserType().equals(EntityUserType.OFFICER)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "cannot_access_by_officer_user", locale);
        }

        if (bulkBill == null) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bulk_not_found", locale);
        }

        if (bulkBill.getBulkBillStatus().equals(BulkBillStatus.BULK_DELETED)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bulk_not_found", locale);
        }

        if (!bulkBill.getBulkBillStatus().equals(BulkBillStatus.BULK_MAKED)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bulk_not_maked", locale);
        }

        if (!bulkBill.getEntity().getId().equals(entityUser.getEntity().getId())) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bulk_not_for_company", locale);
        }

        if (isBulkBillStatusExist(bulkBill, BulkBillStatus.BULK_CHECKED)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bulk_already_checked", locale);
        }

        bulkBill.setBulkBillStatus(BulkBillStatus.BULK_CHECKED);

        BulkBillTimeline checkedBulkBillTimeline = new BulkBillTimeline();
        checkedBulkBillTimeline.setBulkBillStatus(BulkBillStatus.BULK_CHECKED);
        checkedBulkBillTimeline.setBulkBill(bulkBill);
        checkedBulkBillTimeline.setActionDate(LocalDateTime.now());
        checkedBulkBillTimeline.setEntityUser(entityUser);
        checkedBulkBillTimeline.setUserReference(entityUser.getUsername());

        bulkBillTimelineRepo.save(checkedBulkBillTimeline);

        List<BillTimeline> fullBillTimeLineList = new ArrayList<>();

        Long sadadNumberSeq = entityUser.getEntity().getBillSequence();

        for (Bill bill : billList) {
            bill.setBillStatus(BillStatus.CHECKED);

            BillTimeline checkedBillTimeline = new BillTimeline();
            checkedBillTimeline.setBillStatus(BillStatus.CHECKED);
            checkedBillTimeline.setBill(bill);
            checkedBillTimeline.setActionDate(LocalDateTime.now());
            checkedBillTimeline.setEntityUser(entityUser);
            checkedBillTimeline.setUserReference(entityUser.getUsername());

            // billTimelineRepo.save(checkedBillTimeline);
            fullBillTimeLineList.add(checkedBillTimeline);

            sadadNumberSeq += 1;

            String sadadNumber = (bill.getEntity().getCode()) + (bill.getEntityActivity().getCode()) + (sadadNumberSeq);

            bill.setSadadNumber(sadadNumber);

            bill.setBillStatus(BillStatus.UPLOADED_TO_SADAD);

            BillTimeline uploadedToSadadBillTimeline = new BillTimeline();
            uploadedToSadadBillTimeline.setBillStatus(BillStatus.UPLOADED_TO_SADAD);
            uploadedToSadadBillTimeline.setBill(bill);
            uploadedToSadadBillTimeline.setActionDate(LocalDateTime.now());
            uploadedToSadadBillTimeline.setEntityUser(entityUser);
            uploadedToSadadBillTimeline.setUserReference(entityUser.getUsername());

            fullBillTimeLineList.add(uploadedToSadadBillTimeline);
        }

        entityRepo.increaseSadadNumberCounter(sadadNumberSeq, entityUser.getEntity().getId());

        billTimelineRepo.saveAll(fullBillTimeLineList);

        bulkBill.setBulkBillStatus(BulkBillStatus.BULK_UPLOADED_TO_SADAD);

        String asyncRqUid = UUID.randomUUID().toString();
        bulkBill.setAsyncRqUID(asyncRqUid);

        BulkBillTimeline uploadedToSadadBulkBillTimeline = new BulkBillTimeline();
        uploadedToSadadBulkBillTimeline.setBulkBillStatus(BulkBillStatus.BULK_UPLOADED_TO_SADAD);
        uploadedToSadadBulkBillTimeline.setBulkBill(bulkBill);
        uploadedToSadadBulkBillTimeline.setActionDate(LocalDateTime.now());
        uploadedToSadadBulkBillTimeline.setEntityUser(entityUser);
        uploadedToSadadBulkBillTimeline.setUserReference(entityUser.getUsername());
        bulkBillTimelineRepo.save(uploadedToSadadBulkBillTimeline);

        Config config = configRepo.findConfigByKey("INTG_FLAG");

        if (config != null && config.getValue().equalsIgnoreCase("true")) {
            WebServiceResponse webServiceResponse = webServiceHelper.uploadBulk(bulkBill, billList);

            if (!webServiceResponse.getStatus().equals(200)) {
                Document documentXmlResponse = Utils.convertStringToXmlDocument(new JSONObject(webServiceResponse.getBody()).getString("data"));
                String message = Utils.getElementValueByTagName(documentXmlResponse, "faultstring");
                throw new HttpServiceException(HttpStatus.BAD_REQUEST, message, webServiceResponse.getBody(), locale);
            }
        } else {
            bulkBill.setBulkBillStatus(BulkBillStatus.BULK_APPROVED_BY_SADAD);

            BulkBillTimeline approvedBulkBillTimeline = new BulkBillTimeline();
            approvedBulkBillTimeline.setBulkBillStatus(BulkBillStatus.BULK_APPROVED_BY_SADAD);
            approvedBulkBillTimeline.setBulkBill(bulkBill);
            approvedBulkBillTimeline.setActionDate(LocalDateTime.now());
            approvedBulkBillTimeline.setEntityUser(entityUser);
            approvedBulkBillTimeline.setUserReference(entityUser.getUsername());

            bulkBillTimelineRepo.save(approvedBulkBillTimeline);

            fullBillTimeLineList = new ArrayList<>();


            LocalDateTime actionDate = LocalDateTime.now();
            for (Bill bill : billList) {
                BillTimeline approvedBillTimeline = new BillTimeline();
                approvedBillTimeline.setBillStatus(BillStatus.APPROVED_BY_SADAD);
                approvedBillTimeline.setBill(bill);
                approvedBillTimeline.setActionDate(actionDate);
                approvedBillTimeline.setEntityUser(entityUser);
                approvedBillTimeline.setUserReference(entityUser.getUsername());

                fullBillTimeLineList.add(approvedBillTimeline);
            }

            bulkBill.setSuccessCount(billList.size());
            bulkBill.setRejectCount(0);
            bulkBill.setPendingCount(0);

            billRepo.updateBillApproved(bulkBill.getId(), BillStatus.APPROVED_BY_SADAD, actionDate);
            billTimelineRepo.saveAll(fullBillTimeLineList);
            bulkBillRepo.save(bulkBill);

            new Thread(() -> {
                billNotificationService.sendFcmNotificationBulkIssue(bulkBill.getId(), entityUser, locale);

                billNotificationService.sendBillNotificationIssueBulk(bulkBill.getId(), entityUser, locale);
            }).start();
        }
    }

    public boolean isBulkBillStatusExist(BulkBill bulkBill, BulkBillStatus bulkBillStatus) {
        BulkBillTimeline checkTimeline = bulkBillTimelineRepo.findBulkBillTimelineByBulkBillAndBulkBillStatus(bulkBill, bulkBillStatus);

        return checkTimeline != null;
    }

    public BulkBillRqDto validateMasterBulkBill(MultipartFile multipartFile, EntityUser entityUser, Locale locale) {

        List<ValidationMessageExcel> validationMessageExcelList = new ArrayList<>();
        BulkBillRqDto bulkBillRqDto = new BulkBillRqDto();


        // -------------------------------

        List<EntityActivity> entityActivityList = entityActivityRepo.findAllActiveActivityByEntity(entityUser.getEntity());
        Map<String, EntityActivity> entityActivityMap = new HashMap<>();
        entityActivityList.forEach(entityActivity -> entityActivityMap.put(entityActivity.getCode(), entityActivity));

        List<Bill> currentBillList = billRepo.findBillByEntity(entityUser.getEntity());
        Map<String, Bill> entityBillsMapMap = new HashMap<>();
        currentBillList.forEach(bill -> {
            if (bill.getBillNumber() != null && !bill.getBillNumber().isEmpty()) {
                entityBillsMapMap.put(bill.getBillNumber(), bill);
            }
        });

        List<EntityVat> entityVatList = entityVatRepo.findEntityVatByEntity(entityUser.getEntity());
        Map<String, EntityVat> entityVatMap = new HashMap<>();
        entityVatList.forEach(entityVat -> entityVatMap.put(entityVat.getVat().toString(), entityVat));

        List<EntityField> entityFieldList = entityFieldRepo.findAllActiveEntityFieldByEntity(entityUser.getEntity());

        SheetDTO sheetDTO = sheetToDto(multipartFile, locale, validationMessageExcelList, entityFieldList);

        BulkBill bulkBill = bulkBillRepo.findBulkBill(sheetDTO.getFileReferenceValue(), sheetDTO.getOuterName(), entityUser.getEntity());

        if (bulkBill == null) {
            ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("fileReference", "file_reference_not_found", 0, 1, locale);
            validationMessageExcelList.add(validationMessageExcel);
        }

        if (bulkBill != null && !bulkBill.getBulkBillStatus().equals(BulkBillStatus.BULK_GENERATED)) {
            ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("fileReference", "file_reference_already_uploaded", 0, 1, locale);
            validationMessageExcelList.add(validationMessageExcel);
        }

        if (bulkBill != null) {
            bulkBillRqDto.setBulkBillId(bulkBill.getId());
            bulkBillRqDto.setBulkBill(bulkBill);
        }

        // -------------------------------

        BulkType bulkType = BulkType.getValue(sheetDTO.getBulkTypeCell());

        List<Bill> billList;
        AtomicReference<BigDecimal> sumAmount = new AtomicReference<>(new BigDecimal("0"));
        AtomicReference<Long> billNumberSequence = new AtomicReference<>(entityUser.getEntity().getAutoGenerationBillNumberSequence());
        if (bulkType == null || bulkType.name().equals("MASTER_BULK")) {

            billList = sheetDTO.getLines().parallelStream()
                    .map(line -> validateMasterSheetLine(entityUser, locale, validationMessageExcelList,
                            entityBillsMapMap, billNumberSequence, line))
                    .map(line -> mapMasterDtoToBill(entityUser, locale, validationMessageExcelList, entityActivityMap,
                            entityVatMap, bulkBill, sumAmount, line))
                    .collect(Collectors.toList());
        } else {
            Map<String, Bill> billMap = new HashMap<>();

            sheetDTO.getLines()
                    .forEach(line -> processDetailSheetLine(entityUser, locale, validationMessageExcelList, entityBillsMapMap,
                            billNumberSequence, line, billMap, entityVatMap, sumAmount, entityActivityMap, bulkBill));

            billList = new ArrayList<>(billMap.values());
        }

        entityRepo.increaseBillNumberCounter(billNumberSequence.get(), entityUser.getEntity().getId());

        billList.forEach(bill -> {
            BigDecimal totalAmount = bill.getTotalAmount();
            BigDecimal minimumPartialAMount = bill.getMiniPartialAmount();
            int compareResult = totalAmount.compareTo(minimumPartialAMount);

            if (compareResult == -1) {
                ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("minimumPartialAmount", "total amount should not less than minimum partial amount", (1), 1, locale);
                validationMessageExcelList.add(validationMessageExcel);
            }
        });


        bulkBillRqDto.setBillList(billList);

        if (!validationMessageExcelList.isEmpty()) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "invalid_file_data", validationMessageExcelList, locale);
        }

        return bulkBillRqDto;
    }

    private SheetLineDTO processDetailSheetLine(EntityUser entityUser, Locale locale,
                                                List<ValidationMessageExcel> validationMessageExcelList,
                                                Map<String, Bill> entityBillsMapMap,
                                                AtomicReference<Long> billNumberSequence,
                                                SheetLineDTO line, Map<String, Bill> billMap,
                                                Map<String, EntityVat> entityVatMap, AtomicReference<BigDecimal> sumAmount,
                                                Map<String, EntityActivity> entityActivityMap, BulkBill bulkBill) {
        if (billMap.get(line.getMobileNumberValue()) == null) {


            if (!Utils.isNullOrEmpty(line.getBillNumberValue())) {
                Bill bill = entityBillsMapMap.get(line.getBillNumberValue());
                if (bill != null) {
                    ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("billNumber", "bill number already exist", line.getRowIndex(), 2, locale);
                    validationMessageExcelList.add(validationMessageExcel);
                }
            }

            if (entityUser.getEntity().getIsAutoGenerateBillNumber().equals(BooleanFlag.YES) && Utils.isNullOrEmpty(line.getBillNumberValue())) {
                billNumberSequence.updateAndGet(v -> v + 1);
                line.setBillNumberValue(Utils.generateBillNumber(entityUser.getEntity()) + billNumberSequence);
            }

            if (Utils.isNullOrEmpty(line.getServiceNameValue())) {
                ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("serviceName", "service_name_empty", line.getRowIndex(), 2, locale);
                validationMessageExcelList.add(validationMessageExcel);
            }

            if (Utils.isNullOrEmpty(line.getCustomerNameValue())) {
                ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("customerName", "invalid_customer_name", line.getRowIndex(), 3, locale);
                validationMessageExcelList.add(validationMessageExcel);
            }

            if (Utils.isNullOrEmpty(line.getMobileNumberValue())) {
                ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("mobileNumber", "customer_mobile_number_empty", line.getRowIndex(), 4, locale);
                validationMessageExcelList.add(validationMessageExcel);
            }

            if (!Utils.isNullOrEmpty(line.getMobileNumberValue()) && !Utils.isValidMobileNumber(line.getMobileNumberValue())) {
                ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("mobileNumber", "invalid_mobile_number", line.getRowIndex(), 4, locale);
                validationMessageExcelList.add(validationMessageExcel);
            }

            if (!Utils.isNullOrEmpty(line.getEmailValue()) && !Utils.isValidEmail(line.getEmailValue())) {
                ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("email", "invalid_email", line.getRowIndex(), 5, locale);
                validationMessageExcelList.add(validationMessageExcel);
            }

            if (!Utils.isNullOrEmpty(line.getCustomerId()) && line.getCustomerId().length() < 10) {
                ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("customerId", "customer_id_number_not_valid", line.getRowIndex(), 6, locale);
                validationMessageExcelList.add(validationMessageExcel);
            }

            if (Utils.convertToBigDecimal(line.getAmountValue()) == null || Utils.isNullOrEmpty(line.getAmountValue())) {
                ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("amount", "invalid_amount_value", line.getRowIndex(), 7, locale);
                validationMessageExcelList.add(validationMessageExcel);
            }

            LocalDateTime issueDateTime = Utils.parseDateFromString(line.getCreationDateValue(), "yyyy-MM-dd");
            if (issueDateTime == null) {
                ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("issueDate", "issue_date_invalid", line.getRowIndex(), 11, locale);
                validationMessageExcelList.add(validationMessageExcel);
            }

            if (!Utils.isDateCorrect(line.getCreationDateValue(), "yyyy-MM-dd")) {
                ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("issueDate", "date_invalid", line.getRowIndex(), 11, locale);
                validationMessageExcelList.add(validationMessageExcel);
            }

            LocalDateTime expireDateTime = Utils.parseDateFromString(line.getExpireDateValue(), "yyyy-MM-dd");
            if (expireDateTime == null) {
                ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("expireDate", "expire_date_invalid", line.getRowIndex(), 12, locale);
                validationMessageExcelList.add(validationMessageExcel);
            }

            if (!Utils.isDateCorrect(line.getExpireDateValue(), "yyyy-MM-dd")) {
                ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("expireDate", "date_invalid", line.getRowIndex(), 12, locale);
                validationMessageExcelList.add(validationMessageExcel);
            }

            if (issueDateTime != null && expireDateTime != null && issueDateTime.isAfter(expireDateTime)) {
                ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("expireDate", "expire_date_after_issue", line.getRowIndex(), 12, locale);
                validationMessageExcelList.add(validationMessageExcel);
            }

            if (expireDateTime != null && expireDateTime.isBefore(LocalDate.now().atStartOfDay().plusHours(23).plusMinutes(59))) {
                ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("expireDate", "expire_date_is_passed", line.getRowIndex(), 12, locale);
                validationMessageExcelList.add(validationMessageExcel);
            }

            if (Utils.isNullOrEmpty(line.getActivityValue())) {
                ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("activity", "activity_value_empty", line.getRowIndex(), 14, locale);
                validationMessageExcelList.add(validationMessageExcel);
            }

            if (issueDateTime != null && expireDateTime != null && issueDateTime.equals(expireDateTime)) {
                ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("expireDate", "issuance_expire_date_same", line.getRowIndex(), 12, locale);
                validationMessageExcelList.add(validationMessageExcel);
            }

            if (Utils.isNullOrEmpty(line.getVatValue())) {
                ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("vat", "vat_value_empty", line.getRowIndex(), 15, locale);
                validationMessageExcelList.add(validationMessageExcel);
                line.setVatValue("0");
            }

            if ((line.getCustomerTaxNumber() != null) && (!line.getCustomerTaxNumber().isEmpty()) && (line.getCustomerTaxNumber().length() != 15)) {
                ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("customerTaxNumber", "VAT number length not correct", line.getRowIndex(), 5, locale);
                validationMessageExcelList.add(validationMessageExcel);
            }

            line.setIssueDateTime(issueDateTime);
            line.setExpireDateTime(expireDateTime);

            BooleanFlag vatExempted = BooleanFlag.NO;
            BooleanFlag vatNa = BooleanFlag.NO;
            BigDecimal vatAmount = null;
            switch (line.getVatValue()) {
                case "EXE":
                    line.setVatValue("0");
                    vatAmount = new BigDecimal("0");
                    vatExempted = (BooleanFlag.YES);
                    vatNa = (BooleanFlag.NO);
                    break;
                case "NA":
                    line.setVatValue("0");
                    vatAmount = new BigDecimal("0");
                    vatExempted = (BooleanFlag.NO);
                    vatNa = (BooleanFlag.YES);
                    break;
                default:
                    vatExempted = (BooleanFlag.NO);
                    vatNa = (BooleanFlag.NO);
                    vatAmount = new BigDecimal(line.getVatValue());
            }

            EntityVat entityVat = entityVatMap.get(vatAmount.toString());
            if (entityVat == null && vatNa.equals(BooleanFlag.NO) && vatExempted.equals(BooleanFlag.NO)) {
                ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("vat", "vat value is not part of selected vat list", line.getRowIndex(), 8, locale);
                validationMessageExcelList.add(validationMessageExcel);
            }


            BigDecimal billAmount = Utils.convertToBigDecimal(line.getAmountValue());

            BigDecimal discount = Utils.convertToBigDecimal(line.getDiscountValue());

            BigDecimal previousBalanceAmount = Utils.convertToBigDecimal(line.getPreviousBalance());

            BigDecimal minimumPartialAmount = Utils.convertToBigDecimal(line.getMinimumPartialValue());

            if (billAmount == null) {
                billAmount = new BigDecimal("0");
            }

            if (discount == null) {
                discount = new BigDecimal("0");
            }

            discount = discount.divide(new BigDecimal("100"));

            if (previousBalanceAmount == null) {
                previousBalanceAmount = new BigDecimal("0");
            }

            if (minimumPartialAmount == null) {
                minimumPartialAmount = entityUser.getEntity().getMiniPartialDefaultValue();
            }

            String discountType = "PERC";

            BigDecimal discountAmount = billAmount.multiply(discount);

            BigDecimal subAmountAfterDiscount = Utils.calculateSubAmountDiscount(billAmount, discount, discountType);

            subAmountAfterDiscount = subAmountAfterDiscount.subtract(discountAmount);

            BigDecimal totalAmount = Utils.calculateTotalAmountFromVatAndPreviousBalance(subAmountAfterDiscount, vatAmount.toString(), BigDecimal.ZERO);

            sumAmount.set(sumAmount.get().add(totalAmount));

            String activityId = "0";
            if (line.getActivityValue() != null && !line.getActivityValue().isEmpty()) {
                activityId = (line.getActivityValue().split("-")[0]);
                if (activityId == null) {
                    ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("activity", "activity_value_empty", line.getRowIndex(), 14, locale);
                    validationMessageExcelList.add(validationMessageExcel);
                }
            }

            EntityActivity entityActivity = entityActivityMap.get(activityId);
            if (entityActivity == null) {
                ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("activity", "activity_value_empty", line.getRowIndex(), 14, locale);
                validationMessageExcelList.add(validationMessageExcel);
            }

            Bill bill = new Bill();
            bill.setServiceName(line.getServiceNameValue());
            bill.setEntityActivity(entityActivity);
            bill.setBillNumber(line.getBillNumberValue());
            bill.setCustomerMobileNumber(line.getMobileNumberValue());
            bill.setCustomerEmailAddress(line.getEmailValue());
            bill.setCustomerFullName(line.getCustomerNameValue());
            bill.setCustomerAddress(line.getCustomerAddress());
            bill.setCustomerTaxNumber(line.getCustomerTaxNumber());
            bill.setCustomerIdNumber(line.getCustomerId());
            bill.setVat(BigDecimal.ZERO);
            bill.setSubAmount(totalAmount);
            bill.setEntity(entityUser.getEntity());
            bill.setEntityUser(entityUser);
            bill.setDiscount(new BigDecimal(0));
            bill.setCustomerPreviousBalance(previousBalanceAmount);
            bill.setTotalAmount(totalAmount);
            bill.setIssueDate(issueDateTime);
            bill.setExpireDate(expireDateTime);
            bill.setBillStatus(BillStatus.MAKED);
            bill.setBillSource(BillSource.FILE);
            bill.setBillType(BillType.DETAIL_BILL);
            bill.setBulkBill(bulkBill);
            bill.setServiceDescription(line.getDescriptionValue());
            bill.setBillCustomFieldList(line.getBillCustomFieldList());
            bill.setVatNaFlag(BooleanFlag.NO);
            bill.setVatExemptedFlag(BooleanFlag.NO);
            bill.setDiscountType(discountType);
            bill.setDiscount(discountAmount);
            bill.setIsPartialAllowed(BooleanFlag.getValue(line.getIsPartialValue()));
            bill.setMiniPartialAmount(minimumPartialAmount);
            bill.setLogo(entityUser.getEntity().getLogo());
            bill.setStamp(entityUser.getEntity().getStamp());

            if (bill.getIsPartialAllowed() != null && bill.getIsPartialAllowed().equals(BooleanFlag.YES)) {
                bill.setBillCategory(BillCategory.ONE_OFF_PARTIAL);
            } else {
                bill.setBillCategory(BillCategory.INSTANTLY);
                bill.setIsPartialAllowed(BooleanFlag.NO);
            }


            if (!Utils.isNullOrEmpty(line.getCampaignId())) {

                if (line.getCampaignId().split("-").length == 1) {
                    ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("campaignName", "not_valid_campaign_name", line.getRowIndex(), 16, locale);
                    validationMessageExcelList.add(validationMessageExcel);
                } else {
                    bill.setCampaignId(Integer.parseInt(line.getCampaignId().split("-")[0]));
                }
            }

            if (entityUser.getEntity().getIsPartialAllowed().equals(BooleanFlag.NO)) {
                bill.setIsPartialAllowed(BooleanFlag.NO);
                bill.setBillCategory(BillCategory.INSTANTLY);
            }

            List<BillItem> billItemList = new ArrayList<>();

            BillItem billItem = new BillItem();

            billItem.setBill(bill);
            billItem.setNumber(1);
            billItem.setQuantity(new BigDecimal("1"));
            billItem.setName(line.getServiceNameValue());
            billItem.setDiscountType(discountType);
            billItem.setDiscount(Utils.convertToBigDecimal(line.getDiscountValue()) != null ? Utils.convertToBigDecimal(line.getDiscountValue()) : BigDecimal.ZERO);
            billItem.setUnitPrice(billAmount);
            billItem.setTotalPrice(billAmount);
            billItem.setVat(vatAmount);
            billItem.setVatExemptedFlag(vatExempted);
            billItem.setVatNaFlag(vatNa);

            billItemList.add(billItem);

            bill.setBillItemList(billItemList);

            billMap.put(line.getMobileNumberValue(), bill);

            if (!Utils.isNullOrEmpty(bill.getBillNumber())) {
                entityBillsMapMap.put(bill.getBillNumber(), bill);
            }

            if (!Utils.isMinimumPartialAmountAllowed(bill)) {
                ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("minimumPartialAmount", "minimum partial amount should not less than activity minimum amount", line.getRowIndex(), 12, locale);
                validationMessageExcelList.add(validationMessageExcel);
            }
        } else {
            //Current Item Exist
            Bill bill = billMap.get(line.getMobileNumberValue());

            BooleanFlag vatExempted = BooleanFlag.NO;
            BooleanFlag vatNa = BooleanFlag.NO;
            BigDecimal vatAmount = null;
            switch (line.getVatValue()) {
                case "EXE":
                    line.setVatValue("0");
                    vatAmount = new BigDecimal("0");
                    vatExempted = (BooleanFlag.YES);
                    vatNa = (BooleanFlag.NO);
                    break;
                case "NA":
                    line.setVatValue("0");
                    vatAmount = new BigDecimal("0");
                    vatExempted = (BooleanFlag.NO);
                    vatNa = (BooleanFlag.YES);
                    break;
                default:
                    vatExempted = (BooleanFlag.NO);
                    vatNa = (BooleanFlag.NO);
                    vatAmount = new BigDecimal(line.getVatValue());
            }

            EntityVat entityVat = entityVatMap.get(vatAmount.toString());
            if (entityVat == null && vatNa.equals(BooleanFlag.NO) && vatExempted.equals(BooleanFlag.NO)) {
                ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("vat", "vat value is not part of selected vat list", line.getRowIndex(), 8, locale);
                validationMessageExcelList.add(validationMessageExcel);
            }


            BigDecimal billAmount = Utils.convertToBigDecimal(line.getAmountValue());

            BigDecimal discount = Utils.convertToBigDecimal(line.getDiscountValue());

            BigDecimal previousBalanceAmount = Utils.convertToBigDecimal(line.getPreviousBalance());

            BigDecimal minimumPartialAmount = Utils.convertToBigDecimal(line.getMinimumPartialValue());

            if (billAmount == null) {
                billAmount = new BigDecimal("0");
            }

            if (discount == null) {
                discount = new BigDecimal("0");
            }

            discount = discount.divide(new BigDecimal("100"));

            if (previousBalanceAmount == null) {
                previousBalanceAmount = new BigDecimal("0");
            }

            if (minimumPartialAmount == null) {
                minimumPartialAmount = new BigDecimal("0");
            }

            String discountType = "PERC";

            BigDecimal discountAmount = billAmount.multiply(discount);

            BigDecimal subAmountAfterDiscount = Utils.calculateSubAmountDiscount(billAmount, discount, discountType);

            subAmountAfterDiscount = subAmountAfterDiscount.subtract(discountAmount);

            BigDecimal totalAmount = Utils.calculateTotalAmountFromVatAndPreviousBalance(subAmountAfterDiscount, vatAmount.toString(), BigDecimal.ZERO);

            sumAmount.set(sumAmount.get().add(totalAmount));

            bill.setSubAmount(bill.getTotalAmount().add(totalAmount));
            bill.setTotalAmount(bill.getTotalAmount().add(totalAmount));
            bill.setDiscount(bill.getDiscount().add(discountAmount));

            BillItem billItem = new BillItem();

            billItem.setBill(bill);
            billItem.setNumber(bill.getBillItemList().size() + 1);
            billItem.setQuantity(new BigDecimal("1"));
            billItem.setName(line.getServiceNameValue());
            billItem.setDiscountType(discountType);
            billItem.setDiscount(Utils.convertToBigDecimal(line.getDiscountValue()) != null ? Utils.convertToBigDecimal(line.getDiscountValue()) : BigDecimal.ZERO);
            billItem.setUnitPrice(billAmount);
            billItem.setTotalPrice(billAmount);
            billItem.setVat(vatAmount);
            billItem.setVatExemptedFlag(vatExempted);
            billItem.setVatNaFlag(vatNa);

            bill.getBillItemList().add(billItem);

            if (bill.getBillItemList().size() > 10) {
                ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("itemSise", "item should not be more than 10", line.getRowIndex(), 1, locale);
                validationMessageExcelList.add(validationMessageExcel);
            }
        }


        return line;
    }

    private Bill mapMasterDtoToBill(EntityUser entityUser, Locale locale, List<ValidationMessageExcel> validationMessageExcelList,
                                    Map<String, EntityActivity> entityActivityMap, Map<String, EntityVat> entityVatMap,
                                    BulkBill bulkBill, AtomicReference<BigDecimal> sumAmount, SheetLineDTO line) {
        BooleanFlag vatExempted;
        BooleanFlag vatNa;
        BigDecimal vatAmount;
        switch (line.getVatValue()) {
            case "EXE":
                line.setVatValue("0");
                vatAmount = new BigDecimal("0");
                vatExempted = (BooleanFlag.YES);
                vatNa = (BooleanFlag.NO);
                break;
            case "NA":
                line.setVatValue("0");
                vatAmount = new BigDecimal("0");
                vatExempted = (BooleanFlag.NO);
                vatNa = (BooleanFlag.YES);
                break;
            default:
                vatExempted = (BooleanFlag.NO);
                vatNa = (BooleanFlag.NO);
                vatAmount = new BigDecimal(line.getVatValue());
        }

        EntityVat entityVat = entityVatMap.get(vatAmount.toString());
        if (entityVat == null && vatNa.equals(BooleanFlag.NO) && vatExempted.equals(BooleanFlag.NO)) {
            ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("vat", "vat value is not part of selected vat list", line.getRowIndex(), 8, locale);
            validationMessageExcelList.add(validationMessageExcel);
        }

        BigDecimal billAmount = Utils.convertToBigDecimal(line.getAmountValue());
        BigDecimal discount = Utils.convertToBigDecimal(line.getDiscountValue());
        BigDecimal previousBalanceAmount = Utils.convertToBigDecimal(line.getPreviousBalance());
        BigDecimal minimumPartialAmount = Utils.convertToBigDecimal(line.getMinimumPartialValue());

        if (billAmount == null) {
            billAmount = new BigDecimal("0");
        }

        if (discount == null) {
            discount = new BigDecimal("0");
        }

        discount = discount.divide(new BigDecimal(100));

        if (previousBalanceAmount == null) {
            previousBalanceAmount = new BigDecimal("0");
        }

        if (minimumPartialAmount == null) {
            minimumPartialAmount = entityUser.getEntity().getMiniPartialDefaultValue();
        }

        String discountType = "PERC";

        BigDecimal discountAmount = billAmount.multiply(discount);

        BigDecimal subAmountAfterDiscount = Utils.calculateSubAmountDiscount(billAmount, discount, discountType);

        subAmountAfterDiscount = subAmountAfterDiscount.subtract(discountAmount);

        BigDecimal totalAmount = Utils.calculateTotalAmountFromVatAndPreviousBalance(subAmountAfterDiscount, vatAmount.toString(), previousBalanceAmount);

        sumAmount.set(sumAmount.get().add(totalAmount));

        String activityId = "0";

        if (!Utils.isNullOrEmpty(line.getActivityValue())) {
            activityId = (line.getActivityValue().split("-")[0]);
            if (activityId == null) {
                ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("activity", "activity_value_empty", line.getRowIndex(), 14, locale);
                validationMessageExcelList.add(validationMessageExcel);
            }
        }

        EntityActivity entityActivity = entityActivityMap.get(activityId);

        if (entityActivity != null) {
            if (entityActivity.getTheLowestAmountPerUploadedBill() != null) {
                if (totalAmount.compareTo(entityActivity.getTheLowestAmountPerUploadedBill()) == -1) {
                    ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("amount", "bill_amount_limit_per_bill", line.getRowIndex(), 8, locale);
                    validationMessageExcelList.add(validationMessageExcel);
                }
            }

            if (entityActivity.getTheHighestAmountPerUploadedBill() != null) {
                if (totalAmount.compareTo(entityActivity.getTheHighestAmountPerUploadedBill()) == 1) {
                    ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("amount", "bill_amount_limit_per_bill", line.getRowIndex(), 8, locale);
                    validationMessageExcelList.add(validationMessageExcel);
                }
            }
        } else {
            ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("activity", "activity_value_empty", line.getRowIndex(), 14, locale);
            validationMessageExcelList.add(validationMessageExcel);
        }

        Bill bill = new Bill();
        bill.setServiceName(line.getServiceNameValue());
        bill.setEntityActivity(entityActivity);
        bill.setBillNumber(line.getBillNumberValue());
        bill.setCustomerMobileNumber(line.getMobileNumberValue());
        bill.setCustomerEmailAddress(line.getEmailValue());
        bill.setCustomerFullName(line.getCustomerNameValue());
        bill.setCustomerAddress(line.getCustomerAddress());
        bill.setCustomerTaxNumber(line.getCustomerTaxNumber());
        bill.setCustomerIdNumber(line.getCustomerId());
        bill.setVat(vatAmount);
        bill.setSubAmount(billAmount);
        bill.setEntity(entityUser.getEntity());
        bill.setEntityUser(entityUser);
        bill.setDiscount(new BigDecimal(0));
        bill.setCustomerPreviousBalance(previousBalanceAmount);
        bill.setBillCustomFieldList(line.getBillCustomFieldList());
        bill.setTotalAmount(totalAmount);
        bill.setIssueDate(line.getIssueDateTime());
        bill.setExpireDate(line.getExpireDateTime());
        bill.setBillStatus(BillStatus.MAKED);
        bill.setBillSource(BillSource.FILE);
        bill.setBillType(BillType.MASTER_BILL);
        bill.setBulkBill(bulkBill);
        bill.setServiceDescription(line.getDescriptionValue());
        bill.setVatNaFlag(vatNa);
        bill.setVatExemptedFlag(vatExempted);
        bill.setDiscountType(discountType);
        bill.setDiscount(Utils.convertToBigDecimal(line.getDiscountValue()) != null ? Utils.convertToBigDecimal(line.getDiscountValue()) : BigDecimal.ZERO);
        bill.setIsPartialAllowed(BooleanFlag.getValue(line.getIsPartialValue()));
        bill.setMiniPartialAmount(minimumPartialAmount);
        bill.setLogo(entityUser.getEntity().getLogo());
        bill.setStamp(entityUser.getEntity().getStamp());

        return bill;
    }

    private SheetLineDTO validateMasterSheetLine(EntityUser entityUser, Locale locale,
                                                 List<ValidationMessageExcel> validationMessageExcelList,
                                                 Map<String, Bill> entityBillsMapMap, AtomicReference<Long> billNumberSequence,
                                                 SheetLineDTO line) {
        if (!Utils.isNullOrEmpty(line.getBillNumberValue())) {
            Bill bill = entityBillsMapMap.get(line.getBillNumberValue());
            if (bill != null) {
                ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("billNumber", "bill number already exist", line.getRowIndex(), 2, locale);
                validationMessageExcelList.add(validationMessageExcel);
            }
        }

        if (entityUser.getEntity().getIsAutoGenerateBillNumber().equals(BooleanFlag.YES) && Utils.isNullOrEmpty(line.getBillNumberValue())) {
            billNumberSequence.updateAndGet(v -> v + 1);
            line.setBillNumberValue(Utils.generateBillNumber(entityUser.getEntity()) + billNumberSequence);
        }

        if (Utils.isNullOrEmpty(line.getServiceNameValue())) {
            ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("serviceName", "service_name_empty", line.getRowIndex(), 2, locale);
            validationMessageExcelList.add(validationMessageExcel);
        }

        if (Utils.isNullOrEmpty(line.getCustomerNameValue())) {
            ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("customerName", "invalid_customer_name", line.getRowIndex(), 3, locale);
            validationMessageExcelList.add(validationMessageExcel);
        }

        if (Utils.isNullOrEmpty(line.getMobileNumberValue())) {
            ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("mobileNumber", "customer_mobile_number_empty", line.getRowIndex(), 4, locale);
            validationMessageExcelList.add(validationMessageExcel);
        }

        if (!Utils.isNullOrEmpty(line.getMobileNumberValue()) && !Utils.isValidMobileNumber(line.getMobileNumberValue())) {
            ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("mobileNumber", "invalid_mobile_number", line.getRowIndex(), 4, locale);
            validationMessageExcelList.add(validationMessageExcel);
        }


        if (!Utils.isNullOrEmpty(line.getEmailValue()) && !Utils.isValidEmail(line.getEmailValue())) {
            ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("email", "invalid_email", line.getRowIndex(), 5, locale);
            validationMessageExcelList.add(validationMessageExcel);
        }

        if (!Utils.isNullOrEmpty(line.getCustomerId()) && line.getCustomerId().length() < 10) {
            ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("customerId", "customer_id_number_not_valid", line.getRowIndex(), 6, locale);
            validationMessageExcelList.add(validationMessageExcel);
        }

        if (Utils.isNullOrEmpty(line.getActivityValue()) || Utils.convertToBigDecimal(line.getAmountValue()) == null) {
            ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("amount", "invalid_amount_value", line.getRowIndex(), 7, locale);
            validationMessageExcelList.add(validationMessageExcel);
        }

        LocalDateTime issueDateTime = Utils.parseDateFromString(line.getCreationDateValue(), "yyyy-MM-dd");
        if (issueDateTime == null) {
            ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("issueDate", "issue_date_invalid", line.getRowIndex(), 11, locale);
            validationMessageExcelList.add(validationMessageExcel);
        }

        if (!Utils.isDateCorrect(line.getCreationDateValue(), "yyyy-MM-dd")) {
            ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("issueDate", "date_invalid", line.getRowIndex(), 11, locale);
            validationMessageExcelList.add(validationMessageExcel);
        }

        LocalDateTime expireDateTime = Utils.parseDateFromString(line.getExpireDateValue(), "yyyy-MM-dd");
        if (expireDateTime == null) {
            ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("expireDate", "expire_date_invalid", line.getRowIndex(), 12, locale);
            validationMessageExcelList.add(validationMessageExcel);
        }

        if (!Utils.isDateCorrect(line.getExpireDateValue(), "yyyy-MM-dd")) {
            ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("expireDate", "date_invalid", line.getRowIndex(), 12, locale);
            validationMessageExcelList.add(validationMessageExcel);
        }

        if (issueDateTime != null && expireDateTime != null && issueDateTime.isAfter(expireDateTime)) {
            ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("expireDate", "expire_date_after_issue", line.getRowIndex(), 12, locale);
            validationMessageExcelList.add(validationMessageExcel);
        }

        if (expireDateTime != null && expireDateTime.isBefore(LocalDate.now().atStartOfDay())) {
            ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("expireDate", "expire_date_is_passed", line.getRowIndex(), 12, locale);
            validationMessageExcelList.add(validationMessageExcel);
        }

        if (Utils.isNullOrEmpty(line.getActivityValue())) {
            ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("activity", "activity_value_empty", line.getRowIndex(), 14, locale);
            validationMessageExcelList.add(validationMessageExcel);
        }

        if (issueDateTime != null && expireDateTime != null && issueDateTime.equals(expireDateTime)) {
            ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("expireDate", "issuance_expire_date_same", line.getRowIndex(), 12, locale);
            validationMessageExcelList.add(validationMessageExcel);
        }

        line.setIssueDateTime(issueDateTime);
        line.setExpireDateTime(expireDateTime);

        if (Utils.isNullOrEmpty(line.getVatValue())) {
            ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("vat", "vat_value_empty", line.getRowIndex(), 15, locale);
            validationMessageExcelList.add(validationMessageExcel);
            line.setVatValue("0");
        }

        if ((line.getCustomerTaxNumber() != null) && (!line.getCustomerTaxNumber().isEmpty()) && (line.getCustomerTaxNumber().length() != 15)) {
            ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("customerTaxNumber", "VAT number length not correct", line.getRowIndex(), 5, locale);
            validationMessageExcelList.add(validationMessageExcel);
        }

        return line;
    }

    private SheetDTO sheetToDto(MultipartFile multipartFile,
                                Locale locale,
                                List<ValidationMessageExcel> validationMessageExcelList, List<EntityField> entityFieldList) {
        SheetDTO sheetDTO = new SheetDTO();
        Workbook workbook;

        try {
            workbook = WorkbookFactory.create(multipartFile.getInputStream());
        } catch (IOException e) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "error_while_reading_file", locale);
        }

        Sheet billsSheet = workbook.getSheetAt(0);

        if (billsSheet == null) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "empty_sheet", locale);
        }

        if (billsSheet.getLastRowNum() - 3 <= 0) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "empty_bills", locale);
        }

        Row fileReferenceRow = billsSheet.getRow(0);

        Cell fileReferenceCell = fileReferenceRow.getCell(1);
        if (fileReferenceCell == null) {
            ValidationMessageExcel validationMessageExcel = new ValidationMessageExcel("fileReference", "invalid_reference_cell", 0, 1, locale);
            validationMessageExcelList.add(validationMessageExcel);
        } else {
            sheetDTO.setFileReferenceValue(fileReferenceCell.getStringCellValue());
        }

        sheetDTO.setBulkTypeCell(fileReferenceRow.getCell(19).getStringCellValue());

        sheetDTO.setOuterName(multipartFile.getOriginalFilename());

        for (int rowIndex = 4; rowIndex <= billsSheet.getLastRowNum(); rowIndex++) {

            Row billRow = billsSheet.getRow(rowIndex);

            if (billRow == null) {
                continue;
            }

            DataFormatter formatter = new DataFormatter();

            Cell serviceNameCell = billRow.getCell(0);
            Cell mobileNumberCell = billRow.getCell(1);
            Cell amountCell = billRow.getCell(2);
            Cell vatCell = billRow.getCell(3);
            Cell discountCell = billRow.getCell(4);
            Cell activityCell = billRow.getCell(5);
            Cell billNumberCell = billRow.getCell(6);
            Cell customerNameCell = billRow.getCell(7);
            Cell emailCell = billRow.getCell(8);
            Cell customerIdCell = billRow.getCell(9);
            Cell customerAddressCell = billRow.getCell(10);
            Cell customerTaxNumberCell = billRow.getCell(11);
            Cell creationDateCell = billRow.getCell(12);
            Cell expireDateCell = billRow.getCell(13);
            Cell descriptionCell = billRow.getCell(14);
            Cell previousBalanceCell = billRow.getCell(15);
            Cell campaingCell = billRow.getCell(16);
            Cell isPartialCell = billRow.getCell(17);
            Cell minimumPartialCell = billRow.getCell(18);

            List<BillCustomField> billCustomFieldList = new ArrayList<>();
            for (int customFieldIndex = 19, entityFieldIndex = 0; customFieldIndex <= 28; customFieldIndex++, entityFieldIndex++) {

                Cell cell = billRow.getCell(customFieldIndex);

                EntityField entityField;
                try {
                    entityField = entityFieldList.get(entityFieldIndex);
                } catch (Exception ex) {
                    continue;
                }

                String value = formatter.formatCellValue(cell);

                BillCustomField billCustomField = new BillCustomField();
                billCustomField.setEntityField(entityField);
                billCustomField.setValue(value);

                billCustomFieldList.add(billCustomField);
            }

            String serviceNameValue = (serviceNameCell == null) ? (null) : (formatter.formatCellValue(serviceNameCell));

            if (Utils.isNullOrEmpty(serviceNameValue)) {
                continue;
            }

            SheetLineDTO sheetLineDTO = new SheetLineDTO(
                    serviceNameValue,
                    (activityCell == null) ? (null) : (formatter.formatCellValue(activityCell)),
                    (billNumberCell == null) ? (null) : (formatter.formatCellValue(billNumberCell)),
                    (mobileNumberCell == null) ? (null) : (formatter.formatCellValue(mobileNumberCell)),
                    (emailCell == null) ? (null) : (formatter.formatCellValue(emailCell)),
                    (customerNameCell == null) ? (null) : (formatter.formatCellValue(customerNameCell)),
                    (customerAddressCell == null) ? (null) : (formatter.formatCellValue(customerAddressCell)),
                    (customerTaxNumberCell == null) ? (null) : (formatter.formatCellValue(customerTaxNumberCell)),
                    (previousBalanceCell == null) ? "0" : (formatter.formatCellValue(previousBalanceCell)),
                    (vatCell == null) ? (null) : (formatter.formatCellValue(vatCell)),
                    (amountCell == null) ? (null) : (formatter.formatCellValue(amountCell)),
                    (creationDateCell == null) ? (null) : (formatter.formatCellValue(creationDateCell)),
                    (expireDateCell == null) ? (null) : (formatter.formatCellValue(expireDateCell)),
                    (descriptionCell == null) ? (null) : (formatter.formatCellValue(descriptionCell)),
                    (customerIdCell == null) ? (null) : (formatter.formatCellValue(customerIdCell)),
                    (campaingCell == null) ? (null) : (formatter.formatCellValue(campaingCell)),
                    (discountCell == null) ? (null) : (formatter.formatCellValue(discountCell)),
                    (discountCell == null) ? (null) : (formatter.formatCellValue(isPartialCell)),
                    (discountCell == null) ? (null) : (formatter.formatCellValue(minimumPartialCell)),
                    billCustomFieldList,
                    rowIndex + 1);

            sheetDTO.addLine(sheetLineDTO);
        }

        try {
            workbook.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return sheetDTO;
    }
}


