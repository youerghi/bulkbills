package be.bslash.ibps.bills.bulkbills.repositories;

import be.bslash.ibps.bills.bulkbills.entities.Account;
import be.bslash.ibps.bills.bulkbills.entities.SadadPayment;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;

public interface SadadPaymentRepo extends CrudRepository<SadadPayment, Long> {
    @Query("select sum(pmt.paymentAmount) from SadadPayment pmt " +
            "where (pmt.account = :account) ")
    BigDecimal sumOfRemainingPaymentAccount(@Param("account") Account account);
}
