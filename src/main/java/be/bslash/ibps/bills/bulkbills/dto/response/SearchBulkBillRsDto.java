package be.bslash.ibps.bills.bulkbills.dto.response;

import be.bslash.ibps.bills.bulkbills.enums.BulkBillStatus;

import java.time.LocalDateTime;

public class SearchBulkBillRsDto {

    private Long id;
    private String reference;
    private String innerName;
    private String outerName;
    private Long numberOfBills;
    private LocalDateTime createDate;
    private String asyncRqUID;
    private Integer successCount;
    private Integer rejectCount;
    private Integer pendingCount;
    private BulkBillStatus bulkBillStatus;
    private String bulkBillStatusText;
    private String entityCode;
    private String entityName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getInnerName() {
        return innerName;
    }

    public void setInnerName(String innerName) {
        this.innerName = innerName;
    }

    public String getOuterName() {
        return outerName;
    }

    public void setOuterName(String outerName) {
        this.outerName = outerName;
    }

    public Long getNumberOfBills() {
        return numberOfBills;
    }

    public void setNumberOfBills(Long numberOfBills) {
        this.numberOfBills = numberOfBills;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public String getAsyncRqUID() {
        return asyncRqUID;
    }

    public void setAsyncRqUID(String asyncRqUID) {
        this.asyncRqUID = asyncRqUID;
    }

    public Integer getSuccessCount() {
        return successCount;
    }

    public void setSuccessCount(Integer successCount) {
        this.successCount = successCount;
    }

    public Integer getRejectCount() {
        return rejectCount;
    }

    public void setRejectCount(Integer rejectCount) {
        this.rejectCount = rejectCount;
    }

    public Integer getPendingCount() {
        return pendingCount;
    }

    public void setPendingCount(Integer pendingCount) {
        this.pendingCount = pendingCount;
    }

    public BulkBillStatus getBulkBillStatus() {
        return bulkBillStatus;
    }

    public void setBulkBillStatus(BulkBillStatus bulkBillStatus) {
        this.bulkBillStatus = bulkBillStatus;
    }

    public String getBulkBillStatusText() {
        return bulkBillStatusText;
    }

    public void setBulkBillStatusText(String bulkBillStatusText) {
        this.bulkBillStatusText = bulkBillStatusText;
    }

    public String getEntityCode() {
        return entityCode;
    }

    public void setEntityCode(String entityCode) {
        this.entityCode = entityCode;
    }

    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }
}
