package be.bslash.ibps.bills.bulkbills.repositories;

import be.bslash.ibps.bills.bulkbills.entities.Entity;
import be.bslash.ibps.bills.bulkbills.entities.EntityItem;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface EntityItemRepo extends CrudRepository<EntityItem, Long> {
    List<EntityItem> findAll();

    List<EntityItem> findEntityItemByEntity(Entity entity);

}
