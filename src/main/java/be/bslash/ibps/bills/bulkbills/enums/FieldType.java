package be.bslash.ibps.bills.bulkbills.enums;


public enum FieldType {
    INPUT,
    DROP_DOWN;

    public static FieldType getValue(String value) {
        try {
            return FieldType.valueOf(value);
        } catch (Exception ex) {
            return null;
        }
    }
}

