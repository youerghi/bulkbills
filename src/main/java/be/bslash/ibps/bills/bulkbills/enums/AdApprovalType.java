package be.bslash.ibps.bills.bulkbills.enums;


public enum AdApprovalType {
    APPROVED,
    PENDING,
    REJECTED;

    public static AdApprovalType getValue(String value) {
        try {
            return AdApprovalType.valueOf(value);
        } catch (Exception ex) {
            return null;
        }
    }
}

