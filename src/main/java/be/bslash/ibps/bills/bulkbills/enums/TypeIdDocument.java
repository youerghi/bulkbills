package be.bslash.ibps.bills.bulkbills.enums;

public enum TypeIdDocument {
    CRR,
    LIC,
    MIO,
    OTH;

    public static TypeIdDocument getValue(String value) {
        try {
            return TypeIdDocument.valueOf(value);
        } catch (Exception ex) {
            return null;
        }
    }
}

