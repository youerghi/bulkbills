package be.bslash.ibps.bills.bulkbills.repositories;

import be.bslash.ibps.bills.bulkbills.entities.Entity;
import be.bslash.ibps.bills.bulkbills.entities.EntityField;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface EntityFieldRepo extends CrudRepository<EntityField, Long> {
    List<EntityField> findAll();

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    EntityField findEntityFieldById(Long id);

    @Query("select entFild from EntityField entFild " +
            "where entFild.entity=:entity " +
            "and entFild.status ='ACTIVE' " +
            "order by entFild.id asc")
    List<EntityField> findAllActiveEntityFieldByEntity(@Param("entity") Entity entity);

    @Query("select entFild from EntityField entFild " +
            "where entFild.entity=:entity " +
            "and entFild.status ='ACTIVE' and entFild.id=:id")
    EntityField findEntityFieldByIdAndEntity(@Param("id") Long id, @Param("entity") Entity entity);

    @Query("select count(entFild.id) from EntityField entFild " +
            "where entFild.entity=:entity " +
            "and entFild.status ='ACTIVE' " +
            "and entFild.isRequired = 'YES' ")
    Integer countRequiredEntityFieldByEntity(@Param("entity") Entity entity);
}
