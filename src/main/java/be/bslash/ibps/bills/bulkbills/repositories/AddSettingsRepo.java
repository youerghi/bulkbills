package be.bslash.ibps.bills.bulkbills.repositories;


import be.bslash.ibps.bills.bulkbills.entities.AdSettings;
import be.bslash.ibps.bills.bulkbills.entities.Entity;
import org.springframework.data.repository.CrudRepository;

public interface AddSettingsRepo extends CrudRepository<AdSettings, Long> {

    AdSettings findByEntity(Entity entity);

}
