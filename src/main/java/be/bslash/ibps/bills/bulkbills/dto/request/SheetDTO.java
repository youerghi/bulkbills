package be.bslash.ibps.bills.bulkbills.dto.request;

import java.util.ArrayList;
import java.util.List;

public class SheetDTO {
    private String fileReferenceValue;
    private String outerName;
    private String bulkTypeCell;
    private List<SheetLineDTO> lines;

    public void addLine(SheetLineDTO sheetLineDTO) {
        if (lines == null) {
            lines = new ArrayList<>();
        }
        lines.add(sheetLineDTO);
    }

    public String getFileReferenceValue() {
        return fileReferenceValue;
    }

    public void setFileReferenceValue(String fileReferenceValue) {
        this.fileReferenceValue = fileReferenceValue;
    }

    public String getOuterName() {
        return outerName;
    }

    public void setOuterName(String outerName) {
        this.outerName = outerName;
    }

    public List<SheetLineDTO> getLines() {
        return lines;
    }

    public void setLines(List<SheetLineDTO> lines) {
        this.lines = lines;
    }

    public String getBulkTypeCell() {
        return bulkTypeCell;
    }

    public void setBulkTypeCell(String bulkTypeCell) {
        this.bulkTypeCell = bulkTypeCell;
    }
}
