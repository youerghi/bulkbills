package be.bslash.ibps.bills.bulkbills.repositories;

import be.bslash.ibps.bills.bulkbills.entities.BillTransaction;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;

public interface BillTransactionRepo extends CrudRepository<BillTransaction, Long> {
    @Query(value = "select TOP 1 balance_amount " +
            "from bill_transaction " +
            "where account_id= :account_id " +
            "order by id desc",
            nativeQuery = true)
    BigDecimal getFinalBalanceForAccount(@Param("account_id") Long account_id);

}
