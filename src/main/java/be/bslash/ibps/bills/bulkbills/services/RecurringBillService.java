package be.bslash.ibps.bills.bulkbills.services;

import be.bslash.ibps.bills.bulkbills.dto.projection.AccountProjection;
import be.bslash.ibps.bills.bulkbills.dto.projection.EntityActivityProjection;
import be.bslash.ibps.bills.bulkbills.dto.request.BillCustomFieldRqDto;
import be.bslash.ibps.bills.bulkbills.dto.request.BillItemDetailedRqDto;
import be.bslash.ibps.bills.bulkbills.dto.request.RecurringBillApiMakeRqDto;
import be.bslash.ibps.bills.bulkbills.entities.Account;
import be.bslash.ibps.bills.bulkbills.entities.Bill;
import be.bslash.ibps.bills.bulkbills.entities.BillCustomField;
import be.bslash.ibps.bills.bulkbills.entities.BillEmail;
import be.bslash.ibps.bills.bulkbills.entities.BillItem;
import be.bslash.ibps.bills.bulkbills.entities.BillMobileNumber;
import be.bslash.ibps.bills.bulkbills.entities.BillTimeline;
import be.bslash.ibps.bills.bulkbills.entities.BillTransaction;
import be.bslash.ibps.bills.bulkbills.entities.EntityField;
import be.bslash.ibps.bills.bulkbills.entities.EntityUser;
import be.bslash.ibps.bills.bulkbills.enums.AccountCategory;
import be.bslash.ibps.bills.bulkbills.enums.AccountType;
import be.bslash.ibps.bills.bulkbills.enums.BillCategory;
import be.bslash.ibps.bills.bulkbills.enums.BillSource;
import be.bslash.ibps.bills.bulkbills.enums.BillStatus;
import be.bslash.ibps.bills.bulkbills.enums.BillType;
import be.bslash.ibps.bills.bulkbills.enums.BooleanFlag;
import be.bslash.ibps.bills.bulkbills.enums.EntityUserType;
import be.bslash.ibps.bills.bulkbills.enums.IdType;
import be.bslash.ibps.bills.bulkbills.enums.Status;
import be.bslash.ibps.bills.bulkbills.exceptions.HttpServiceException;
import be.bslash.ibps.bills.bulkbills.repositories.AccountRepo;
import be.bslash.ibps.bills.bulkbills.repositories.BillCustomFieldRepo;
import be.bslash.ibps.bills.bulkbills.repositories.BillEmailRepo;
import be.bslash.ibps.bills.bulkbills.repositories.BillItemRepo;
import be.bslash.ibps.bills.bulkbills.repositories.BillMobileNumberRepo;
import be.bslash.ibps.bills.bulkbills.repositories.BillRepo;
import be.bslash.ibps.bills.bulkbills.repositories.BillTimelineRepo;
import be.bslash.ibps.bills.bulkbills.repositories.BillTransactionRepo;
import be.bslash.ibps.bills.bulkbills.repositories.CityRepo;
import be.bslash.ibps.bills.bulkbills.repositories.DistrictRepo;
import be.bslash.ibps.bills.bulkbills.repositories.EntityActivityRepo;
import be.bslash.ibps.bills.bulkbills.repositories.EntityFieldRepo;
import be.bslash.ibps.bills.bulkbills.repositories.EntityRepo;
import be.bslash.ibps.bills.bulkbills.repositories.EntityVatRepo;
import be.bslash.ibps.bills.bulkbills.repositories.SadadPaymentRepo;
import be.bslash.ibps.bills.bulkbills.utils.BillerUtils;
import be.bslash.ibps.bills.bulkbills.utils.Utils;
import be.bslash.ibps.bills.bulkbills.ws.WebServiceHelper;
import be.bslash.ibps.bills.bulkbills.ws.WebServiceResponse;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Service
@Transactional
public class RecurringBillService {

    @Autowired
    private WebServiceHelper webServiceHelper;
    @Autowired
    private AccountRepo accountRepo;
    @Autowired
    private BillRepo billRepo;
    @Autowired
    private EntityActivityRepo entityActivityRepo;
    @Autowired
    private CityRepo cityRepo;
    @Autowired
    private DistrictRepo districtRepo;
    @Autowired
    private EntityRepo entityRepo;
    @Autowired
    private BillTimelineRepo billTimelineRepo;
    @Autowired
    private EntityVatRepo entityVatRepo;
    @Autowired
    private BillItemRepo billItemRepo;
    @Autowired
    private EntityFieldRepo entityFieldRepo;
    @Autowired
    private BillCustomFieldRepo billCustomFieldRepo;
    @Autowired
    private BillEmailRepo billEmailRepo;
    @Autowired
    private BillMobileNumberRepo billMobileNumberRepo;
    @Autowired
    private BillTransactionRepo billTransactionRepo;
    @Autowired
    private SadadPaymentRepo sadadPaymentRepo;

    public static final Logger LOGGER = LogManager.getLogger(RecurringBillService.class);

    @Transactional
    public Set<Bill> saveRecurringBillApi(List<RecurringBillApiMakeRqDto> recurringBillApiMakeRqDtoList, EntityUser entityUser, BillSource billSource, Locale locale) {

        verifyUser(entityUser, locale);

        Map<Bill, RecurringBillApiMakeRqDto> bills = new ConcurrentHashMap<>();

        System.out.println("Creating bill...");
        recurringBillApiMakeRqDtoList.parallelStream().forEach(recurringBillApiMakeRqDto -> {
            Bill bill = createBill(recurringBillApiMakeRqDto, entityUser, billSource, locale);
            bills.put(bill, recurringBillApiMakeRqDto);
        });
        billRepo.saveAllAndFlush(bills.keySet());
        System.out.println("Done creating bill...");

        System.out.println("Creating billtimeline...");

        List<BillTimeline> makedBilltimelines = bills.keySet()
                .parallelStream()
                .map(bill -> createBillTimeline(BillStatus.MAKED, bill, entityUser))
                .collect(Collectors.toList());
        billTimelineRepo.saveAll(makedBilltimelines);
        System.out.println("Done Creating billtimeline...");

        System.out.println("Creating billitems...");
        List<BillItem> billItems = bills.entrySet()
                .parallelStream().map(entry -> createBillItems(entityUser, locale, entry))
                .flatMap(Collection::parallelStream)
                .collect(Collectors.toList());
        billItemRepo.saveAll(billItems);
        System.out.println("Done Creating billitems...");


        System.out.println("Creating billcustomfields...");
        List<BillCustomField> billCustomFields = bills.entrySet()
                .parallelStream()
                .map(entry -> createBillCustomFields(entityUser, locale, entry))
                .flatMap(Collection::parallelStream)
                .collect(Collectors.toList());
        billCustomFieldRepo.saveAll(billCustomFields);
        System.out.println("Done Creating billcustomfields...");


        System.out.println("Creating billEmails...");
        List<BillEmail> billEmails = bills.entrySet()
                .parallelStream().map(entry -> createBillEmails(locale, entry))
                .flatMap(Collection::parallelStream)
                .collect(Collectors.toList());
        billEmailRepo.saveAll(billEmails);
        System.out.println("Done Creating billEmails...");

        System.out.println("Creating mobileNumber...");
        List<BillMobileNumber> billMobileNumbers = bills.entrySet()
                .parallelStream()
                .map(entry -> createBillMobileNumbers(locale, entry))
                .flatMap(Collection::parallelStream)
                .collect(Collectors.toList());
        billMobileNumberRepo.saveAll(billMobileNumbers);
        System.out.println("Done Creating billEmails...");

        System.out.println("Creating billtransactions...");
        List<BillTransaction> billTransactions = bills.keySet()
                .parallelStream()
                //.filter(bill -> bill.getDueDate() != null && bill.getDueDate().isBefore(LocalDateTime.now()))
                .map(this::createBillTransaction)
                .collect(Collectors.toList());
        billTransactionRepo.saveAll(billTransactions);
        System.out.println("Done Creating billtransactions...");

        System.out.println("executing recurringBillJob...");

        recurringBillsJob(bills.keySet());

        System.out.println("Done recurringBillJob...");

        System.out.println("Creating billTinelines...");
        List<BillTimeline> billTimelines = bills.keySet()
                .parallelStream()
                .map(bill -> updateBillSadadTimeline(bill, entityUser))
                .flatMap(Collection::parallelStream)
                .collect(Collectors.toList());
        billTimelineRepo.saveAll(billTimelines);
        System.out.println("Done Creating billEmails...");

        return bills.keySet();
    }

    @Transactional(readOnly = true)
    public BillTransaction createBillTransaction(Bill bill) {
        BillTransaction billTransaction = new BillTransaction();
        billTransaction.setBill(bill);
        billTransaction.setPreviousAmount(bill.getAccountBalance());
        billTransaction.setAccount(accountRepo.getById(bill.getAccount().getId()));
        billTransaction.setAmount(bill.getTotalAmount());
        billTransaction.setDate(bill.getDueDate());

        if (billTransactionRepo.getFinalBalanceForAccount(bill.getAccount().getId()) == null) {
            billTransaction.setBalanceAmount(bill.getTotalAmount().add(bill.getAccountBalance()));
        } else {
            billTransaction.setBalanceAmount(bill.getTotalAmount().add(billTransactionRepo.getFinalBalanceForAccount(bill.getAccount().getId())));
        }
        billTransaction.setName("Issued Invoice");
        billTransaction.setDocumentNumber(bill.getBillNumber());

        return billTransaction;
    }

    private List<BillMobileNumber> createBillMobileNumbers(Locale locale, Map.Entry<Bill, RecurringBillApiMakeRqDto> entry) {
        Bill recurringBill = entry.getKey();
        RecurringBillApiMakeRqDto recurringBillApiMakeRqDto = entry.getValue();

        List<BillMobileNumber> billMobileNumberList = new ArrayList<>();

        if (!Utils.isNullOrEmpty(recurringBillApiMakeRqDto.getCustomerMobileList())) {
            if (recurringBillApiMakeRqDto.getCustomerMobileList().size() > 5) {
                throw new HttpServiceException(HttpStatus.BAD_REQUEST, "mobile list should not more than 5 times", locale);
            }

            Set<String> set = new HashSet<>(recurringBillApiMakeRqDto.getCustomerMobileList());
            recurringBillApiMakeRqDto.getCustomerMobileList().clear();
            recurringBillApiMakeRqDto.getCustomerMobileList().addAll(set);

            for (String mobile : recurringBillApiMakeRqDto.getCustomerMobileList()) {

                if (mobile == null) {
                    continue;
                }

                String formattedMobile = Utils.formatMobileNumber(mobile);

                if (formattedMobile == null || formattedMobile.equals(recurringBill.getCustomerMobileNumber())) {
                    continue;
                }

                BillMobileNumber billMobileNumber = new BillMobileNumber();
                billMobileNumber.setBill(recurringBill);
                billMobileNumber.setMobileNumber(Utils.formatMobileNumber(mobile));
                billMobileNumberList.add(billMobileNumber);
            }
        }

        return billMobileNumberList;
    }

    private List<BillEmail> createBillEmails(Locale locale, Map.Entry<Bill, RecurringBillApiMakeRqDto> entry) {
        Bill recurringBill = entry.getKey();
        RecurringBillApiMakeRqDto recurringBillApiMakeRqDto = entry.getValue();
        List<BillEmail> billEmailList = new ArrayList<>();

        if (!Utils.isNullOrEmpty(recurringBillApiMakeRqDto.getCustomerEmailList())) {

            if (recurringBillApiMakeRqDto.getCustomerEmailList().size() > 5) {
                throw new HttpServiceException(HttpStatus.BAD_REQUEST, "email list should not more than 5 times", locale);
            }

            Set<String> set = new HashSet<>(recurringBillApiMakeRqDto.getCustomerEmailList());
            recurringBillApiMakeRqDto.getCustomerEmailList().clear();
            recurringBillApiMakeRqDto.getCustomerEmailList().addAll(set);

            for (String email : recurringBillApiMakeRqDto.getCustomerEmailList()) {

                if (email == null) {
                    continue;
                }

                if (email.equals(recurringBill.getCustomerEmailAddress())) {
                    continue;
                }

                BillEmail billEmail = getBillEmail(recurringBill, email);
                billEmailList.add(billEmail);
            }

        }

        return billEmailList;
    }

    @Transactional(readOnly = true)
    public List<BillCustomField> createBillCustomFields(EntityUser entityUser, Locale locale, Map.Entry<Bill, RecurringBillApiMakeRqDto> entry) {
        Bill recurringBill = entry.getKey();
        RecurringBillApiMakeRqDto recurringBillApiMakeRqDto = entry.getValue();

        List<BillCustomField> recurringBillCustomFields = new ArrayList<>();

        if (recurringBillApiMakeRqDto.getCustomFieldList() != null && !recurringBillApiMakeRqDto.getCustomFieldList().isEmpty()) {
            for (BillCustomFieldRqDto billCustomFieldRqDto : recurringBillApiMakeRqDto.getCustomFieldList()) {

                EntityField entityField = entityFieldRepo.findEntityFieldById(billCustomFieldRqDto.getFieldId());

                if (entityField == null) {
                    continue;
                }

                if (entityField.getStatus().equals(Status.DELETED)) {
                    continue;
                }

                if (entityField.getStatus().equals(Status.INACTIVE)) {
                    throw new HttpServiceException(HttpStatus.BAD_REQUEST, "custom field inactive", locale);
                }

                if (!entityField.getEntity().getId().equals(entityUser.getEntity().getId())) {
                    throw new HttpServiceException(HttpStatus.BAD_REQUEST, "custom field not related for current biller", locale);
                }

                if (!Utils.isValidCustomField(entityField, billCustomFieldRqDto.getFieldValue())) {
                    throw new HttpServiceException(HttpStatus.BAD_REQUEST, "custom field value is not valid", locale);
                }

                if (entityField.getIsRequired().equals(BooleanFlag.YES) && Utils.isNullOrEmpty(billCustomFieldRqDto.getFieldValue())) {
                    throw new HttpServiceException(HttpStatus.BAD_REQUEST, "custom field required value is missing", locale);
                }

                BillCustomField billCustomField = new BillCustomField();
                billCustomField.setBill(recurringBill);
                billCustomField.setEntityField(entityField);
                billCustomField.setValue(billCustomFieldRqDto.getFieldValue());

                recurringBillCustomFields.add(billCustomField);
            }
        }


        return recurringBillCustomFields;
    }

    private List<BillItem> createBillItems(EntityUser entityUser, Locale locale, Map.Entry<Bill, RecurringBillApiMakeRqDto> entry) {
        Bill recurringBill = entry.getKey();
        RecurringBillApiMakeRqDto recurringBillApiMakeRqDto = entry.getValue();

        Set<BillItemDetailedRqDto> billItemDetailedRqDtoSet = new HashSet<>(recurringBillApiMakeRqDto.getBillItemList());

        if (billItemDetailedRqDtoSet.size() < recurringBillApiMakeRqDto.getBillItemList().size()) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "Duplicate item name is not allowed", locale);
        }

        List<BillItem> recurringBillItems = new ArrayList<>();
        int line = 1;
        for (BillItemDetailedRqDto billItemDetailedRqDto : recurringBillApiMakeRqDto.getBillItemList()) {
            BillerUtils.validateEntityVat(entityUser, locale, billItemDetailedRqDto.getVat(), entityVatRepo);

            BillItem billItem = (BillItem) billItemDetailedRqDto.toEntity(locale);
            billItem.setBill(recurringBill);
            billItem.setNumber(line++);

            billItem.setDiscount(billItem.getDiscount());
            billItem.setVat(billItem.getVat());

            billItem.setUnitPrice(billItemDetailedRqDto.getUnitPrice());
            billItem.setQuantity(billItemDetailedRqDto.getQuantity());

            billItem.setTotalPrice(billItem.getUnitPrice().multiply(billItem.getQuantity()));

            recurringBillItems.add(billItem);
        }
        return recurringBillItems;
    }

    private void verifyAccount(AccountProjection account, Locale locale) {
        if (account == null || account.getStatus().equals(Status.DELETED)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "account not exist", locale);
        }

        if (account.getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "account inactive", locale);
        }

        if (account.getAccountCategory().equals(AccountCategory.NORMAL)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "normal account cannot create recurring bill", locale);
        }
    }

    public void recurringBillsJob(Set<Bill> bills) {
        List<Bill> collectedList = bills.stream().map(recurringBill -> {
            System.out.println(recurringBill.getCycleNumber());
            LocalDateTime dueDate = LocalDateTime.now();
            Account account = recurringBill.getAccount();
            BigDecimal totalRemainingBalance = billRepo.sumOfRemainingDueBillsTest(dueDate, account);

            Bill bill = new Bill();
            bill.setBillCategory(recurringBill.getBillCategory());
            bill.setSadadNumber(account.getAccountNumber());
            bill.setCycleNumber(recurringBill.getCycleNumber());
            bill.setDueDate(dueDate);
            bill.setAccount(account);
            if (totalRemainingBalance != null) {
                BigDecimal totalPaymentBalance = sadadPaymentRepo.sumOfRemainingPaymentAccount(account);
                if (totalPaymentBalance == null) {
                    totalPaymentBalance = BigDecimal.ZERO;
                }
                bill.setTotalRunningAmount(totalRemainingBalance.add(totalPaymentBalance));
                bill.setMiniPartialAmount(account.getMiniPartialAmount() != null ? account.getMiniPartialAmount() : totalRemainingBalance.add(totalPaymentBalance));
            } else {
                totalRemainingBalance = BigDecimal.ZERO;

                account.setBalance(totalRemainingBalance);
                bill.setTotalRunningAmount(totalRemainingBalance);
                bill.setMiniPartialAmount(account.getMiniPartialAmount() != null ? account.getMiniPartialAmount() : totalRemainingBalance);
            }

            accountRepo.updateBalance(totalRemainingBalance, account.getId());

            if (recurringBill.getDueDate().toLocalDate().equals(LocalDate.now())) {
                return null;
            }

            return bill;
        }).filter(Objects::nonNull).collect(Collectors.toList());

        WebServiceResponse webServiceResponse = webServiceHelper.uploadBulkRecurring(collectedList);
        if (webServiceResponse.getStatus().equals(HttpStatus.INTERNAL_SERVER_ERROR.value())) {
            throw new HttpServiceException(HttpStatus.INTERNAL_SERVER_ERROR, "INTERNAL_SERVER_ERROR");
        }
        System.out.println(webServiceResponse.getBody());
    }

    private List<BillTimeline> updateBillSadadTimeline(Bill bill, EntityUser entityUser) {
        bill.setBillStatus(BillStatus.CHECKED);

        BillTimeline checkedBillTimeline = createBillTimeline(BillStatus.CHECKED, bill, entityUser);
        BillTimeline uploadedToSadadBillTimeline = createBillTimeline(BillStatus.UPLOADED_TO_SADAD, bill, entityUser);
        BillTimeline approvedBySadadBillTimeline = createBillTimeline(BillStatus.APPROVED_BY_SADAD, bill, entityUser);

        bill.setBillStatus(BillStatus.APPROVED_BY_SADAD);
        bill.setApproveDateBySadad(approvedBySadadBillTimeline.getActionDate());

        return List.of(checkedBillTimeline, uploadedToSadadBillTimeline, approvedBySadadBillTimeline);
    }

    private void verifyUser(EntityUser entityUser, Locale locale) {
        if (!entityUser.getEntityUserType().equals(EntityUserType.SUPERVISOR) && entityUser.getEntityUserType().equals(EntityUserType.OFFICER)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "cannot_access_for_officer", locale);
        }

        if (entityUser.getStatus().equals(Status.DELETED)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_deleted", locale);
        }

        if (entityUser.getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_inactive", locale);
        }
    }

    public void setSystemGeneratedBillNumber(final RecurringBillApiMakeRqDto recurringBillApiMakeRqDto, final EntityUser entityUser, final Bill recurringBill) {
        String prefixBillNumber = Utils.generateBillNumber(entityUser.getEntity());
        Long sequence = entityRepo.increaseBillNumberCounterAndGet(entityUser.getEntity().getId());

        if (StringUtils.isEmpty(recurringBillApiMakeRqDto.getBillNumber())) {
            recurringBill.setBillNumber(prefixBillNumber + sequence);
        } else {
            recurringBill.setBillNumber(recurringBillApiMakeRqDto.getBillNumber());
        }
    }

    private BillEmail getBillEmail(final Bill currentBill, final String email) {
        BillEmail billEmail = new BillEmail();
        billEmail.setBill(currentBill);
        billEmail.setEmail(email);
        return billEmail;
    }

    private BillTimeline createBillTimeline(BillStatus maked, Bill recurringBill, EntityUser entityUser) {
        BillTimeline billTimeline = new BillTimeline();
        billTimeline.setBillStatus(maked);
        billTimeline.setBill(recurringBill);
        billTimeline.setActionDate(LocalDateTime.now());
        billTimeline.setEntityUser(entityUser);
        billTimeline.setUserReference(entityUser.getUsername());
        return billTimeline;
    }

    @Transactional
    public Bill createBill(RecurringBillApiMakeRqDto recurringBillApiMakeRqDto, EntityUser entityUser, BillSource billSource, Locale locale) {
        AccountProjection account = accountRepo.findAccount(recurringBillApiMakeRqDto.getAccountNumber());

        verifyAccount(account, locale);

        if (!Utils.isNullOrEmpty(recurringBillApiMakeRqDto.getBillNumber())) {
            if (billRepo.checkBillExists(recurringBillApiMakeRqDto.getBillNumber(), entityUser.getEntity())) {
                throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_already_exist", locale);
            }
        }

        BigDecimal subAmountAfterDiscount = Utils.calculateSubAmountDiscount(recurringBillApiMakeRqDto.getSubAmount(), recurringBillApiMakeRqDto.getDiscount(), recurringBillApiMakeRqDto.getDiscountType());

        BigDecimal totalAmount = Utils.calculateTotalAmountFromVatAndPreviousBalance(subAmountAfterDiscount, recurringBillApiMakeRqDto.getVat(), BigDecimal.ZERO);

        BigDecimal miniPartialAmount = recurringBillApiMakeRqDto.getMiniPartialAmount();
        if (miniPartialAmount == null) {
            miniPartialAmount = BigDecimal.ZERO;
        }

        if (totalAmount.doubleValue() < miniPartialAmount.doubleValue()) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "Total amount should not less than minimum partial amount", locale);
        }

        EntityActivityProjection entityActivity = entityActivityRepo.findEntityActivityById(recurringBillApiMakeRqDto.getEntityActivityId());

        if (entityActivity == null) {
            entityActivity = entityActivityRepo.findEntityActivity(recurringBillApiMakeRqDto.getEntityActivityCode(), entityUser.getEntity().getId());
        }

        if (entityActivity == null || !entityActivity.getEntityId().equals(entityUser.getEntity().getId())) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "Entity activity is not related to user", locale);
        }

        if (!entityActivity.getStatus().equals(Status.ACTIVE)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "invalid_entity_activity_id", locale);
        }

        if (entityActivity.getTheLowestAmountPerUploadedBill() != null) {
            if (totalAmount.compareTo(entityActivity.getTheLowestAmountPerUploadedBill()) == -1) {
                throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_amount_min", locale);
            }
        }

        if (entityActivity.getTheHighestAmountPerUploadedBill() != null) {
            //checking bill limit per bill
            if (totalAmount.compareTo(entityActivity.getTheHighestAmountPerUploadedBill()) == 1) {
                throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_amount_limit_per_bill", locale);
            }
        }
        if (recurringBillApiMakeRqDto.getIsPartialAllowed() != null && recurringBillApiMakeRqDto.getIsPartialAllowed().equals("YES")) {
            if (miniPartialAmount.compareTo(entityActivity.getTheLowestAmountPerUploadedBill()) < 0) {
                throw new HttpServiceException(HttpStatus.BAD_REQUEST, "Minimum partial amount should not be less than minimum amount for the bill", locale);
            }
        }
        if (entityActivity.getMonthlyHighestValueOfExpectedUploadedBills() != null) {
            //checking bill limit per month
            LocalDateTime startDayOfMonth = Utils.getStartDayOfCurrentMonth();
            LocalDateTime currentDay = Utils.getCurrentDateTime();

            BigDecimal totalAmountForCurrentMonth = billRepo.volumeByApprovedDate(startDayOfMonth, currentDay, entityActivity.getId(), entityUser.getEntity());
            if (totalAmountForCurrentMonth == null) {
                totalAmountForCurrentMonth = new BigDecimal("0");
            }

            if ((totalAmount.add(totalAmountForCurrentMonth)).compareTo(entityActivity.getMonthlyHighestValueOfExpectedUploadedBills()) == 1) {
                throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_amount_limit_per_month", locale);
            }
        }

        String customerMobileNumber;
        String customerEmail;
        String customerFullName;
        String customerIdNumber;
        String customerTaxNumber;
        String customerIdType;

        if (AccountType.getValue(account.getType()) == AccountType.IndividualAccount) {

            customerMobileNumber = recurringBillApiMakeRqDto.getCustomerMobileNumber() != null ? recurringBillApiMakeRqDto.getCustomerMobileNumber() : account.getPhoneNumber();
            customerEmail = recurringBillApiMakeRqDto.getCustomerEmailAddress() != null ? recurringBillApiMakeRqDto.getCustomerEmailAddress() : account.getEmail();
            customerFullName = recurringBillApiMakeRqDto.getCustomerFullName() != null ? recurringBillApiMakeRqDto.getCustomerFullName() : account.getFirstName() + " " + account.getLastName();
            customerIdNumber = recurringBillApiMakeRqDto.getCustomerIdNumber() != null ? recurringBillApiMakeRqDto.getCustomerIdNumber() : account.getIdNumber();
            customerTaxNumber = recurringBillApiMakeRqDto.getCustomerTaxNumber() != null ? recurringBillApiMakeRqDto.getCustomerTaxNumber() : account.getVatNumber();
            customerIdType = recurringBillApiMakeRqDto.getCustomerIdType() != null ? recurringBillApiMakeRqDto.getCustomerIdType() : account.getIdType() != null ? account.getIdType().name() : null;
        } else {

            customerMobileNumber = recurringBillApiMakeRqDto.getCustomerMobileNumber() != null ? recurringBillApiMakeRqDto.getCustomerMobileNumber() : account.getContactPersonNumber();
            customerEmail = recurringBillApiMakeRqDto.getCustomerEmailAddress() != null ? recurringBillApiMakeRqDto.getCustomerEmailAddress() : account.getContactPersonEmail();
            customerFullName = recurringBillApiMakeRqDto.getCustomerFullName() != null ? recurringBillApiMakeRqDto.getCustomerFullName() : account.getCompanyName();
            customerIdNumber = recurringBillApiMakeRqDto.getCustomerIdNumber() != null ? recurringBillApiMakeRqDto.getCustomerIdNumber() : account.getIdNumber();
            customerTaxNumber = recurringBillApiMakeRqDto.getCustomerTaxNumber() != null ? recurringBillApiMakeRqDto.getCustomerTaxNumber() : account.getVatNumber();
            customerIdType = recurringBillApiMakeRqDto.getCustomerIdType() != null ? recurringBillApiMakeRqDto.getCustomerIdType() : account.getTypeIdDocument() != null ? account.getTypeIdDocument().name() : null;
        }

        Bill recurringBill = new Bill();
        recurringBill.setCustomerMobileNumber(Utils.formatMobileNumber(customerMobileNumber));
        recurringBill.setCustomerEmailAddress(customerEmail);
        recurringBill.setCustomerFullName(customerFullName);
        recurringBill.setDueDate(Utils.parseDateFromString(recurringBillApiMakeRqDto.getDueDate(), "yyyy-MM-dd"));
        recurringBill.setIssueDate(Utils.parseDateFromString(recurringBillApiMakeRqDto.getDueDate(), "yyyy-MM-dd"));
        recurringBill.setCustomerIdNumber(customerIdNumber);
        recurringBill.setSubAmount(recurringBillApiMakeRqDto.getSubAmount());
        recurringBill.setDiscount(null);
        recurringBill.setCustomerTaxNumber(customerTaxNumber);
        recurringBill.setCustomerPreviousBalance(BigDecimal.ZERO);
        recurringBill.setCustomerIdType(IdType.getValue(customerIdType));
        recurringBill.setIsPartialAllowed(BooleanFlag.getValue(recurringBillApiMakeRqDto.getIsPartialAllowed()));

        if (recurringBillApiMakeRqDto.getIsPartialAllowed() != null && recurringBillApiMakeRqDto.getIsPartialAllowed().equalsIgnoreCase("YES")) {
            recurringBill.setMiniPartialAmount(recurringBillApiMakeRqDto.getMiniPartialAmount());
        } else {
            recurringBill.setMiniPartialAmount(account.getBalance().add(totalAmount));
        }
        recurringBill.setTotalAmount(totalAmount);
        recurringBill.setBillType(BillType.RECURRING_BILL);
        recurringBill.setAccount(accountRepo.getById(account.getId()));
        recurringBill.setBillSource(billSource);
        recurringBill.setBillStatus(BillStatus.MAKED);
        recurringBill.setEntity(entityUser.getEntity());
        recurringBill.setEntityUser(entityUser);
        recurringBill.setCreateDate(LocalDateTime.now());
        recurringBill.setEntityActivity(entityActivityRepo.getById(entityActivity.getId()));
        recurringBill.setCampaignId(recurringBillApiMakeRqDto.getCampaignId());

        recurringBill.setBillCategory(BillCategory.RECURRING_ACCOUNT_PAR);
        recurringBill.setMaxPartialAmount(recurringBillApiMakeRqDto.getMaxPartialAmount());
        recurringBill.setPaymentNumber(0);
        recurringBill.setSettlementNumber(0);

        recurringBill.setAdditionalNumber(recurringBillApiMakeRqDto.getAdditionalNumber());
        recurringBill.setBuildingNumber(recurringBillApiMakeRqDto.getBuildingNumber());
        recurringBill.setCity(cityRepo.findCityByCode(recurringBillApiMakeRqDto.getCityCode()));
        recurringBill.setDistrict(districtRepo.findDistrictByCode(recurringBillApiMakeRqDto.getDistrictCode()));
        recurringBill.setPostalCode(recurringBillApiMakeRqDto.getPostalCode());
        recurringBill.setStreet(recurringBillApiMakeRqDto.getStreetName());
        recurringBill.setReferenceNumber(recurringBillApiMakeRqDto.getReferenceNumber());

        recurringBill.setLogo(entityUser.getEntity().getLogo());
        recurringBill.setStamp(entityUser.getEntity().getStamp());

        Integer cycleNumber = accountRepo.increaseCycleNumberCounterAndGet(account.getId());

        recurringBill.setCycleNumber(cycleNumber);
        recurringBill.setSadadNumber(account.getAccountNumber());
        recurringBill.setRemainAmount(totalAmount);
        recurringBill.setTotalRunningAmount(totalAmount);
        recurringBill.setAccountBalance(account.getBalance());

        setSystemGeneratedBillNumber(recurringBillApiMakeRqDto, entityUser, recurringBill);

        recurringBill.setCustomerAddress(recurringBillApiMakeRqDto.getCustomerAddress());

        if (recurringBillApiMakeRqDto.getIsPartialAllowed() != null && recurringBillApiMakeRqDto.getIsPartialAllowed().equalsIgnoreCase("YES")) {
            accountRepo.updateMiniPartialAmount(account.getId(), recurringBillApiMakeRqDto.getMiniPartialAmount());
        } else {
            accountRepo.updateMiniPartialAmount(account.getId(), account.getBalance().add(recurringBill.getTotalAmount()));
        }

        return recurringBill;
    }
}
