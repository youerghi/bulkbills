package be.bslash.ibps.bills.bulkbills.repositories;

import be.bslash.ibps.bills.bulkbills.entities.Bill;
import be.bslash.ibps.bills.bulkbills.entities.BillEmail;
import be.bslash.ibps.bills.bulkbills.entities.BulkBill;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface BillEmailRepo extends CrudRepository<BillEmail, Long> {

    List<BillEmail> findAll();


    @Query("select item from BillEmail item " +
            "where item.bill=:bill " +
            "order by item.id asc ")
    List<BillEmail> findAllBillEmail(@Param("bill") Bill bill);

    @Query("select item from BillEmail item " +
            "where item.bill.bulkBill=:bulkBill " +
            "order by item.id asc ")
    List<BillEmail> findAllBillEmailByBulk(@Param("bulkBill") BulkBill bulkBill);


}
