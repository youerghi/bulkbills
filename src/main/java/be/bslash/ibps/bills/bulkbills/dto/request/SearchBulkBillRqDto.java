package be.bslash.ibps.bills.bulkbills.dto.request;

import java.util.List;

public class SearchBulkBillRqDto {

    private String entityCode;
    private String activityCode;
    private String reference;
    private String outerName;
    private String innerName;
    private String issueDateFrom;
    private String issueDateTo;
    private List<String> bulkStatus;
    private Integer page;
    private Integer size;

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getOuterName() {
        return outerName;
    }

    public void setOuterName(String outerName) {
        this.outerName = outerName;
    }

    public String getInnerName() {
        return innerName;
    }

    public void setInnerName(String innerName) {
        this.innerName = innerName;
    }

    public String getIssueDateFrom() {
        return issueDateFrom;
    }

    public void setIssueDateFrom(String issueDateFrom) {
        this.issueDateFrom = issueDateFrom;
    }

    public String getIssueDateTo() {
        return issueDateTo;
    }

    public void setIssueDateTo(String issueDateTo) {
        this.issueDateTo = issueDateTo;
    }

    public List<String> getBulkStatus() {
        return bulkStatus;
    }

    public void setBulkStatus(List<String> bulkStatus) {
        this.bulkStatus = bulkStatus;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public String getEntityCode() {
        return entityCode;
    }

    public void setEntityCode(String entityCode) {
        this.entityCode = entityCode;
    }

    public String getActivityCode() {
        return activityCode;
    }

    public void setActivityCode(String activityCode) {
        this.activityCode = activityCode;
    }
}
