package be.bslash.ibps.bills.bulkbills.repositories;

import be.bslash.ibps.bills.bulkbills.entities.Entity;
import be.bslash.ibps.bills.bulkbills.entities.EntityUser;
import be.bslash.ibps.bills.bulkbills.enums.Status;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface EntityUserRepo extends CrudRepository<EntityUser, Long> {
    List<EntityUser> findAll();

    @Query("select comUsr from EntityUser comUsr " +
            "where comUsr.entity=:entity " +
            "and comUsr.status <>'DELETED' " +
            "order by comUsr.id asc ")
    List<EntityUser> findEntityUserByEntity(@Param("entity") Entity entity);

    @Query("select comUsr from EntityUser comUsr " +
            "where comUsr.username=:username " +
            "and comUsr.status <>'DELETED'")
    EntityUser findEntityUserByUsername(@Param("username") String username);

    @Query("select comUsr from EntityUser comUsr " +
            "where comUsr.username=:username " +
            "and comUsr.status='DELETED'")
    List<EntityUser> findDeletedEntityUserByUsername(@Param("username") String username);

    @Modifying
    @Query("update EntityUser usr set usr.status=:status " +
            "where usr.id=:id")
    void updateStatus(
            @Param("id") Long id,
            @Param("status") Status status);

    @Modifying
    @Query("update EntityUser usr set usr.failedLoginCounter=:failedLoginCounter " +
            "where usr.id=:id")
    void updateCounter(
            @Param("id") Long id,
            @Param("failedLoginCounter") Integer failedLoginCounter);

    @Modifying
    @Query("update EntityUser usr set usr.isPasswordReset=:isPasswordReset " +
            "where usr.id=:id")
    void updatePasswordReset(
            @Param("id") Long id,
            @Param("isPasswordReset") Integer isPasswordReset);

    @Modifying
    @Query("update EntityUser usr set usr.fcmToken=:fcmToken " +
            "where usr.id=:id")
    void updateFcmToken(
            @Param("id") Long id,
            @Param("fcmToken") String fcmToken);
}
