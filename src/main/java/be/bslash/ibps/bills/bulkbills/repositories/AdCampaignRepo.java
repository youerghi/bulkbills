package be.bslash.ibps.bills.bulkbills.repositories;

import be.bslash.ibps.bills.bulkbills.entities.AdCampaign;
import be.bslash.ibps.bills.bulkbills.entities.Entity;
import be.bslash.ibps.bills.bulkbills.enums.AdApprovalType;
import be.bslash.ibps.bills.bulkbills.enums.Status;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;

public interface AdCampaignRepo extends CrudRepository<AdCampaign, Long>, JpaSpecificationExecutor<AdCampaign> {

    @Query("select ad from AdCampaign ad where "
            + "ad.entityUser.entity = :entity and "
            + "ad.approvalType = :approvalType and "
            + "ad.status = :status and "
            + "ad.startDate <= :today and "
            + "ad.endDate >= :today")
    List<AdCampaign> findAdCampaignByApprovalTypeAndStatusAndEntitys(@Param("approvalType") AdApprovalType adApprovalType, @Param("status") Status staus, @Param("entity") Entity entity, @Param("today") LocalDateTime today);
}
