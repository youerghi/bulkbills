package be.bslash.ibps.bills.bulkbills.dto.projection;

import be.bslash.ibps.bills.bulkbills.enums.Status;

import java.math.BigDecimal;

public interface EntityActivityProjection {
    Long getId();

    Status getStatus();

    BigDecimal getTheLowestAmountPerUploadedBill();

    BigDecimal getTheHighestAmountPerUploadedBill();

    BigDecimal getMonthlyHighestValueOfExpectedUploadedBills();

    Long getEntityId();

}
