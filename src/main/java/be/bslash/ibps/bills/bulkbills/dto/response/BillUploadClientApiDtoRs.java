package be.bslash.ibps.bills.bulkbills.dto.response;

public class BillUploadClientApiDtoRs {

    private String billNumber;
    private String sadadNumber;

    public String getBillNumber() {
        return billNumber;
    }

    public void setBillNumber(String billNumber) {
        this.billNumber = billNumber;
    }

    public String getSadadNumber() {
        return sadadNumber;
    }

    public void setSadadNumber(String sadadNumber) {
        this.sadadNumber = sadadNumber;
    }
}
