package be.bslash.ibps.bills.bulkbills.dto.request;


import be.bslash.ibps.bills.bulkbills.exceptions.HttpServiceException;
import be.bslash.ibps.bills.bulkbills.exceptions.ValidationMessage;
import be.bslash.ibps.bills.bulkbills.utils.Utils;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class EntityUserLoginRqDto {

    private String username;
    private String password;
    private String fcmToken;
    private String recaptcha;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }

    public String getRecaptcha() {
        return recaptcha;
    }

    public void setRecaptcha(String recaptcha) {
        this.recaptcha = recaptcha;
    }

    public void validate(Locale locale) {
        List<ValidationMessage> errorList = new ArrayList<>();

        if (Utils.isNullOrEmpty(this.username)) {
            ValidationMessage validationMessage = new ValidationMessage("username", "username_empty", locale);
            errorList.add(validationMessage);
        }

        if (Utils.isNullOrEmpty(this.password)) {
            ValidationMessage validationMessage = new ValidationMessage("password", "password_empty", locale);
            errorList.add(validationMessage);
        }

        if (!errorList.isEmpty()) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "invalid_request", errorList, locale);
        }
    }

    public void validateClientApi(Locale locale) {
        List<ValidationMessage> errorList = new ArrayList<>();

        if (Utils.isNullOrEmpty(this.username)) {
            ValidationMessage validationMessage = new ValidationMessage("username", "username_empty", locale);
            errorList.add(validationMessage);
        }

        if (Utils.isNullOrEmpty(this.password)) {
            ValidationMessage validationMessage = new ValidationMessage("password", "password_empty", locale);
            errorList.add(validationMessage);
        }

        if (!errorList.isEmpty()) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "invalid_request", errorList, locale);
        }
    }
}
