package be.bslash.ibps.bills.bulkbills.repositories;

import be.bslash.ibps.bills.bulkbills.entities.Entity;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.LockModeType;
import java.util.List;

public interface EntityRepo extends CrudRepository<Entity, Long>, JpaSpecificationExecutor {
    List<Entity> findAll();

    Entity findEntityByCode(String code);

    @Modifying
    @Query("update Entity ent set ent.billSequence=:billSequence " +
            "where ent.id=:entityId")
    void increaseSadadNumberCounter(
            @Param("billSequence") Long billSequence,
            @Param("entityId") Long entityId);

    @Modifying
    @Query("update Entity ent set ent.autoGenerationBillNumberSequence=:billSequence " +
            "where ent.id=:entityId")
    void increaseBillNumberCounter(
            @Param("billSequence") Long billSequence,
            @Param("entityId") Long entityId);

    @Query(value = "update entity " +
            "set auto_generation_bill_number_sequence = auto_generation_bill_number_sequence + 1 " +
            "output inserted.auto_generation_bill_number_sequence " +
            "where id=:id ", nativeQuery = true)
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    Long increaseBillNumberCounterAndGet(@Param("id") Long id);


    @Modifying
    @Query("update Entity ent set ent.smsPointBalance=:smsPointBalance " +
            "where ent.id=:entityId")
    void updateSmsPointBalance(
            @Param("smsPointBalance") Long smsPointBalance,
            @Param("entityId") Long entityId);

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    @Query("select ent from Entity ent " +
            "where ent.id=:entityId")
    Entity findEntityForUpdate(@Param("entityId") Long entityId);
}
