package be.bslash.ibps.bills.bulkbills.enums;


public enum Language {
    ALL,
    ARABIC,
    ENGLISH;

    public static Language getValue(String value) {
        try {
            if (value.equals("BOTH")) {
                return Language.ALL;
            }
            return Language.valueOf(value);
        } catch (Exception ex) {
            return null;
        }
    }
}

