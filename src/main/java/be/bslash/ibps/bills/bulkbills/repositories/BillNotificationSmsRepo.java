package be.bslash.ibps.bills.bulkbills.repositories;

import be.bslash.ibps.bills.bulkbills.entities.BillNotificationSms;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BillNotificationSmsRepo extends CrudRepository<BillNotificationSms, Long>, JpaSpecificationExecutor {

    List<BillNotificationSms> findAll();

}
