package be.bslash.ibps.bills.bulkbills.dto.response;


import be.bslash.ibps.bills.bulkbills.enums.BulkBillStatus;

import java.time.LocalDateTime;

public class BulkBillTimelineDtoRs {

    private Long id;
    private String companyUsername;
    private String companyUserFullName;
    private BulkBillStatus bulkBillStatus;
    private String bulkBillStatusText;
    private LocalDateTime actionDate;
    private String userReference;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCompanyUsername() {
        return companyUsername;
    }

    public void setCompanyUsername(String companyUsername) {
        this.companyUsername = companyUsername;
    }

    public String getCompanyUserFullName() {
        return companyUserFullName;
    }

    public void setCompanyUserFullName(String companyUserFullName) {
        this.companyUserFullName = companyUserFullName;
    }

    public BulkBillStatus getBulkBillStatus() {
        return bulkBillStatus;
    }

    public void setBulkBillStatus(BulkBillStatus bulkBillStatus) {
        this.bulkBillStatus = bulkBillStatus;
    }

    public String getBulkBillStatusText() {
        return bulkBillStatusText;
    }

    public void setBulkBillStatusText(String bulkBillStatusText) {
        this.bulkBillStatusText = bulkBillStatusText;
    }

    public LocalDateTime getActionDate() {
        return actionDate;
    }

    public void setActionDate(LocalDateTime actionDate) {
        this.actionDate = actionDate;
    }

    public String getUserReference() {
        return userReference;
    }

    public void setUserReference(String userReference) {
        this.userReference = userReference;
    }
}
