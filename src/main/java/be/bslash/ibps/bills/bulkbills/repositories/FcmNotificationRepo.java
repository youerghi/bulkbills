package be.bslash.ibps.bills.bulkbills.repositories;

import be.bslash.ibps.bills.bulkbills.entities.FcmNotification;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface FcmNotificationRepo extends CrudRepository<FcmNotification, Long> {

    List<FcmNotification> findAll();
}
