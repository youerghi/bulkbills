package be.bslash.ibps.bills.bulkbills.repositories;

import be.bslash.ibps.bills.bulkbills.entities.Bill;
import be.bslash.ibps.bills.bulkbills.entities.BillMobileNumber;
import be.bslash.ibps.bills.bulkbills.entities.BulkBill;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface BillMobileNumberRepo extends CrudRepository<BillMobileNumber, Long> {

    List<BillMobileNumber> findAll();

    @Query("select item from BillMobileNumber item " +
            "where item.bill=:bill " +
            "order by item.id asc ")
    List<BillMobileNumber> findAllBillMobileNumber(@Param("bill") Bill bill);

    @Query("select item from BillMobileNumber item " +
            "where item.bill.bulkBill=:bulkBill " +
            "order by item.id asc ")
    List<BillMobileNumber> findAllBillMobileNumberByBulk(@Param("bulkBill") BulkBill bulkBill);

}
