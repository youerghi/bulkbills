package be.bslash.ibps.bills.bulkbills.utils;

import be.bslash.ibps.bills.bulkbills.entities.EntityUser;
import be.bslash.ibps.bills.bulkbills.enums.BooleanFlag;
import be.bslash.ibps.bills.bulkbills.exceptions.HttpServiceException;
import be.bslash.ibps.bills.bulkbills.repositories.EntityVatRepo;
import org.springframework.http.HttpStatus;

import java.util.Locale;

public class BillerUtils {
    public static final String VAT_VALUE_IS_NOT_MATCH_ALLOWED_VAT = "VAT value is not match allowed VAT";
    public static final String EXE = "EXE";
    public static final String NA = "NA";

    public static void validateEntityVat(final EntityUser entityUser, final Locale locale, final String vat, final EntityVatRepo entityVatRepo) {

        if (vat != null) {

            if (Utils.isStringEqual(vat, EXE) || Utils.isStringEqual(vat, NA)) {
                if (Utils.isStringEqual(vat, EXE) && entityUser.getEntity().getVatExemptedFlag().equals(BooleanFlag.NO)) {
                    throw new HttpServiceException(HttpStatus.BAD_REQUEST, VAT_VALUE_IS_NOT_MATCH_ALLOWED_VAT, locale);
                } else if (Utils.isStringEqual(vat, NA) && entityUser.getEntity().getVatNaFlag().equals(BooleanFlag.NO)) {

                    throw new HttpServiceException(HttpStatus.BAD_REQUEST, VAT_VALUE_IS_NOT_MATCH_ALLOWED_VAT, locale);
                }
            } else {
                if (!entityVatRepo.existsEntityVatByEntityAndVat(entityUser.getEntity(), Utils.convertToBigDecimal(vat))) {

                    throw new HttpServiceException(HttpStatus.BAD_REQUEST, VAT_VALUE_IS_NOT_MATCH_ALLOWED_VAT, locale);
                }
            }
        }
    }
}
