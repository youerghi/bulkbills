package be.bslash.ibps.bills.bulkbills.enums;


public enum BillSource {
    USER_INTERFACE,
    FILE,
    API;


    public static BillSource getValue(String value) {
        try {
            return BillSource.valueOf(value);
        } catch (Exception ex) {
            return null;
        }
    }
}

