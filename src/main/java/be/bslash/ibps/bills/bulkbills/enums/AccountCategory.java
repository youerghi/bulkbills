package be.bslash.ibps.bills.bulkbills.enums;

public enum AccountCategory {
    NORMAL,
    SADAD;

    public static AccountCategory getValue(String value) {
        try {
            return AccountCategory.valueOf(value);
        } catch (Exception ex) {
            return null;
        }
    }
}

