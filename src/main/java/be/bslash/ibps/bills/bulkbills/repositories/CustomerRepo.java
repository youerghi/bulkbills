package be.bslash.ibps.bills.bulkbills.repositories;


import be.bslash.ibps.bills.bulkbills.entities.Customer;
import be.bslash.ibps.bills.bulkbills.entities.Entity;
import be.bslash.ibps.bills.bulkbills.enums.Status;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;


public interface CustomerRepo extends CrudRepository<Customer, Long>, JpaSpecificationExecutor<Customer> {


    List<Customer> findAll();

    @Query("select c from Customer c where "
            + "c.entityUser.entity = :entity and "
            + "c.status = :status")
    List<Customer> findCustomerByStatusAndEntity(@Param("entity") Entity entity, @Param("status") Status staus);
}
