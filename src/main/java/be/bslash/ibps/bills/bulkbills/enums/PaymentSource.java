package be.bslash.ibps.bills.bulkbills.enums;

public enum PaymentSource {
    ENTITY_PAYMENT,
    SADAD_NOTIFICATION;
}

