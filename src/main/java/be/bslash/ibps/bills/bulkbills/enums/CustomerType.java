package be.bslash.ibps.bills.bulkbills.enums;


public enum CustomerType {
    IND,
    BUS;

    public static CustomerType getValue(String value) {
        try {
            return CustomerType.valueOf(value);
        } catch (Exception ex) {
            return null;
        }
    }
}

