package be.bslash.ibps.bills.bulkbills.controllers;

import be.bslash.ibps.bills.bulkbills.configurations.MessageEnvelope;
import be.bslash.ibps.bills.bulkbills.dto.request.BillCustomFieldRqDto;
import be.bslash.ibps.bills.bulkbills.dto.request.EntityUserLoginRqDto;
import be.bslash.ibps.bills.bulkbills.dto.response.BillUploadClientApiDtoRs;
import be.bslash.ibps.bills.bulkbills.dto.request.RecurringBillApiMakeRqDto;
import be.bslash.ibps.bills.bulkbills.entities.Bill;
import be.bslash.ibps.bills.bulkbills.entities.EntityField;
import be.bslash.ibps.bills.bulkbills.entities.EntityUser;
import be.bslash.ibps.bills.bulkbills.enums.BillSource;
import be.bslash.ibps.bills.bulkbills.enums.EntityUserType;
import be.bslash.ibps.bills.bulkbills.enums.Status;
import be.bslash.ibps.bills.bulkbills.exceptions.HttpServiceException;
import be.bslash.ibps.bills.bulkbills.services.BillNotificationService;
import be.bslash.ibps.bills.bulkbills.services.EntityFieldService;
import be.bslash.ibps.bills.bulkbills.services.EntityUserService;
import be.bslash.ibps.bills.bulkbills.services.RecurringBillService;
import be.bslash.ibps.bills.bulkbills.utils.Utils;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;

@CrossOrigin
@Controller
@RequestMapping("/clientApi")
public class ClientApiController {
    private final ObjectMapper objectMapper = new ObjectMapper();

    private final EntityUserService entityUserService;
    private final EntityFieldService entityFieldService;
    private final BillNotificationService billNotificationService;
    private final RecurringBillService recurringBillService;


    public ClientApiController(EntityUserService entityUserService, EntityFieldService entityFieldService,
                               BillNotificationService billNotificationService, RecurringBillService recurringBillService) {
        this.entityUserService = entityUserService;
        this.entityFieldService = entityFieldService;
        this.billNotificationService = billNotificationService;
        this.recurringBillService = recurringBillService;
    }

    @RequestMapping(value = "/recurring/bulk-file", method = RequestMethod.POST)
    public ResponseEntity<MessageEnvelope> uploadBulkRecurringFile(HttpServletRequest httpServletRequest,
                                                                   @RequestParam("file") MultipartFile file) throws IOException {

        long start = System.nanoTime();
        Locale locale = httpServletRequest.getLocale();

        EntityUser entityUser = verifyUser(httpServletRequest, locale);

        List<RecurringBillApiMakeRqDto> recurringBillApiMakeRqDtoList = objectMapper.readValue(file.getInputStream(),
                new TypeReference<>() {
                }
        );

        recurringBillApiMakeRqDtoList.parallelStream().forEach(recurringBillApiMakeRqDto -> {
            recurringBillApiMakeRqDto.validate(locale);
            if (recurringBillApiMakeRqDto.getCustomFieldList() != null) {
                for (BillCustomFieldRqDto c : recurringBillApiMakeRqDto.getCustomFieldList()) {
                    if (Utils.isNullOrEmpty(c.getFieldId())) {
                        throw new HttpServiceException(HttpStatus.FORBIDDEN, "Custom field id should fill", locale);
                    }

                    if (Utils.isNullOrEmpty(c.getFieldValue())) {
                        throw new HttpServiceException(HttpStatus.FORBIDDEN, "Custom field value should fill", locale);
                    }
                    EntityField e = entityFieldService.findEntityFieldByIdAndEntity(c.getFieldId(), entityUser.getEntity());
                    if (e == null || Utils.isNullOrEmpty(e.getId())) {
                        throw new HttpServiceException(HttpStatus.FORBIDDEN, "Custom field id should valid", locale);
                    }
                }
            }
        });

        Set<Bill> billList = recurringBillService.saveRecurringBillApi(recurringBillApiMakeRqDtoList, entityUser, BillSource.API, locale);

        List<BillUploadClientApiDtoRs> accountCreateDtoRsList = mapBillList(billList, locale, entityUser);

        MessageEnvelope messageEnvelope = new MessageEnvelope(HttpStatus.OK, "success", accountCreateDtoRsList, locale);

        double elapsedTimeInSecond = (double) (System.nanoTime() - start) / 1_000_000_000;
        System.out.println("Total " + elapsedTimeInSecond + " seconds");

        return new ResponseEntity<>(messageEnvelope, HttpStatus.OK);
    }

    @RequestMapping(value = "/recurring/bulk", method = RequestMethod.POST)
    public ResponseEntity<MessageEnvelope> uploadBulkRecurring(HttpServletRequest httpServletRequest,
                                                               @RequestBody List<RecurringBillApiMakeRqDto> recurringBillApiMakeRqDtoList) {

        long start = System.nanoTime();
        Locale locale = httpServletRequest.getLocale();

        EntityUser entityUser = verifyUser(httpServletRequest, locale);

        recurringBillApiMakeRqDtoList.parallelStream().forEach(recurringBillApiMakeRqDto -> {
            recurringBillApiMakeRqDto.validate(locale);
            if (recurringBillApiMakeRqDto.getCustomFieldList() != null) {
                for (BillCustomFieldRqDto c : recurringBillApiMakeRqDto.getCustomFieldList()) {
                    if (Utils.isNullOrEmpty(c.getFieldId())) {
                        throw new HttpServiceException(HttpStatus.FORBIDDEN, "Custom field id should fill", locale);
                    }

                    if (Utils.isNullOrEmpty(c.getFieldValue())) {
                        throw new HttpServiceException(HttpStatus.FORBIDDEN, "Custom field value should fill", locale);
                    }
                    EntityField e = entityFieldService.findEntityFieldByIdAndEntity(c.getFieldId(), entityUser.getEntity());
                    if (e == null || Utils.isNullOrEmpty(e.getId())) {
                        throw new HttpServiceException(HttpStatus.FORBIDDEN, "Custom field id should valid", locale);
                    }
                }
            }
        });

        Set<Bill> billList = recurringBillService.saveRecurringBillApi(recurringBillApiMakeRqDtoList, entityUser, BillSource.API, locale);

        List<BillUploadClientApiDtoRs> accountCreateDtoRsList = mapBillList(billList, locale, entityUser);

        MessageEnvelope messageEnvelope = new MessageEnvelope(HttpStatus.OK, "success", accountCreateDtoRsList, locale);

        double elapsedTimeInSecond = (double) (System.nanoTime() - start) / 1_000_000_000;
        System.out.println("Total " + elapsedTimeInSecond + " seconds");

        return new ResponseEntity<>(messageEnvelope, HttpStatus.OK);
    }

    private EntityUser verifyUser(HttpServletRequest httpServletRequest, Locale locale) {
        String username = httpServletRequest.getHeader("username");

        String password = httpServletRequest.getHeader("password");

        EntityUserLoginRqDto entityUserLoginRqDto = new EntityUserLoginRqDto();
        entityUserLoginRqDto.setUsername(username);
        entityUserLoginRqDto.setPassword(password);

        entityUserLoginRqDto.validateClientApi(locale);

        EntityUser entityUser = entityUserService.findEntityUserByUsername(username, locale);

        if (entityUser.getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_inactive", locale);
        }

        if (entityUser.getStatus().equals(Status.DELETED)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_deleted", locale);
        }

        if (entityUser.getEntity().getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "entity_inactive", locale);
        }
        return entityUser;
    }

    private List<BillUploadClientApiDtoRs> mapBillList(Set<Bill> billList, Locale locale, EntityUser entityUser) {
        if (entityUser.getEntityUserType().equals(EntityUserType.SUPER_ADMIN_PLUS)
                || entityUser.getEntityUserType().equals(EntityUserType.SUPER_ADMIN)
                || entityUser.getEntityUserType().equals(EntityUserType.SUPERVISOR_PLUS)) {

            return billList.parallelStream().map(bill -> {
                new Thread(() -> billNotificationService.sendBillNotificationIssue(bill, entityUser, locale)).start();
                BillUploadClientApiDtoRs billUploadClientApiDtoRs = new BillUploadClientApiDtoRs();
                billUploadClientApiDtoRs.setBillNumber(bill.getBillNumber());
                billUploadClientApiDtoRs.setSadadNumber(bill.getSadadNumber());

                return billUploadClientApiDtoRs;
            }).collect(Collectors.toList());

        }
        return Collections.emptyList();
    }
}
