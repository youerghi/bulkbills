package be.bslash.ibps.bills.bulkbills.enums;

public enum AccessChannel {
    ATM,
    IVR,
    KIOSK,
    INTERNET,
    PORTAL,
    BTELLER,
    POS,
    CAM,
    CORP,
    CCC,
    PDA,
    SMS,
    MOBILE,
    USSD;

}

