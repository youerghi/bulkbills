package be.bslash.ibps.bills.bulkbills.repositories;

import be.bslash.ibps.bills.bulkbills.entities.BillNotificationEmail;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BillNotificationEmailRepo extends CrudRepository<BillNotificationEmail, Long> {

    List<BillNotificationEmail> findAll();

}
