package be.bslash.ibps.bills.bulkbills.services;

import be.bslash.ibps.bills.bulkbills.dto.request.EntityUserLoginRqDto;
import be.bslash.ibps.bills.bulkbills.entities.EntityUser;
import be.bslash.ibps.bills.bulkbills.enums.Status;
import be.bslash.ibps.bills.bulkbills.exceptions.HttpServiceException;
import be.bslash.ibps.bills.bulkbills.repositories.EntityUserRepo;
import org.apache.commons.lang.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Locale;

@Service
@Transactional(readOnly = true)
public class EntityUserService {

    private final EntityUserRepo entityUserRepo;

    private final PasswordEncoder passwordEncoder;
    public EntityUserService(EntityUserRepo entityUserRepo, PasswordEncoder passwordEncoder) {
        this.entityUserRepo = entityUserRepo;
        this.passwordEncoder = passwordEncoder;
    }

    @javax.transaction.Transactional(dontRollbackOn = HttpServiceException.class)
    public EntityUser loginEntityUserClient(EntityUserLoginRqDto entityUserLoginRqDto, Locale locale) {

        EntityUser entityUser = entityUserRepo.findEntityUserByUsername(StringUtils.deleteWhitespace(entityUserLoginRqDto.getUsername().trim()));

        if (entityUser == null) {
            throw new HttpServiceException(HttpStatus.UNAUTHORIZED, "invalid_login", locale);
        }

        Integer loginCounter = entityUser.getFailedLoginCounter();
        if (loginCounter == null) {
            loginCounter = 0;
        }

        if (entityUser.getStatus().equals(Status.INACTIVE) && loginCounter < 4) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_inactive", locale);
        }

        if (entityUser.getStatus().equals(Status.INACTIVE) && loginCounter >= 4) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_blocked", locale);
        }

        if (entityUser.getStatus().equals(Status.DELETED)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_deleted", locale);
        }

        if (entityUser.getEntity().getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "entity_inactive", locale);
        }

        if (entityUser.getIdExpireDate() != null) {
            if (entityUser.getIdExpireDate().isBefore(LocalDateTime.now())) {
                throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_id_expired", locale);

            }
        }

        if (entityUser.getEntity().getIdExpiryDate() != null) {
            if (entityUser.getEntity().getIdExpiryDate().isBefore(LocalDateTime.now())) {
                throw new HttpServiceException(HttpStatus.FORBIDDEN, "company_id_expired", locale);
            }
        }

        if (loginCounter >= 4) {
            entityUserRepo.updateStatus(entityUser.getId(),Status.INACTIVE);
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_blocked", locale);
        }

        if (!passwordEncoder.matches(entityUserLoginRqDto.getPassword(), entityUser.getPassword())) {
            loginCounter = loginCounter + 1;

            entityUserRepo.updateCounter(entityUser.getId(),loginCounter);

            throw new HttpServiceException(HttpStatus.UNAUTHORIZED, "invalid_login", locale);
        }

        entityUserRepo.updateCounter(entityUser.getId(),0);

        entityUserRepo.updatePasswordReset(entityUser.getId(),0);

        entityUserRepo.updateFcmToken(entityUser.getId(), entityUserLoginRqDto.getFcmToken());

        return entityUser;
    }

    public EntityUser findEntityUserByUsername(String username, Locale locale) {

        EntityUser entityUser = entityUserRepo.findEntityUserByUsername(username);
        if (entityUser == null) {
            List<EntityUser> entityUserList = entityUserRepo.findDeletedEntityUserByUsername(username);
            if (entityUserList != null && !entityUserList.isEmpty()) {
                entityUser = entityUserList.get(0);
            }
        }
        return entityUser;
    }


}
